package topk

import (
	"reflect"
	"sort"
	"testing"
)

func TestTopK(t *testing.T) {
	type args struct {
		items []int
		k     int
	}

	tests := []struct {
		name string
		args args
		want []int
	}{
		{
			"top 3",
			args{
				[]int{5, 3, 7, 1, 8, 2, 9, 4, 3, 2, 6, 6},
				3,
			},
			[]int{7, 8, 9},
		},
		{
			"top 5",
			args{
				[]int{5, 3, 7, 1, 8, 2, 9, 4, 3, 2, 6, 6},
				5,
			},
			[]int{7, 8, 9, 6, 6},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got := TopKSort(tt.args.items, tt.args.k)
			if !sameElements(got, tt.want) {
				t.Errorf("TopKSort() = %v, want %v", got, tt.want)
			}

			got = TopKPartialSort(tt.args.items, tt.args.k)
			if !sameElements(got, tt.want) {
				t.Errorf("TopKPartialSort() = %v, want %v", got, tt.want)
			}
		})
	}
}

func sameElements(s1 []int, s2 []int) bool {
	len1, len2 := len(s1), len(s2)
	if len1 != len2 {
		return false
	}

	c1, c2 := make([]int, len1), make([]int, len2)
	copy(c1, s1)
	copy(c2, s2)

	sort.Ints(c1)
	sort.Ints(c2)
	return reflect.DeepEqual(c1, c2)
}
