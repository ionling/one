package topk

import (
	"sort"
)

// 普通排序
func TopKSort(items []int, k int) []int {
	length := len(items)
	if k >= length {
		return items
	}

	cp := make([]int, length)
	copy(cp, items)
	sort.Ints(cp)
	return cp[length-k:]
}

// 局部排序
func TopKPartialSort(items []int, k int) (res []int) {
	length := len(items)
	if k >= length {
		return items
	}

	for i := 0; i < k; i++ {
		max, maxi := items[i], i
		loopStart := i + 1
		for ii, v := range items[loopStart:] {
			if max < v {
				max, maxi = v, ii+loopStart
			}
		}
		tmp := items[i]
		items[i] = max
		items[maxi] = tmp

		res = append(res, max)
	}

	return
}
