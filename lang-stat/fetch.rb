# frozen_string_literal: true

require 'csv'
require 'json'
require 'net/http'

RepoInfo = Struct.new(:created_at, :repo_size, :forks, :open_issues, :stars, :watchers)
RepoURL = Struct.new(:name, :url)

def get_repo_info(url)
  response = Net::HTTP.get_response(URI(url))
  d = JSON.parse(response.body)
  RepoInfo.new(d['created_at'], d['size'], d['forks_count'], d['open_issues_count'],
               d['stargazers_count'], d['watchers_count'])
end

def html_url_to_api(url)
  url.insert url.index('github'), 'api.'
  url.insert url.index('.com') + 4, '/repos'
end

def repo_api_urls
  repos = CSV.read('lang-repos.tsv', { col_sep: "\t" })
  repos.shift                   # Skip header
  repos.filter_map do |x|
    lang = x[0]
    RepoURL.new(lang, html_url_to_api(x[1])) unless lang.start_with? '#'
  end
end

def repo_infos(repo_urls)
  repo_urls.map do |x|
    puts "fetching #{x.name} info"
    begin
      i = get_repo_info x.url
      [x.name, i.created_at, i.repo_size, i.forks, i.open_issues, i.stars, i.watchers]
    rescue StandardError => e
      puts 'got error: ', e
    end
  end
end

def save_repo_infos(infos)
  CSV.open('infos.tsv', 'w', { col_sep: "\t" }) do |csv|
    csv << %w[lang created_at size forks open_issues stars watchers]
    infos.each { |x| csv << x }
  end
end

def fetch
  save_repo_infos repo_infos repo_api_urls
end

fetch
