import std/asyncdispatch
import std/asynchttpserver
import std/json
import std/strformat
import std/tables
import strutils
import sugar


### Data

type
  Text = object
    id: int
    text: string
  TextStore = ref object
    offset: int
    items: TableRef[int, string]

func newTextStore(): TextStore =
  TextStore(offset: 0, items: newTable[int, string]())

func add(s: TextStore, text: string): int =
  s.offset += 1
  result = s.offset
  s.items[s.offset] = text

func remove(s: TextStore, id: int) =
  s.items.del id

func list(s: TextStore): seq[Text] =
  for id, text in s.items.pairs():
    result.add Text(id: id, text: text)


### API

proc jsonResp(req: Request, code: HttpCode, content: JsonNode,
              headers: HttpHeaders = nil): Future[void] =
  let h = if headers == nil: newHttpHeaders(true) else: headers
  h["Content-Type"] = "application/json"
  req.respond(code, $content, h)

proc parseJson(
  req: Request,
  h: (proc (req: Request, data: JsonNode) {.async.}),
) {.async, gcsafe.} =
  var j: JsonNode
  try:
    j = req.body.parseJson
  except JsonParsingError:
    await req.jsonResp(Http400, %* {
      "msg": "Problems parsing JSON",
    })
    return

  await h(req, j)


type
  TextHandler = object
    store: TextStore

func newTextHandler(): auto =
  TextHandler(store: newTextStore())

proc create(h: TextHandler, req: Request, j: JsonNode) {.async.} =
  let text = j{"text"}.getStr
  if text == "":
    await req.jsonResp(Http422, %* {
      "msg": "Blank text field",
    })
    return

  let id = h.store.add text
  await req.jsonResp(Http201, %* {
    "id": id,
  })

proc delete(h: TextHandler, req: Request, j: JsonNode) {.async.} =
  let id = j{"id"}.getInt
  if id == 0:
    await req.jsonResp(Http422, %* {
      "msg": "Blank id field",
    })
    return

  h.store.remove id
  await req.respond(Http204, "")

proc list(h: TextHandler, req: Request) {.async, gcsafe.} =
  let items = h.store.list
  await req.jsonResp(Http200, %* {
    "count": items.len,
    "items": items,
  }, )


func newHandler(): auto =
  var textHandler = newTextHandler()

  result = proc (req: Request) {.async.} =
    var path = req.url.path
    if path.startsWith "/api":
      path.removePrefix "/api"
      if path.startsWith "/texts":
        case req.reqMethod:
          of HttpGet:
            await textHandler.list(req)
            return
          of HttpPost:
            await req.parseJson (req, j) => textHandler.create(req, j)
            return
          of HttpDelete:
            await req.parseJson (req, j) => textHandler.delete(req, j)
            return
          else:
            await req.respond(Http405, "")

    await req.respond(Http404, "")


### Server

proc main {.async.} =
  var server = newAsyncHttpServer()
  server.listen(Port(8010))
  let port = server.getPort
  echo &"Server listening on port: {port.uint16}"
  let handler = newHandler()
  while true:
    if server.shouldAcceptRequest():
      await server.acceptRequest(handler)
    else:
      # too many concurrent connections, `maxFDs` exceeded.
      # wait 500ms for FDs to be closed.
      await sleepAsync(500)


when isMainModule:
  waitFor main()
