# -*- org-confirm-babel-evaluate: nil -*-
#+PROPERTY: header-args :noweb yes :results output :wrap src json

* Variables
#+name: vars
#+begin_src sh
  http="http --pretty=format --ignore-stdin"
  local_api=":8010/api"
  texts_api=":8010/api/texts"
#+end_src

* Error

** Parsing JSON
#+begin_src sh
  <<vars>>
  $http POST "$texts_api"
#+end_src

#+RESULTS:
#+begin_src json
{
    "msg": "Problems parsing JSON"
}
#+end_src

** Blank field
#+begin_src sh
  <<vars>>
  $http POST "$texts_api" id:=1234
#+end_src

#+RESULTS:
#+begin_src json
{
    "msg": "Blank text field"
}
#+end_src

* Texts

** Create
#+begin_src sh
  <<vars>>
  $http POST "$texts_api" text=1234
#+end_src

#+RESULTS:
#+begin_src json
{
    "id": 4
}
#+end_src

** List
#+begin_src sh
  <<vars>>
  $http GET "$texts_api"
#+end_src

#+RESULTS:
#+begin_src json
{
    "count": 3,
    "items": [
        {
            "id": 4,
            "text": "1234"
        },
        {
            "id": 3,
            "text": "1235"
        },
        {
            "id": 1,
            "text": "1236"
        }
    ]
}
#+end_src

** Delete
#+begin_src sh
  <<vars>>
  $http DELETE "$texts_api" id:=2
#+end_src

#+RESULTS:
#+begin_src json
#+end_src
