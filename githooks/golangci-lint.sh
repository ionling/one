#!/usr/bin/env bash

# REF https://github.com/dnephin/pre-commit-golang/blob/master/run-golangci-lint.sh

declare -A run_mods
root=$(pwd)

for file in $@; do
    cd $(dirname $root/$file)
    mod_root=$(dirname $(go env GOMOD))
    test -n "${run_mods[$mod_root]}" && continue

    run_mods[$mod_root]=1
    cd $mod_root
    golangci-lint run --fix || exit 1
done
