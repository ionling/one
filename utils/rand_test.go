// -*- go-test-args: "-v -count 1";  -*-
package utils

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestRandBool(t *testing.T) {
	var (
		count     = 10000
		trueCount int
	)
	for i := 0; i < count; i++ {
		if RandBool() {
			trueCount++
		}
	}
	diff := count/2 - trueCount
	if diff < 0 {
		diff = -diff
	}
	assert.True(t, diff < 100)
}

func TestRandPhoneNumber(t *testing.T) {
	tests := []struct {
		name    string
		wantLen int
	}{
		{"1", 11},
		{"2", 11},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			assert.Equal(t, tt.wantLen, len(RandPhoneNumber()))
		})
	}
}

func TestRandString(t *testing.T) {
	type args struct {
		prefix string
		length int
	}
	tests := []struct {
		name string
		args args
	}{
		{"normal", args{"abab", 10}},
		{"truncated", args{"ababkjfklse", 3}},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got := RandString(tt.args.prefix, tt.args.length)
			assert.Equal(t, tt.args.length, len(got))
		})
	}
}
