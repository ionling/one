package utils

import (
	"math"
	"math/rand"
	"strconv"
	"strings"
	"time"
)

func RandBool() bool {
	rand.Seed(time.Now().UnixNano())
	return rand.Intn(2) == 1
}

func RandPhoneNumber() string {
	rand.Seed(time.Now().UnixNano())
	tenBillion := int(math.Pow10(10))
	return strconv.Itoa(tenBillion + rand.Intn(tenBillion))
}

// RandString generate random string.
// Prefix will be truncated if its length larger than length param.
func RandString(prefix string, length int) string {
	rand.Seed(time.Now().UnixNano())
	chars := "ABCDEFGHIJKLMNOPQRSTUVWXYZ" +
		"abcdefghijklmnopqrstuvwxyz" +
		"0123456789"

	diff := length - len(prefix)
	var b strings.Builder
	for i := 0; i < diff; i++ {
		b.WriteByte(chars[rand.Intn(len(chars))])
	}
	return (prefix + b.String())[:length]
}
