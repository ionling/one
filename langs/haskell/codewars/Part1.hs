{-# LANGUAGE ImportQualifiedPost #-}

module Part1 where

import Control.Arrow ((>>>))
import Control.Monad (join)
import Data.Bool (bool, otherwise, (&&), (||))
import Data.Char (isAlpha, isAscii, isAsciiLower, ord, toLower)
import Data.List (elemIndex, nub, partition, unfoldr)
import Data.Map.Strict (Map, fromAscList, lookupLE)
import Data.Map.Strict qualified as M
import Data.Maybe (Maybe, fromMaybe, mapMaybe, maybe)
import Data.Set qualified as S
import Data.Tuple (swap)
import Prelude hiding (maximum, minimum)

--- reversedString

-- https://www.codewars.com/kata/5168bb5dfe9a00b126000018/train/haskell
reversedString :: String -> String
reversedString (x : xs) = reversedString xs ++ [x]

-- https://www.codewars.com/kata/reviews/59e0d992865ddc418e00149d/groups/59e8a32974a919db71000c9f
reversedString2 :: String -> String
reversedString2 = foldl (flip (:)) []

--- areYouPlayingBanjo
--- https://www.codewars.com/kata/53af2b8861023f1d88000832/haskell

areYouPlayingBanjo :: String -> String
-- NOTE Pattern matching: http://learnyouahaskell.com/syntax-in-functions#pattern-matching
areYouPlayingBanjo "" = ""
areYouPlayingBanjo ('R' : xs) = "R" ++ xs ++ " plays banjo"
areYouPlayingBanjo ('r' : xs) = "r" ++ xs ++ " plays banjo"
areYouPlayingBanjo name = name ++ " does not play banjo"

areYouPlayingBanjo2 :: String -> String
areYouPlayingBanjo2 "" = ""
-- NOTE Guards: http://learnyouahaskell.com/syntax-in-functions#guards-guards
areYouPlayingBanjo2 (x : xs)
  | x == 'R' = "R" ++ xs ++ " plays banjo"
  | x == 'r' = "r" ++ xs ++ " plays banjo"
areYouPlayingBanjo2 name = name ++ " does not play banjo"

-- https://www.codewars.com/kata/reviews/5423320fcfcab1b541000006/groups/54233282e8eb18b46e000008
areYouPlayingBanjo3 :: String -> String
areYouPlayingBanjo3 name@(c : _)
  | c `elem` "rR" = name ++ " plays banjo"
  | otherwise = name ++ " does not play banjo"

-- https://www.codewars.com/kata/reviews/5423320fcfcab1b541000006/groups/54239d0dc8386d1bca0003b5
areYouPlayingBanjo4 :: String -> String
-- NOTE Where
areYouPlayingBanjo4 name = name ++ " " ++ play name ++ " banjo"
  where
    play ('r' : _) = "plays"
    play ('R' : _) = "plays"
    play _ = "does not play"

--- doubleInteger
--- https://www.codewars.com/kata/53ee5429ba190077850011d4/haskell

doubleInteger :: Num a => a -> a
doubleInteger a = a + a

-- https://www.codewars.com/kata/reviews/541e0bde65330c1eec000108/groups/5420e39bde6208fb340011d6
doubleInteger2 :: Num a => a -> a
doubleInteger2 = (* 2)

-- https://www.codewars.com/kata/reviews/541e0bde65330c1eec000108/groups/542a65d2909c977172000407
doubleInteger3 :: Num a => a -> a
doubleInteger3 = join (+)

--- minimum/maximum
--- https://www.codewars.com/kata/577a98a6ae28071780000989/train/haskell

minimum :: [Int] -> Int
-- NOTE that (x:[]) and (x:y:[]) could be rewriten as [x] and [x,y],
-- because its syntatic sugar, we don't need the parentheses.
-- Refer http://learnyouahaskell.com/syntax-in-functions#pattern-matching
minimum [x] = x
minimum (x : xs)
  | x >= min = min
  | otherwise = x
  where
    min = minimum xs

maximum :: [Int] -> Int
maximum [x] = x
maximum (x : xs)
  | x >= max = x
  | otherwise = max
  where
    max = maximum xs

-- https://www.codewars.com/kata/reviews/62b313873d21a80001496151/groups/638e0fdfe36cca00010f0de8
-- https://stackoverflow.com/questions/58981396/what-does-single-quote-apostrophe-mean-after-function-names-in-haskell
minimum' :: [Int] -> Int
minimum' [x] = x
minimum' (x : y : xs) = minimum' (min x y : xs)

-- https://www.codewars.com/kata/reviews/62b313873d21a80001496151/groups/62b314e94b631f0001bee966
minimum2 :: [Int] -> Int
minimum2 = foldr1 min

maximum2 :: [Int] -> Int
maximum2 = foldl1 max

-- https://www.codewars.com/kata/reviews/62b313873d21a80001496151/groups/62b37bc11985110001d2f088
maximum3 :: [Int] -> Int
maximum3 [x] = x
maximum3 [x, y] = max x y
maximum3 (x : xs) = max x $ maximum3 xs

--- evenOrOdd
--- https://www.codewars.com/kata/53da3dbb4a5168369a0000fe/train/haskell

evenOrOdd :: (Integral a) => a -> String
evenOrOdd n
  | r == 0 = "Even"
  | otherwise = "Odd"
  where
    r = n `rem` 2

--- romanNumerals
-- https://www.codewars.com/kata/51b62bf6a9c58071c600001b/haskell

romanNumerals :: Integer -> String
romanNumerals n
  | n >= 1000 = "M" ++ romanNumerals (n - 1000)
  | n >= 900 = "CM" ++ romanNumerals (n - 900)
  | n >= 500 = "D" ++ romanNumerals (n - 500)
  | n >= 400 = "CD" ++ romanNumerals (n - 400)
  | n >= 100 = "C" ++ romanNumerals (n - 100)
  | n >= 90 = "XC" ++ romanNumerals (n - 90)
  | n >= 50 = "L" ++ romanNumerals (n - 50)
  | n >= 40 = "XL" ++ romanNumerals (n - 40)
  | n >= 10 = "X" ++ romanNumerals (n - 10)
  | n == 9 = "IX" ++ romanNumerals (n - 9)
  | n >= 5 = "V" ++ romanNumerals (n - 5)
  | n == 4 = "IV" ++ romanNumerals (n - 4)
  | n >= 1 = "I" ++ romanNumerals (n - 1)
  | otherwise = ""

-- https://www.codewars.com/kata/reviews/54a5e21c37f435519800005e/groups/54a5e21c37f4355198000060
-- After seeing this solution, I find that it's essential to define the combined units,
-- such as CM and CD.
numerals :: [(String, Integer)]
numerals =
  zip
    ["M", "CM", "D", "CD", "C", "XC", "L", "XL", "X", "IX", "V", "IV", "I"]
    [1000, 900, 500, 400, 100, 90, 50, 40, 10, 9, 5, 4, 1]

romanNumerals2 :: Integer -> String
romanNumerals2 0 = ""
romanNumerals2 n = k ++ romanNumerals2 (n - v)
  where
    (k, v) = head $ filter ((<= n) . snd) numerals

-- https://www.codewars.com/kata/reviews/54a5e21c37f435519800005e/groups/5629e801de6d9dd1c30000e7
-- NOTE map, lookupLE, unfoldr
numerals3 :: (Num a, Ord a) => M.Map a String
numerals3 =
  M.fromList
    [ (1000, "M"),
      (900, "CM"),
      (500, "D"),
      (400, "CD"),
      (100, "C"),
      (90, "XC"),
      (50, "L"),
      (40, "XL"),
      (10, "X"),
      (9, "IX"),
      (5, "V"),
      (4, "IV"),
      (1, "I")
    ]

rn :: (Num a, Ord a) => a -> Maybe (String, a)
rn n = do
  (v, s) <- M.lookupLE n numerals3
  return (s, n - v)

romanNumerals3 :: (Num a, Ord a) => a -> String
romanNumerals3 n = concat $ unfoldr rn n

-- https://www.codewars.com/kata/reviews/54a5e21c37f435519800005e/groups/5c607e18d40a5d00017a1ab6
-- NOTE fmap, swap, <$>
romans :: Map Integer String
romans =
  fromAscList
    [ (1, "I"),
      (4, "IV"),
      (5, "V"),
      (9, "IX"),
      (10, "X"),
      (40, "XL"),
      (50, "L"),
      (90, "XC"),
      (100, "C"),
      (400, "CD"),
      (500, "D"),
      (900, "CM"),
      (1000, "M")
    ]

nextRoman :: Integer -> Maybe (String, Integer)
nextRoman n = fmap (n -) . swap <$> lookupLE n romans

romanNumerals4 :: Integer -> String
romanNumerals4 = concat . unfoldr nextRoman

--- multiplesOf3And5
-- https://www.codewars.com/kata/514b92a657cdc65150000006/haskell
multiplesOf3And5 :: Integer -> Integer
multiplesOf3And5 n = sum $ S.fromList $ sumLess 0 n 3 ++ sumLess 0 n 5

sumLess :: Integer -> Integer -> Integer -> [Integer]
-- m is max
sumLess n m step
  | n >= m = []
  | m - n < step = [n]
  | otherwise = n : sumLess (n + step) m step

-- https://www.codewars.com/kata/reviews/5546614c0240a76900000188/groups/5546614c0240a7a4be00018d
multiplesOf3And5' :: Integer -> Integer
multiplesOf3And5' number = sum [n | n <- [1 .. number - 1], n `mod` 3 == 0 || n `mod` 5 == 0]

-- https://www.codewars.com/kata/reviews/5546614c0240a76900000188/groups/5549c1c0a1cf335820000030
multiplesOf3And5'' :: Integer -> Integer
multiplesOf3And5'' n = sum $ nub $ [3, 6 .. n - 1] ++ [5, 10 .. n - 1]

--- cakes
-- https://www.codewars.com/kata/525c65e51bf619685c000059/train/haskell

type Ingredient = String

type Amount = Int

type Recipe = [(Ingredient, Amount)]

type Storage = [(Ingredient, Amount)]

cakes :: Recipe -> Storage -> Int
cakes recipe storage =
  minimum [cakesOne x recipe storage | (x, _) <- recipe]
  where
    cakesOne :: Ingredient -> Recipe -> Storage -> Int
    cakesOne ingredient recipe storage = getAmount ingredient storage `div` getAmount ingredient recipe
    getAmount :: Ingredient -> [(Ingredient, Amount)] -> Amount
    getAmount ingredient recipes = headDefault [y | (x, y) <- recipes, x == ingredient]
    headDefault [] = 0
    headDefault x = head x

-- https://www.codewars.com/kata/reviews/54a85407046913eae40000e1/groups/54aa705ebe9afb2a200007b1
-- NOTE lookup, maybe
cakes2 :: Recipe -> Storage -> Int
cakes2 recipe storage = minimum $ map (\(w, q) -> maybe 0 (`div` q) $ lookup w storage) recipe

-- https://www.codewars.com/kata/reviews/54a85407046913eae40000e1/groups/54aa705ebe9afb2a200007b1
-- NOTE mapM, <$>
cakes3 :: Recipe -> Storage -> Int
cakes3 recipe storage = maybe 0 minimum $ mapM storageQuotient recipe
  where
    storageQuotient (ingr, reqAmt) = (`div` reqAmt) <$> lookup ingr storage

-- https://www.codewars.com/kata/reviews/54a85407046913eae40000e1/groups/58b564866b64b123c6000499
bound :: Storage -> (Ingredient, Amount) -> Int
bound s (i, a) = maybe 0 (`div` a) (lookup i s)

cakes4 :: Recipe -> Storage -> Int
cakes4 recipe storage = minimum $ map (bound storage) recipe

-- https://www.codewars.com/kata/reviews/54a85407046913eae40000e1/groups/5ce1adf89dba8e000196fe5a
-- Hard to understand
-- NOTE fst, snd, partition, null
cakes5 :: Recipe -> Storage -> Int
cakes5 [] _ = maxBound :: Int
cakes5 ((ri, ra) : rs) ss = if null s then 0 else minimum (div (snd $ head s) ra : [cakes5 rs sa])
  where
    (s, sa) = partition ((== ri) . fst) ss

-- https://www.codewars.com/kata/reviews/54a85407046913eae40000e1/groups/5f8adbaf3853d200015a6ab6
cakes6 :: Recipe -> Storage -> Int
cakes6 recipe storage = minimum [fromMaybe 0 (lookup ingredient storage) `div` amount | (ingredient, amount) <- recipe]

-- https://www.codewars.com/kata/reviews/54a85407046913eae40000e1/groups/54d130118a433db2e40009e7
cakes7 :: Recipe -> Storage -> Int
cakes7 recipe storage = maybe 0 minimum (sequence [(`div` amnt) <$> lookup ingr storage | (ingr, amnt) <- recipe])

--- spinWords
-- https://www.codewars.com/kata/5264d2b162488dc400000001/train/haskell
spinWords :: String -> String
spinWords s =
  unwords $ map reverseIf $ words s
  where
    reverseIf s
      | length s < 5 = s
      | otherwise = reverse s

-- https://www.codewars.com/kata/reviews/55169d3a891547bedd000681/groups/5516b57777dd9e4bb70007c8
spinWords2 :: String -> String
spinWords2 = unwords . map f . words
  where
    f xs
      | length xs < 5 = xs
      | otherwise = reverse xs

-- https://www.codewars.com/kata/reviews/55169d3a891547bedd000681/groups/55d00fd885abaf10d5000039
spinWords3 :: String -> String
spinWords3 s = unwords [if length word >= 5 then reverse word else word | word <- words s]

-- https://www.codewars.com/kata/reviews/55169d3a891547bedd000681/groups/64378da9116bad000185e8c3
-- NOTE bool, >>>
spin :: String -> String
spin x = bool x (reverse x) (length x >= 5)

spinWords4 :: String -> String
spinWords4 = words >>> map spin >>> unwords

--- alphabetPosition
-- https://www.codewars.com/kata/546f922b54af40e1e90001da/train/haskell
alphabetPosition :: String -> String
alphabetPosition s =
  unwords [show $ diff x | x <- s, isAlpha x]
  where
    diff c =
      ord (toLower c) - ord 'a' + 1
    isAlpha c =
      and [p $ toLower c | p <- [(<= 'z'), (>= 'a')]]

-- https://www.codewars.com/kata/reviews/61e1608d5fe87200015bcb0c/groups/61e674e6fcac0e00017ae5d8
-- NOTE succ, mapMaybe, elemIndex
alphabetPosition2 :: [Char] -> String
alphabetPosition2 =
  unwords
    . map (show . succ)
    . mapMaybe ((`elemIndex` alphabet) . toLower)
  where
    alphabet = ['a' .. 'z']

-- https://www.codewars.com/kata/reviews/61e1608d5fe87200015bcb0c/groups/641467aeb0eec8000100f335
alphabetPosition3 :: [Char] -> String
alphabetPosition3 =
  unwords
    . map (\c -> show $ ord c - ord 'a' + 1)
    . filter isAsciiLower
    . map toLower

-- https://www.codewars.com/kata/reviews/61e1608d5fe87200015bcb0c/groups/62762106cc98ff000181c980
-- Interesting dots
alphabetPosition4 :: [Char] -> String
alphabetPosition4 =
  unwords
    . map (show . (\x -> x - 96) . ord . toLower)
    . filter isAlpha
    . filter isAscii

-- NOTE fromEnum
alphabetPosition5 :: [Char] -> String
alphabetPosition5 =
  unwords
    . filter (\x -> (read x >= 1) && (read x < 27))
    . map (f . toLower)
  where
    f a = show (fromEnum a - 96)

--- top3
-- https://www.codewars.com/kata/51e056fe544cf36c410000fb/train/haskell

-- top3 :: [Char] -> [[Char]]
-- top3 = toLower . filter () . words . foldl () .

-- https://stackoverflow.com/questions/9142731/what-does-the-symbol-mean-in-haskell

main :: IO ()
main =
  do
    print "hello"
