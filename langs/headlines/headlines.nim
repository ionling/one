import os
import osproc
import sequtils
import sugar
import strutils
import re


type Heading = tuple
  lineNo: string
  level: int
  title: string


proc getFullHeadings(f: string): seq[Heading] =
  var headings: seq[Heading] = @[]

  func getFullTitle(): string =
    var lastLevel = 0

    result = splitFile(f).name
    for h in headings:
      result &= "/".repeat(h.level - lastLevel) & h.title
      lastLevel = h.level

  let outp = execProcess r"rg -n '^\*+ ' " & f
  for line in outp.splitLines:
    var matches: array[3, string]
    if not line.match(re"(\d+):(\*+) (.+)", matches, 0) :
      continue

    let heading: Heading = (matches[0], matches[1].len, matches[2])
    headings = headings.filter x => x.level < heading.level
    headings.add heading
    result.add (heading.lineNo, 0, getFullTitle())


for f in walkFiles "/home/vision/org/*.org":
  for h in getFullHeadings f:
    echo h
