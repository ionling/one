package main

import (
	"fmt"
	"os"
	"os/exec"
	"path/filepath"
	"regexp"
	"runtime/pprof"
	"strings"
)

type Heading struct {
	LineNo string
	Level  int
	Title  string
}

func main() {
	f, err := os.Create("cpuprofile")
	if err != nil {
		panic(err)
	}

	pprof.StartCPUProfile(f)
	defer pprof.StopCPUProfile()

	matches, err := filepath.Glob("/home/vision/org/*.org")
	if err != nil {
		panic(err)
	}

	for _, match := range matches {
		for _, v := range getFullHeadings(match) {
			fmt.Println(v)
		}
	}
}

func getFullHeadings(f string) (r []Heading) {
	headings := []Heading{}

	getFullTitle := func() (t string) {
		t = strings.TrimSuffix(f, filepath.Ext(f))
		lastLevel := 0
		for _, h := range headings {
			t += strings.Repeat("/", h.Level-lastLevel) + h.Title
		}
		return
	}

	out, err := exec.Command("rg", "-n", `^\*+ `, f).Output()
	if err != nil {
		return
	}

	re := regexp.MustCompile(`(\d+):(\*+) (.+)`)
	lines := strings.Split(strings.TrimSpace(string(out)), "\n")
	for _, line := range lines {
		matches := re.FindAllStringSubmatch(line, -1)[0]
		heading := Heading{
			LineNo: matches[1],
			Level:  len(matches[2]),
			Title:  matches[3],
		}

		tmp := []Heading{}
		for _, h := range headings {
			if h.Level < heading.Level {
				tmp = append(tmp, h)
			}
		}
		headings = tmp
		headings = append(headings, heading)

		r = append(r, Heading{
			LineNo: heading.LineNo,
			Level:  0,
			Title:  getFullTitle(),
		})
	}

	return
}
