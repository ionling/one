import glob
import re
import subprocess
from collections import namedtuple
from collections.abc import Iterator
from pathlib import Path

Heading = namedtuple("Heading", "line level title")


def get_full_headlines(file: str) -> Iterator[Heading]:
    headings = list[Heading]()

    def full_title():
        title = Path(file).stem

        last_level = 0
        for h in headings:
            title += "/" * (h.level - last_level) + h.title
            last_level = h.level

        return title

    cmd = ["rg", "-n", r"^\*+ "]
    cmd.append(file)
    p = subprocess.run(cmd, capture_output=True)
    for line in p.stdout.decode().splitlines():
        matches = re.findall(r"(\d+):(\*+) (.+)", line)[0]
        heading = Heading(matches[0], len(matches[1]), matches[2])
        headings = list(filter(lambda x: x.level < heading.level, headings))
        headings.append(heading)
        yield Heading(heading.line, 0, full_title())


for f in glob.glob("/home/vision/org/*.org"):
    for h in get_full_headlines(f):
        print(h.line, h.title)
