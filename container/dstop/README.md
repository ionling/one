# DStop

`dstop` is abbr for `docker stop`,
which is used to trace system signals sent by `docker stop`.
