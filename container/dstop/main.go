package main

import (
	"io"
	"log"
	"os"
	"os/signal"
	"syscall"
)

func main() {
	f, err := os.OpenFile("dstop.log", os.O_RDWR|os.O_CREATE|os.O_APPEND, 0640)
	if err != nil {
		log.Fatalln("open log file:", err)
	}
	defer f.Close()

	log.SetOutput(io.MultiWriter(os.Stderr, f))
	log.Println("Starting...")

	c := make(chan os.Signal, 1)
	signal.Notify(c, os.Interrupt, syscall.SIGTERM, syscall.SIGINT)
	v := <-c

	log.Println("Got signal:", v)
	log.Println("Stopping...")
}
