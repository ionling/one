package main

import (
	"fmt"
	"net/url"
	"os"
	"strconv"

	"github.com/pkg/errors"
	"github.com/spf13/cobra"

	pc "proxy-check"
)

var rootCmd = &cobra.Command{
	Use:   "proxycheck [proxy-url]",
	Short: "A tool to check proxy server",
	Args:  cobra.ExactArgs(1),
	RunE: func(cmd *cobra.Command, args []string) error {
		u, err := url.Parse(args[0])
		if err != nil {
			return errors.Wrap(err, "parse url")
		}

		var scheme pc.ProxySchema
		switch u.Scheme {
		case "http":
			scheme = pc.Http
		case "socks5":
			scheme = pc.Socks5
		}

		port, err := strconv.Atoi(u.Port())
		if err != nil {
			return errors.Wrap(err, "parse port")
		}

		ok, err := pc.Check(u.Hostname(), port, scheme)
		if err != nil {
			return errors.Wrap(err, "check")
		}
		if ok {
			fmt.Println("available")
		} else {
			fmt.Println("unavailable")
		}
		return nil
	},
}

func main() {
	if err := rootCmd.Execute(); err != nil {
		os.Exit(1)
	}
}
