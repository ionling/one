set -e

echo 'building'
go build cmd/main.go

echo 'Moving to /usr/local/bin'
sudo mv main /usr/local/bin/proxycheck

echo 'Done'
