package proxycheck

import "testing"

func TestCheck(t *testing.T) {
	type args struct {
		host   string
		port   int
		scheme ProxySchema
	}
	tests := []struct {
		name    string
		args    args
		want    bool
		wantErr bool
	}{
		{
			"socks5",
			args{
				"localhost",
				1080,
				Socks5,
			},
			true,
			false,
		},
		{
			"http",
			args{
				"localhost",
				8001,
				Http,
			},
			true,
			false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := Check(tt.args.host, tt.args.port, tt.args.scheme)
			if (err != nil) != tt.wantErr {
				t.Errorf("Check() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("Check() = %v, want %v", got, tt.want)
			}
		})
	}
}
