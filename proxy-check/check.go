package proxycheck

import (
	"fmt"
	"io/ioutil"
	"net/http"
	"net/url"
	"time"

	"github.com/pkg/errors"
)

type ProxySchema uint8

const (
	Http ProxySchema = iota
	Socks5
)

func Check(host string, port int, scheme ProxySchema) (bool, error) {
	var ss string
	switch scheme {
	case Socks5:
		ss = "socks5"
	case Http:
		ss = "http"
	default:
		return false, errors.New("invalid proxy schema")
	}
	proxy := http.ProxyURL(&url.URL{
		Scheme: ss,
		Host:   fmt.Sprintf("%s:%d", host, port),
	})
	transport := http.Transport{Proxy: proxy}
	client := &http.Client{Transport: &transport, Timeout: 5 * time.Second}
	resp, err := client.Get("https://www.google.com")
	if err != nil {
		return false, errors.Wrap(err, "get google")
	}

	body, err := ioutil.ReadAll(resp.Body)
	defer resp.Body.Close()
	if err != nil {
		return false, errors.Wrap(err, "read body")
	}
	return len(body) > 0, nil
}
