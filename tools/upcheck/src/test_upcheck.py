from . import upcheck


def test_gopls():
    assert upcheck.Gopls.name == "gopls"
    assert upcheck.Gopls.get_last_version() == "v0.14.2-pre.1"
    assert upcheck.Gopls.get_installed_version() == "v0.14.1"


def test_navi():
    assert upcheck.Navi.name == "navi"
    assert upcheck.Navi.get_last_version() == "v2.22.1"
    assert upcheck.Navi.get_installed_version() == ""
