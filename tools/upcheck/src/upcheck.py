import subprocess
from abc import ABC, abstractmethod

import requests


class Package(ABC):
    name: str

    @staticmethod
    @abstractmethod
    def get_last_version():
        pass

    @staticmethod
    @abstractmethod
    def get_installed_version() -> str:
        pass


class Gopls(Package):
    name = "gopls"

    @staticmethod
    def get_last_version():
        goproxy_url = "https://goproxy.cn"
        pkg = "golang.org/x/tools/gopls"
        r = requests.get(f"{goproxy_url}/{pkg}/@v/list")
        return r.text.splitlines()[-1]

    @staticmethod
    def get_installed_version():
        r = subprocess.run(["gopls", "version"], capture_output=True, text=True)
        return r.stdout.splitlines()[0].split()[1]


class Navi(Package):
    name = "navi"

    @staticmethod
    def get_last_version():
        repo = "denisidoro/navi"
        r = requests.get(f"https://api.github.com/repos/{repo}/releases")
        return r.json()[0]["tag_name"]

    @staticmethod
    def get_installed_version():
        r = subprocess.run(["navi", "--version"], capture_output=True, text=True)
        return f"v{r.stdout.splitlines()[0].split()[1]}"


class Fzf(Package):
    name = "fzf"

    @staticmethod
    def get_last_version():
        pass

    @staticmethod
    def get_installed_version():
        r = subprocess.run(["fzf", "--version"], capture_output=True, text=True)
        return f"v{r.stdout.splitlines()[0].split()[0]}"


def check():
    for p in [Gopls, Navi]:
        last_v, installed_v = p.get_last_version(), p.get_installed_version()
        if last_v == installed_v:
            print(f"{p.name}: is up to date {last_v}")
        else:
            print(f"{p.name}: {installed_v} -> {last_v}")


if __name__ == "__main__":
    check()
