import zipfile


def list_epub_files(epub_path):
    try:
        # Open the EPUB file as a ZIP archive
        with zipfile.ZipFile(epub_path, "r") as epub:
            # List all files in the archive
            file_list = epub.namelist()
        return file_list
    except zipfile.BadZipFile:
        print("Error: The file is not a valid ZIP archive.")
        return []
    except FileNotFoundError:
        print("Error: The file was not found.")
        return []


def parse_name(s: str):
    """Return a tuple with the index number first and the word name second."""
    tp = s.removeprefix("OEBPS/").removesuffix(".xhtml").split("_")
    if len(tp) == 1:
        tp = [0, tp[0]]
    elif len(tp) == 2:
        tp[0] = int(tp[0])
    return tp


"""
Example:

  <ol>
    <li>
      <a href="3_am.xhtml">a.m.</a>
      <ol>
        <li>
          <a href="4_abacus.xhtml">abacus</a>
          <ol>
            <li><a href="5_abalone.xhtml">abalone</a></li>
          </ol>
        </li>
      </ol>
    </li>
  </ol>
"""

# It's better to use the group-by method.
# It's simpler and more intuitive.

def gen_toc_lines(files):
    res = []
    def append(tag, level=0):
        res.append(f"{"  "*level}{tag}")

    append("<li>")
    append("<ol>", level=1)
    append("<li>", level=2)
    append("<ol>", level=3)

    def end_t2():
        append("</ol>", level=3)
        append("</li>", level=2)

    def end_t0():
        end_t2()
        append("</ol>", level=1)
        res.append(f"</li>")

    prev_t0 = ""  # Prev title 1
    prev_t1 = ""
    for f in files:
        name = f[1]
        t0 = name[:1]
        t1 = name[:2]
        href = f"{f[0]}_{f[1]}.xhtml"
        new_t0 = t0 != prev_t0
        new_t1 = t1 != prev_t1
        if new_t0:
            end_t0()
            append("<li>")
            append(f"<a href='{href}'>{t0.upper()}</a>", level=1)

        if new_t1:
            if new_t0:
                append("<ol>", level=1)
            else:
                end_t2()

            append("<li>",level=2)
            append(f"<a href='{href}'>{t1.upper()}</a>", level=3)
            append("<ol>", level=3)

        append(f"<li><a href='{href}'>{name}</a></li>", level=4)
        prev_t0 = t0
        prev_t1 = t1

    end_t0()
    return res


def gen_toc_file():
    epub_path = "xxx.epub"  # CHANGEME
    files = list_epub_files(epub_path)

    file_tuples = [parse_name(it) for it in files if it.endswith(".xhtml")]
    file_tuples = sorted(file_tuples, key=lambda t: t[0])  # Sort by index
    lines = gen_toc_lines(file_tuples)

    with open("x.html", "w") as out:
        out.write("\n".join(lines))


if __name__ == "__main__":
    gen_toc_file()
