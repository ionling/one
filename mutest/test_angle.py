import angle


def test_between():
    assert angle.between(12, 00) == 0
    assert angle.between(3, 00) == 90
    assert angle.between(6, 30) == 180 + 15 - 180  # base + correction - minutes
    assert angle.between(7, 50) == abs(210 + 3 - 300)
