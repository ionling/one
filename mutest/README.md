# Mutaion Testing Demo

Mutation testing demo in Python use the `mutmut` package.

Refer to [Python 突变测试介绍](https://linux.cn/article-12871-1.html).

Run with:

```sh
mutmut run --paths-to-mutate angle.py
```
