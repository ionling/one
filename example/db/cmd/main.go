package main

import (
	"database/sql"
	"log"

	dbe "db"
)

func main() {
	dsn := "root:9999@tcp(127.0.0.1:3306)/example"
	db, err := sql.Open("mysql", dsn)
	if err != nil {
		log.Fatal(err)
	}

	if err := db.Ping(); err != nil {
		log.Fatal(err)
	}

	defer db.Close()

	if err := dbe.DirtyRead(db); err != nil {
		log.Fatal(err)
	}
}
