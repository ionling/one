package db

import (
	"database/sql"
	"fmt"
	"log"

	_ "github.com/go-sql-driver/mysql"
	"github.com/pkg/errors"
)

type Step struct {
	Name string
	Run  func() error
}

func RunSteps(steps ...Step) (err error) {
	for _, st := range steps {
		fmt.Println(st.Name)
		if err = st.Run(); err != nil {
			return errors.Wrap(err, st.Name)
		}
	}

	return
}

func DirtyRead(db *sql.DB) (err error) {
	_, err = db.Exec("SET GLOBAL TRANSACTION ISOLATION LEVEL READ UNCOMMITTED")
	if err != nil {
		return errors.Wrap(err, "set isolation level to read uncommitted")
	}

	defer func() {
		_, err := db.Exec("SET GLOBAL TRANSACTION ISOLATION LEVEL REPEATABLE READ")
		if err != nil {
			log.Println("recover isolation level", err)
		}
	}()

	if err := RunSteps(dirtyReadSteps(db)...); err != nil {
		return errors.Wrap(err, "run steps")
	}

	return
}

func dirtyReadSteps(db *sql.DB) []Step {
	var (
		id         int64
		tx         *sql.Tx
		initAmount = 200
	)

	return []Step{
		{
			"insert account",
			func() error {
				ires, err := db.Exec(
					"INSERT INTO account (user_id, amount) VALUES (100, ?)",
					initAmount,
				)
				if err != nil {
					return errors.Wrap(err, "exec insert")
				}

				id, err = ires.LastInsertId()
				if err != nil {
					return errors.Wrap(err, "get last insert id")
				}

				return nil
			},
		}, {
			"begin update transaction",
			func() (err error) {
				tx, err = db.Begin()
				return errors.Wrap(err, "begin update tx")
			},
		}, {
			"update account",
			func() (err error) {
				_, err = tx.Exec("UPDATE account SET amount = 900 WHERE id = ?", id)
				return errors.Wrap(err, "exec update")
			},
		}, {
			"get dirty amount",
			func() error {
				var dirtyAmount int
				row := db.QueryRow("SELECT amount FROM account WHERE id = ?", id)
				if err := row.Scan(&dirtyAmount); err != nil {
					return errors.Wrap(err, "scan dirty amount")
				}

				fmt.Println("dirty amount:", dirtyAmount)
				return nil
			},
		}, {
			"rollback update transaction",
			func() error {
				err := tx.Rollback()
				return errors.Wrap(err, "rollback update tx")
			},
		}, {
			"get clean amount",
			func() error {
				var cleanAmount int
				row := db.QueryRow("SELECT amount FROM account WHERE id = ?", id)
				if err := row.Scan(&cleanAmount); err != nil {
					return errors.Wrap(err, "scan clean amount")
				}

				fmt.Println("clean amount:", cleanAmount)
				return nil
			},
		}, {
			"delete account",
			func() error {
				res, err := db.Exec("DELETE FROM account WHERE id = ?", id)
				if err != nil {
					return errors.Wrap(err, "exec delete")
				}

				rows, err := res.RowsAffected()
				if err != nil {
					return errors.Wrap(err, "get rows affected")
				}

				if rows == 0 {
					return errors.New("not found")
				}

				return nil
			},
		},
	}
}
