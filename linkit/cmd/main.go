package main

import (
	"context"
	"fmt"
	"linkit"

	"github.com/manifoldco/promptui"
	_ "github.com/mattn/go-sqlite3"
	"github.com/pkg/errors"
	"github.com/spf13/cobra"
)

func init() {
	addCmd.Flags().StringSliceP("tag", "t", []string{}, "link tag")
	rootCmd.AddCommand(addCmd)
}

var rootCmd = &cobra.Command{
	Use: "link",
	Run: func(cmd *cobra.Command, args []string) {
		fmt.Println("Hello Link!")
	},
}

var addCmd = &cobra.Command{
	Use:   "add",
	Short: "Add new link",
	Args:  cobra.ExactArgs(1),
	RunE: func(cmd *cobra.Command, args []string) error {
		config, err := linkit.LoadTOML("config.toml")
		if err != nil {
			return errors.WithMessage(err, "load config")
		}

		parser, err := linkit.NewParser(config.Network.Proxy)
		if err != nil {
			return errors.WithMessage(err, "new parser")
		}

		db, err := linkit.NewDB(config.DB)
		if err != nil {
			return errors.WithMessage(err, "new db")
		}

		url := args[0]
		fmt.Println("Parsing...")

		link, err := parser.Parse(url)
		if err != nil {
			return err
		}

		tags, err := cmd.LocalFlags().GetStringSlice("tag")
		if err != nil {
			return err
		}

		link.Tags = append(link.Tags, tags...)
		fmt.Println("URL: ", link.URL)
		fmt.Println("Title: ", link.Title)
		fmt.Println("Summary: ", link.Summary)
		fmt.Println("Tags: ", link.Tags)
		prompt := promptui.Select{
			Label: "Save?",
			Items: []string{"Yes", "No"},
		}
		_, res, err := prompt.Run()
		if err != nil {
			return err
		}
		if res != "Yes" {
			fmt.Println("User canceled")
			return nil
		}

		l, err := db.Link.
			Create().
			SetURL(link.URL).
			SetTitle(link.Title).
			SetSummary(link.Summary).
			SetTags(link.Tags).
			Save(context.Background())
		if err != nil {
			return errors.Wrap(err, "create link")
		}

		fmt.Println("New link added:", l.String())

		return nil
	},
}

func main() {
	rootCmd.Execute()
}
