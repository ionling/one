// Code generated by entc, DO NOT EDIT.

package ent

import (
	"linkit/ent/link"
	"linkit/ent/schema"
)

// The init function reads all schema descriptors with runtime
// code (default values, validators or hooks) and stitches it
// to their package variables.
func init() {
	linkFields := schema.Link{}.Fields()
	_ = linkFields
	// linkDescURL is the schema descriptor for url field.
	linkDescURL := linkFields[0].Descriptor()
	// link.DefaultURL holds the default value on creation for the url field.
	link.DefaultURL = linkDescURL.Default.(string)
	// linkDescTitle is the schema descriptor for title field.
	linkDescTitle := linkFields[1].Descriptor()
	// link.DefaultTitle holds the default value on creation for the title field.
	link.DefaultTitle = linkDescTitle.Default.(string)
	// linkDescSummary is the schema descriptor for summary field.
	linkDescSummary := linkFields[2].Descriptor()
	// link.DefaultSummary holds the default value on creation for the summary field.
	link.DefaultSummary = linkDescSummary.Default.(string)
}
