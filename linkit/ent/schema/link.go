package schema

import (
	"github.com/facebook/ent"
	"github.com/facebook/ent/schema/field"
)

// Link holds the schema definition for the Link entity.
type Link struct {
	ent.Schema
}

// Fields of the Link.
func (Link) Fields() []ent.Field {
	return []ent.Field{
		field.String("url").Default(""),
		field.String("title").Default(""),
		field.String("summary").Default(""),
		field.Strings("tags"),
	}
}

// Edges of the Link.
func (Link) Edges() []ent.Edge {
	return nil
}
