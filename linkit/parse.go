package linkit

import (
	"crypto/tls"
	"net/http"
	"net/url"
	"regexp"
	"strings"

	"github.com/pkg/errors"
)

type Link struct {
	Title   string
	URL     string
	Summary string
	Tags    []string
}

type Parser struct {
	client *http.Client
}

func NewParser(proxyUrl string) (*Parser, error) {
	proxy, err := url.Parse(proxyUrl)
	if err != nil {
		return nil, err
	}

	return &Parser{
		client: &http.Client{
			Transport: &http.Transport{
				Proxy:           http.ProxyURL(proxy),
				TLSClientConfig: &tls.Config{InsecureSkipVerify: true},
			},
		},
	}, nil
}

// Parse parses the given url.
func (p *Parser) Parse(url string) (l *Link, err error) {
	if strings.Contains(url, "github.com") {
		return p.parseGitHub(url)
	}

	err = errors.New("not supported url: " + url)
	return
}

func (p *Parser) parseGitHub(url string) (l *Link, err error) {
	tags := []string{"github"}
	resp, err := p.client.Get(url)
	if err != nil {
		return
	}
	regex := `<title>(.+)<\/title>`
	reg := regexp.MustCompile(regex)
	body := make([]byte, 10*1024)
	_, err = resp.Body.Read(body)
	if err != nil {
		err = errors.Wrap(err, "read body")
		return
	}
	matches := reg.FindStringSubmatch(string(body))
	if len(matches) < 2 {
		err = errors.New("parse error: " + url)
		return
	}
	splits := strings.Split(matches[1], ": ")
	l = &Link{
		Title:   splits[0],
		URL:     url,
		Summary: splits[1],
		Tags:    tags,
	}
	return
}
