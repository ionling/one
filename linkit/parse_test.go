package linkit

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

func TestParserParse(t *testing.T) {
	type args struct {
		url string
	}
	tests := []struct {
		name    string
		args    args
		wantL   *Link
		wantErr bool
	}{
		{
			"GitHub",
			args{"https://github.com/gnebbia/kb"},
			&Link{
				URL:     "https://github.com/gnebbia/kb",
				Title:   "GitHub - gnebbia/kb",
				Summary: "A minimalist command line knowledge base manager",
				Tags:    []string{"github"},
			},
			false,
		},
	}

	config, err := LoadTOML("config.test.toml")
	require.NoError(t, err)

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			p, err := NewParser(config.Network.Proxy)
			assert.NoError(t, err)
			gotL, err := p.Parse(tt.args.url)
			assert.True(t, tt.wantErr == (err != nil))
			assert.Equal(t, tt.wantL, gotL)
		})
	}
}
