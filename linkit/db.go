package linkit

import (
	"context"

	"github.com/pkg/errors"

	"linkit/ent"
)

func NewDB(c DBConfig) (*ent.Client, error) {
	db, err := ent.Open(c.Driver, c.DSN)
	if err != nil {
		return nil, errors.Wrap(err, "open")
	}

	// Run the auto migration tool
	if err = db.Schema.Create(context.Background()); err != nil {
		return nil, errors.Wrap(err, "create")
	}
	return db, nil
}
