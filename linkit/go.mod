module linkit

go 1.15

require (
	github.com/facebook/ent v0.4.3
	github.com/knadh/koanf v0.14.0
	github.com/manifoldco/promptui v0.7.0
	github.com/mattn/go-sqlite3 v1.14.3
	github.com/mitchellh/mapstructure v1.4.0 // indirect
	github.com/pkg/errors v0.9.1
	github.com/spf13/cobra v1.0.0
	github.com/stretchr/testify v1.6.1
)
