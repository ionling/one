package linkit

import (
	"github.com/knadh/koanf"
	"github.com/knadh/koanf/parsers/toml"
	"github.com/knadh/koanf/providers/file"
	"github.com/pkg/errors"
)

var (
	k = koanf.New(".")
)

type Config struct {
	DB      DBConfig      `koanf:"db"`
	Network NetworkConfig `koanf:"network"`
}

type DBConfig struct {
	Driver string `koanf:"driver"`
	DSN    string `koanf:"dsn"`
}

type NetworkConfig struct {
	Proxy string `koanf:"proxy"`
}

func LoadTOML(path string) (*Config, error) {
	if err := k.Load(file.Provider(path), toml.Parser()); err != nil {
		return nil, errors.Wrap(err, "load")
	}

	conf := new(Config)
	if err := k.Unmarshal("", conf); err != nil {
		return nil, errors.Wrap(err, "unmarshal")
	}
	return conf, nil
}
