package bench

import "sync"

type ArrayPool struct {
	lock  sync.Mutex
	items []interface{}
	New   func() interface{}
}

func (p *ArrayPool) Get() (res any) {
	p.lock.Lock()
	defer p.lock.Unlock()

	if len(p.items) == 0 {
		return p.New()
	}

	res = p.items[0]
	p.items = p.items[1:] //nolint:staticcheck
	return
}

func (p *ArrayPool) Put(item any) {
	p.lock.Lock()
	defer p.lock.Unlock()
	p.items = append(p.items, item) //nolint:staticcheck
}

type ChanPool struct {
	Chan chan any
	New  func() any
}

func (p *ChanPool) Get() any {
	select {
	case it := <-p.Chan:
		return it
	default:
		return p.New()
	}
}

func (p *ChanPool) Put(it any) {
	p.Chan <- it
}
