package bench

import (
	"encoding/json"
	"sync"
	"testing"
)

type Student struct {
	Name string
	Age  int
}

var (
	buf, _ = json.Marshal(Student{Name: "Geektutu", Age: 25})
	newStu = func() interface{} {
		return new(Student)
	}
)

func BenchmarkNoPool(b *testing.B) {
	b.RunParallel(func(p *testing.PB) {
		for p.Next() {
			stu := new(Student)
			_ = json.Unmarshal(buf, stu)
		}
	})
}

func BenchmarkSyncPool(b *testing.B) {
	p := sync.Pool{New: newStu}
	b.RunParallel(func(pb *testing.PB) {
		for pb.Next() {
			stu := p.Get().(*Student)
			_ = json.Unmarshal(buf, stu)
			p.Put(stu)
		}
	})
}

func BenchmarkArrayPool(b *testing.B) {
	p := ArrayPool{New: newStu}
	b.RunParallel(func(pb *testing.PB) {
		for pb.Next() {
			stu := p.Get().(*Student)
			_ = json.Unmarshal(buf, stu)
			p.Put(stu)
		}
	})
}

func BenchmarkChanPool(b *testing.B) {
	p := ChanPool{
		Chan: make(chan any, 100),
		New:  newStu,
	}
	b.RunParallel(func(pb *testing.PB) {
		for pb.Next() {
			stu := p.Get().(*Student)
			_ = json.Unmarshal(buf, stu)
			p.Put(stu)
		}
	})
}
