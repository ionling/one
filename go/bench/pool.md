# Pool

There isn't much difference with or without the pool,
perhaps the resource-intensive part is the `json.Unmarshal` operation.

## NoPool

```
Running tool: /usr/bin/go test -benchmem -run=^$ -bench ^BenchmarkNoPool$ bench -v

goos: linux
goarch: amd64
pkg: bench
cpu: Intel(R) Core(TM) i5-10400 CPU @ 2.90GHz
BenchmarkUnmarshalWithoutPool
BenchmarkUnmarshalWithoutPool-12    	 8466172	       143.4 ns/op	     256 B/op	       7 allocs/op
PASS
ok  	bench	1.361s
```

## `sync.Pool`

```
Running tool: /usr/bin/go test -benchmem -run=^$ -bench ^BenchmarkSyncPool$ bench -v

goos: linux
goarch: amd64
pkg: bench
cpu: Intel(R) Core(TM) i5-10400 CPU @ 2.90GHz
BenchmarkUnmarshalWithPool
BenchmarkUnmarshalWithPool-12    	 8587626	       139.5 ns/op	     232 B/op	       6 allocs/op
PASS
ok  	bench	1.343s
```

## ArrayPool

```
Running tool: /usr/bin/go test -benchmem -run=^$ -bench ^BenchmarkArrayPool$ bench -v

goos: linux
goarch: amd64
pkg: bench
cpu: Intel(R) Core(TM) i5-10400 CPU @ 2.90GHz
BenchmarkUnmarshalWithMyPool
BenchmarkUnmarshalWithMyPool-12    	 2823520	       418.3 ns/op	     263 B/op	       6 allocs/op
PASS
ok  	bench	1.614s
```

## ChanPool

```
Running tool: /usr/bin/go test -benchmem -run=^$ -bench ^BenchmarkChanPool$ bench -v

goos: linux
goarch: amd64
pkg: bench
cpu: Intel(R) Core(TM) i5-10400 CPU @ 2.90GHz
BenchmarkChanPool
BenchmarkChanPool-12    	 3729578	       274.9 ns/op	     232 B/op	       6 allocs/op
PASS
ok  	bench	1.355s
```
