package bench

import (
	"context"
	"net"
	"testing"
	"time"
)

type DNSServer struct {
	Name string
	Addr string
}

func BenchmarkDNS(b *testing.B) {
	// The iteration of map is unordered, so we use array.
	hosts := []string{"baidu.com", "cowtransfer.com", "segmentfault.com"}
	servers := []DNSServer{
		{"AdobaOffice", "192.168.2.1:53"},
		{"AliDNS", "223.5.5.5:53"},
		{"DNSPod", "119.29.29.29:53"},
		{"Google", "8.8.8.8:53"},
		{"OpenDNS", "208.67.222.222:5353"},
		{"Cloudflare", "1.0.0.1:53"},
	}
	for _, s := range servers {
		b.Run(s.Name, func(b *testing.B) {
			r := net.Resolver{
				PreferGo: true,
				Dial: func(ctx context.Context, network, address string) (net.Conn, error) {
					d := net.Dialer{
						Timeout: 6 * time.Second,
					}
					return d.DialContext(ctx, network, s.Addr)
				},
			}
			for _, host := range hosts {
				b.Run(host, func(b *testing.B) {
					for i := 0; i < b.N; i++ {
						_, err := r.LookupHost(context.Background(), host)
						if err != nil {
							b.Log("addr:", s.Addr, "err:", err)
							b.Fail()
						}
					}
				})
			}
		})
	}
}
