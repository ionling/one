package draft

import (
	"context"
	"errors"
	"fmt"
	"log/slog"

	clientv3 "go.etcd.io/etcd/client/v3"
	"go.etcd.io/etcd/client/v3/concurrency"
)

type Handler func(ctx context.Context) error

type Middleware func(Handler) Handler

func Idempotent(c *clientv3.Client, lockPrefix string, ttl int, l *slog.Logger) Middleware {
	return func(next Handler) Handler {
		return func(ctx context.Context) error {
			sess, err := concurrency.NewSession(c, concurrency.WithTTL(ttl))
			if err != nil {
				return fmt.Errorf("new session: %w", err)
			}
			sess.Orphan()
			mu := concurrency.NewMutex(sess, "/lock/"+lockPrefix)
			if err := mu.TryLock(ctx); err != nil {
				if errors.Is(err, concurrency.ErrLocked) {
					l = l.With("middleware", "Idempotent")
					l.DebugContext(ctx, "another session exists, skip")
					return nil
				}
				return fmt.Errorf("lock: %w", err)
			}
			return next(ctx)
		}
	}
}
