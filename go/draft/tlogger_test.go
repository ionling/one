package draft

import (
	"testing"

	"github.com/go-kratos/kratos/v2/log"
)

func TestTLogger(t *testing.T) {
	// It's hard to make `t.Log()` get right file locations
	l := NewTLogger(t)
	_ = l.Log(log.LevelDebug, "hello")
	lh := log.NewHelper(l)
	lh.Info("hello")
	lh.Debug("The greatest glory in living lies not in never falling")
	lh.Info("The way to get started is to quit talking and begin doing")
	lh.Warn("Your time is limited, so don't waste it living someone else's life")
	lh.Error("If you look at what you have in life, you'll always have more")
}
