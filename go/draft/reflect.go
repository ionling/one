package draft

import (
	"reflect"
	"strings"
)

// IndirectKind retrieves the kind of a [reflect.Type] by recursively dereferencing it
// until it is no longer a pointer.
func IndirectKind(t reflect.Type) reflect.Kind {
	if k := t.Kind(); k != reflect.Pointer {
		return k
	}

	return IndirectKind(t.Elem())
}

// DRAFT: It's too trouble to support all kinds of types and nested types,
// such as map of generic struct to int.
// Additionally, there are no strong requirements for this func.

// TypeFullNameWithoutTypeParams returns the package path and name of type t concatenated by a dot
// without type parameters.
//
// If t is [reflect.Func], returns nothing.
func TypeFullNameWithoutTypeParams(t reflect.Type) string {
	if t.Kind() == reflect.Pointer {
		return "*" + TypeFullNameWithoutTypeParams(t.Elem())
	}
	name := t.Name()
	if name == "" {
		return t.String()
	}
	pkgPath := t.PkgPath()
	i := strings.Index(name, "[")
	if i != -1 {
		name = name[:i]
	}
	if pkgPath == "" {
		return name
	}
	return pkgPath + "." + name
}
