package draft

import (
	"reflect"
	"testing"
	"time"
)

func TestIndirectKind(t *testing.T) {
	var (
		str   = "string"
		strp  = &str
		strpp = &strp
		slc   = []int{1, 2, 3}
		slcp  = &slc
		slcpp = &slcp
		ch    = make(chan struct{})
		chp   = &ch
		chpp  = &chp
	)
	tests := []struct {
		name string
		args reflect.Type
		want reflect.Kind
	}{
		{"string", reflect.TypeOf(str), reflect.String},
		{"*string", reflect.TypeOf(strp), reflect.String},
		{"**string", reflect.TypeOf(strpp), reflect.String},
		{"slice", reflect.TypeOf(slc), reflect.Slice},
		{"*slice", reflect.TypeOf(slcp), reflect.Slice},
		{"**slice", reflect.TypeOf(slcpp), reflect.Slice},
		{"channel", reflect.TypeOf(ch), reflect.Chan},
		{"*channel", reflect.TypeOf(chp), reflect.Chan},
		{"**channel", reflect.TypeOf(chpp), reflect.Chan},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := IndirectKind(tt.args); got != tt.want {
				t.Errorf("IndirectKind() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestTypeFullNameWithoutTypeParams(t *testing.T) {
	type Hello[T any] struct {
		Name T
	}
	type World[T, P any] struct {
		Name  T
		Place P
	}
	var (
		tm   = time.Now() // TiMe
		ptm  = &tm
		pptm = &ptm

		si  = []int{1, 2, 3} // Slice of Int
		psi = &si            // Pointer

		cb  = make(chan bool) // Channel of Bool
		pcb = &cb

		mii  = make(map[int]int) // Map of Int to Int
		pmii = &mii
		msi  = make(map[Hello[int]]int) // Map of Struct to Int

		hs  = Hello[string]{}
		phs = &hs

		wh  = World[Hello[int], map[string]int]{}
		pwh = &World[Hello[int], map[string]int]{}
	)
	tests := []struct {
		args any
		want string
	}{
		{9, "int"},
		{"mkdocs", "string"},
		{tm, "time.Time"},
		{ptm, "*time.Time"},
		{pptm, "**time.Time"},
		{si, "[]int"},
		{psi, "*[]int"},
		{mii, "map[int]int"},
		{pmii, "*map[int]int"},
		{msi, "map[draft.Hello[int]]int"},
		{cb, "chan bool"},
		{pcb, "*chan bool"},
		{hs, "draft.Hello"},
		{phs, "*draft.Hello"},
		{wh, "draft.World"},
		{pwh, "*draft.World"},
		{TypeFullNameWithoutTypeParams, "func(reflect.Type) string"},
	}
	for _, tt := range tests {
		tp := reflect.TypeOf(tt.args)
		t.Run(tt.want, func(t *testing.T) {
			if got := TypeFullNameWithoutTypeParams(tp); got != tt.want {
				t.Errorf("TypeFullNameWithoutTypeParams() = %v, want %v", got, tt.want)
			}
		})
	}
}
