package draft

import (
	"bytes"
	"fmt"
	"testing"

	"github.com/go-kratos/kratos/v2/log"
)

type tlogger struct {
	t *testing.T
}

// Log logs the key-value pairs.
// It refers to [log.NewStdLogger].
func (l *tlogger) Log(level log.Level, kvs ...any) error {
	if len(kvs) == 0 {
		return nil
	}
	if (len(kvs) & 1) == 1 {
		kvs = append(kvs, "UNPAIRED")
	}
	buf := bytes.Buffer{}
	buf.WriteString(level.String())
	for i := 0; i < len(kvs); i += 2 {
		_, _ = fmt.Fprintf(&buf, " %s=%v", kvs[i], kvs[i+1])
	}
	l.t.Helper()
	l.t.Log(buf.String())
	return nil
}

func NewTLogger(t *testing.T) log.Logger {
	return &tlogger{t}
}
