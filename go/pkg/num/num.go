package num

import (
	"fmt"
)

func ReadableFloor(n int64) string {
	type unit struct {
		N int64
		S string
	}
	for _, u := range []unit{
		{10000, "W"},
		{1000, "K"},
	} {
		if n >= u.N {
			return fmt.Sprintf("%d%s", n/u.N, u.S)
		}
	}

	return fmt.Sprintf("%d", n)
}
