package num

import "testing"

func TestReadableFloor(t *testing.T) {
	type args struct {
		n int64
	}
	tests := []struct {
		name string
		args args
		want string
	}{
		{"690", args{690}, "690"},
		{"3106", args{3106}, "3K"},
		{"3K", args{3000}, "3K"},
		{"8K", args{8000}, "8K"},
		{"1W", args{10000}, "1W"},
		{"100W", args{1000000}, "100W"},
		{"2000W", args{20000000}, "2000W"},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := ReadableFloor(tt.args.n); got != tt.want {
				t.Errorf("readableNum() = %v, want %v", got, tt.want)
			}
		})
	}
}
