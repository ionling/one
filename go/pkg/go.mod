module pkg

go 1.16

require (
	github.com/go-redis/redis v6.15.9+incompatible
	github.com/google/uuid v1.2.0
	github.com/onsi/ginkgo v1.15.2 // indirect
	github.com/onsi/gomega v1.11.0 // indirect
	github.com/pkg/errors v0.9.1
	github.com/qiniu/go-sdk/v7 v7.9.5
	github.com/stretchr/testify v1.7.0
)
