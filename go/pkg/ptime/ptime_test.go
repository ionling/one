package ptime

import (
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
)

func TestDate(t *testing.T) {
	t.Log("today", TodayDate())
	t.Log("yesterday", YesterdayDate())

	in := time.Date(2020, 2, 18, 21, 45, 9, 34, time.Local)
	expected := time.Date(2020, 2, 18, 0, 0, 0, 0, time.Local)
	assert.Equal(t, expected, Date(in))

	out2 := Date(time.Now())
	assert.Equal(t, 0, out2.Hour())
	assert.Equal(t, 0, out2.Minute())
	assert.Equal(t, 0, out2.Second())
}

func TestDateEqual(t *testing.T) {
	type args struct {
		d1 time.Time
		d2 time.Time
	}

	shTz, err := time.LoadLocation("Asia/Shanghai")
	if err != nil {
		t.Fatal(err)
	}

	tests := []struct {
		name string
		args args
		want bool
	}{
		{
			"same time",
			args{
				time.Date(2001, 3, 4, 5, 6, 7, 8, time.Local),
				time.Date(2001, 3, 4, 5, 6, 7, 8, time.Local),
			},
			true,
		},
		{
			"same date",
			args{
				time.Date(2001, 3, 4, 8, 6, 7, 9, time.Local),
				time.Date(2001, 3, 4, 5, 6, 7, 8, time.Local),
			},
			true,
		},
		{
			"different date",
			args{
				time.Date(2001, 3, 4, 8, 6, 7, 9, time.Local),
				time.Date(2001, 3, 8, 5, 6, 7, 8, time.Local),
			},
			false,
		},
		{
			"UTCDate() equal",
			args{
				time.Date(2001, 3, 8, 8, 6, 7, 9, time.UTC),
				UTCDate(2001, 3, 8),
			},
			true,
		},
		{
			"UTCDate() not equal",
			args{
				time.Date(2001, 3, 4, 8, 6, 7, 9, time.UTC),
				UTCDate(2001, 3, 8),
			},
			false,
		},
		{
			"different timezone",
			args{
				time.Date(2001, 3, 8, 2, 6, 7, 9, shTz),
				UTCDate(2001, 3, 8),
			},
			true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := DateEqual(tt.args.d1, tt.args.d2); got != tt.want {
				t.Errorf("DateEqual() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestRandomTime(t *testing.T) {
	type args struct {
		start time.Time
		stop  time.Time
	}
	tests := []struct {
		name string
		args args
	}{{
		"one",
		args{time.Unix(12345, 0), time.Unix(92345, 0)},
	}, {
		"two",
		args{time.Unix(-777895, 0), time.Unix(-8123, 0)},
	}, {
		"three",
		args{time.Unix(-1234, 0), time.Unix(1234, 0)},
	}}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			start, stop := tt.args.start, tt.args.stop
			got := RandomTime(tt.args.start, tt.args.stop)
			if got.Before(start) {
				t.Errorf("RandomTime() = %v, lt %v", got, start)
			} else if got == stop || got.After(stop) {
				t.Errorf("RandomTime() = %v, gte %v", got, stop)
			}
		})
	}
}

func TestThisMonth(t *testing.T) {
	t.Log(ThisMonth())
}

func TestEndOfDay(t *testing.T) {
	in := time.Date(2020, 2, 18, 21, 45, 9, 34, time.Local)
	expected := time.Date(2020, 2, 18, 23, 59, 59, 999999999, time.Local)
	assert.Equal(t, expected, EndOfDay(in))

	out2 := EndOfDay(time.Now())
	assert.Equal(t, 23, out2.Hour())
	assert.Equal(t, 59, out2.Minute())
	assert.Equal(t, 59, out2.Second())
}

func TestUtilDays(t *testing.T) {
	t1 := time.Date(2020, 2, 18, 21, 45, 9, 34, time.Local)
	t2 := time.Date(2020, 2, 28, 11, 45, 9, 34, time.Local)
	assert.Equal(t, 10, UntilDays(t1, t2))

	t3 := time.Now().AddDate(0, 0, 9)
	assert.Equal(t, 9, NowUntilDays(t3))
}
