package ptime

import (
	"math/rand"
	"time"
)

const (
	Day = 24 * time.Hour
)

// Date truncate the time part, keep only the date.
// e.g., 2020-1-20 21:30:19 -> 2020-1-20 00:00:00
func Date(t time.Time) time.Time {
	return StartOfDay(t)
}

func UTCDate(year, month, day int) time.Time {
	return time.Date(year, time.Month(month), day, 0, 0, 0, 0, time.UTC)
}

func TodayDate() time.Time {
	y, m, d := time.Now().Date()
	return time.Date(y, m, d, 0, 0, 0, 0, time.Local)
}

func YesterdayDate() time.Time {
	return TodayDate().AddDate(0, 0, -1)
}

func DateEqual(d1 time.Time, d2 time.Time) bool {
	d1y, d1m, d1d := d1.Date()
	d2y, d2m, d2d := d2.Date()
	return d1y == d2y &&
		d1m == d2m &&
		d1d == d2d
}

func RandomTime(start, stop time.Time) time.Time {
	startu, stopu := start.Unix(), stop.Unix()
	n := rand.Int63n(stopu - startu)
	return time.Unix(n+startu, 0)
}

// ThisMonths returns `time.Now()` without day, hour, min...
func ThisMonth() time.Time {
	y, m, _ := time.Now().Date()
	return time.Date(y, m, 0, 0, 0, 0, 0, time.Local)
}

// StartOfDay returns the start time of a day.
// e.g., 2020-1-20 21:30:19 -> 2020-1-20 00:00:00
func StartOfDay(t time.Time) time.Time {
	return time.Date(t.Year(), t.Month(), t.Day(), 0, 0, 0, 0, t.Location())
}

// EndOfDay returns the end time of a day.
// e.g., 2020-1-20 21:30:19.123456789 -> 2020-1-20 23:59:59.999999999
func EndOfDay(t time.Time) time.Time {
	return StartOfDay(t).AddDate(0, 0, 1).Add(-time.Nanosecond)
}

// UntilDays returns the number of days from the t until u.
// Only the date part will be retained before calculation.
func UntilDays(f time.Time, u time.Time) int {
	return int(Date(u).Sub(Date(f)).Hours()) / 24
}

// NowUntilDays returns the number of days from the current time until t.
// Only the date part will be retained before calculation.
func NowUntilDays(t time.Time) int {
	return int(Date(t).Sub(Date(time.Now())).Hours()) / 24
}
