package mq

import (
	"time"

	"github.com/go-redis/redis"
	"github.com/pkg/errors"
)

type Idempotentor struct {
	r          *redis.Client
	expiration time.Duration
	prefix     string
}

func NewIdempotentor(
	r *redis.Client,
	expiration time.Duration,
	prefix string,
) *Idempotentor {
	return &Idempotentor{
		r:          r,
		expiration: expiration,
		prefix:     prefix,
	}
}

func (i Idempotentor) Check(key string) (ok bool, err error) {
	err = i.r.Get(i.prefix + key).Err()
	if err == redis.Nil {
		return true, nil
	}
	return false, errors.Wrap(err, "get")
}

func (i Idempotentor) Mark(key string) (err error) {
	ok, err := i.r.SetNX(i.prefix+key, "", i.expiration).Result()
	if err != nil {
		return
	}
	if !ok {
		err = errors.New("failed")
	}
	return
}
