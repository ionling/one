package mq

import (
	"testing"
	"time"

	"github.com/go-redis/redis"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

func TestIdempotentor(t *testing.T) {
	opt, err := redis.ParseURL("redis://localhost:6379/1")
	if err != nil {
		panic(err)
	}

	var (
		id = "2"
		i  = NewIdempotentor(redis.NewClient(opt), 4*time.Second, "key:")
	)

	ok, err := i.Check(id)
	require.NoError(t, err)
	assert.True(t, ok)

	err = i.Mark(id)
	require.NoError(t, err)

	err = i.Mark(id)
	require.EqualError(t, err, "failed")

	ok, err = i.Check(id)
	require.NoError(t, err)
	assert.False(t, ok)
}
