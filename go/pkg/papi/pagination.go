package apiutil

// PageToOffset convert page-size style pagination to limit-offset style.
func PageToOffset(page uint, pageSize uint) (limit uint, offset uint) {
	return PageToOffsetDefault(page, pageSize, 20)
}

func PageToOffsetDefault(
	page uint,
	pageSize uint,
	defaultSize uint,
) (limit uint, offset uint) {
	if page == 0 {
		page = 1
	}

	if pageSize == 0 {
		pageSize = defaultSize
	}

	limit = pageSize
	offset = (page - 1) * pageSize
	return
}
