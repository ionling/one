package apiutil

import (
	"testing"
)

func TestPageToOffset(t *testing.T) {
	type args struct {
		page     uint
		pageSize uint
	}
	tests := []struct {
		name       string
		args       args
		wantLimit  uint
		wantOffset uint
	}{
		{name: "0", args: args{}, wantLimit: 20, wantOffset: 00},
		{name: "1", args: args{1, 20}, wantLimit: 20, wantOffset: 0},
		{name: "2", args: args{2, 16}, wantLimit: 16, wantOffset: 16},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			gotLimit, gotOffset := PageToOffset(tt.args.page, tt.args.pageSize)
			if gotLimit != tt.wantLimit {
				t.Errorf(
					"PageToOffset() gotLimit = %v, want %v",
					gotLimit,
					tt.wantLimit,
				)
			}
			if gotOffset != tt.wantOffset {
				t.Errorf(
					"PageToOffset() gotOffset = %v, want %v",
					gotOffset,
					tt.wantOffset,
				)
			}
		})
	}
}
