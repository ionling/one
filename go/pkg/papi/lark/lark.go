package lark

import (
	"crypto/hmac"
	"crypto/sha256"
	"encoding/base64"
	"fmt"

	"pkg/phttp"
)

type Resp struct {
	Code int
	Msg  string
	Data interface{}
}

func (r Resp) Error() string {
	return fmt.Sprintf("%d, %s, %v", r.Code, r.Msg, r.Data)
}

type BotReq struct {
	Timestamp int64       `json:"timestamp"`
	Sign      string      `json:"sign"`
	MsgType   string      `json:"msg_type"`
	Card      interface{} `json:"card"`
}

func BotSendMsg(url string, req BotReq) (resp Resp, err error) {
	err = phttp.PostJSONI(url, req, &resp)
	return
}

func MustBotSign(secret string, timestamp int64) string {
	// timestamp + key 做 sha256, 再进行 base64 encode
	s := fmt.Sprintf("%v\n%s", timestamp, secret)
	h := hmac.New(sha256.New, []byte(s))
	var data []byte
	_, err := h.Write(data)
	if err != nil {
		panic(err)
	}
	return base64.StdEncoding.EncodeToString(h.Sum(nil))
}
