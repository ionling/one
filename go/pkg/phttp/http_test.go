package phttp

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

func TestPostJSON(t *testing.T) {
	type User struct {
		Name string
	}
	type Resp struct {
		JSON User
	}

	u := User{Name: "Hello"}
	var resp Resp
	err := PostJSONI("https://httpbin.org/post", u, &resp)
	require.NoError(t, err)
	assert.Equal(t, u, resp.JSON)

	err = PostJSONS("https://httpbin.org/post", `{"Name":"Hello"}`, &resp)
	require.NoError(t, err)
	assert.Equal(t, u, resp.JSON)
}
