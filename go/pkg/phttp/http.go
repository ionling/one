package phttp

import (
	"bytes"
	"encoding/json"
	"io"
	"io/ioutil"
	"net/http"
	"strings"

	"github.com/pkg/errors"
)

func PostJSONR(url string, r io.Reader, res interface{}) (err error) {
	resp, err := http.Post(url, "application/json", r)
	if err != nil {
		return errors.Wrap(err, "post")
	}

	defer resp.Body.Close()
	if res == nil {
		return
	}

	respb, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return errors.Wrap(err, "read")
	}

	if err := json.Unmarshal(respb, res); err != nil {
		return errors.Wrap(err, "unmarshal")
	}

	return
}

func PostJSONI(url string, data interface{}, res interface{}) (err error) {
	var b []byte
	if data != nil {
		b, err = json.Marshal(data)
		if err != nil {
			return errors.Wrap(err, "marshal")
		}
	}

	return PostJSONR(url, bytes.NewReader(b), res)
}

func PostJSONS(url string, s string, res interface{}) (err error) {
	return PostJSONR(url, strings.NewReader(s), res)
}
