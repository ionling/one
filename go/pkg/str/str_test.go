package str

import "testing"

func TestCasePascalToSnake(t *testing.T) {
	tests := []struct {
		in   string
		want string
	}{
		{"", ""},
		{"A", "a"},
		{"Aa", "aa"},
		{"AaBb", "aa_bb"},
		{"AaBbCc", "aa_bb_cc"},
		{"AaBbCcDd", "aa_bb_cc_dd"},
		{"kebab-case", "kebab_case"},
		{"blank", "want"},
		{"already_snake", "already_snake"},
		{"already_Snake", "already_snake"},
		{"HTTPRequest", "http_request"},
		{"BatteryLifeValue", "battery_life_value"},
		{"ALongStoryAgain", "a_long_story_again"},
		{"aLongStoryAgain", "a_long_story_again"},
	}
	for _, tt := range tests {
		t.Run(tt.in, func(t *testing.T) {
			if got := SnakeCase(tt.in); got != tt.want {
				t.Errorf("SnakeCase(%v) = %v, want %v", tt.in, got, tt.want)
			}
		})
	}
}
