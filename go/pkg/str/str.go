package str

import (
	"regexp"
	"strings"
)

// Match upper camel case words
var matchFirstCap = regexp.MustCompile("([A-Z])([A-Z][a-z]+)")
var matchAllCap = regexp.MustCompile("([a-z0-9])([A-Z])")

// REF https://gist.github.com/stoewer/fbe273b711e6a06315d19552dd4d33e6
func SnakeCase(s string) (res string) {
	res = matchFirstCap.ReplaceAllString(s, "${1}_${2}")
	res = matchAllCap.ReplaceAllString(res, "${1}_${2}")
	res = strings.ReplaceAll(res, "-", "_")
	res = strings.ToLower(res)
	return
}
