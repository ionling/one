package qiniu

import (
	"context"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestPutFileResume(t *testing.T) {
	conf := PutConfig{
		Credential: Credential{
			AccessKey: "j_oLlWOxeyh9H88qHPpiVpPdA8jvbJmTl1F_O1ek",
			SecretKey: "az4ouWRGGBXv491rDokOItx9ibg3_wGHPnubcMUb",
		},
		Bucket: Bucket{
			Domain: "https://img.os.fire.com",
			Name:   "img",
		},
	}
	url, err := PutFileResumeWithoutKey(context.Background(), conf, "qiniu.go")
	assert.NoError(t, err)
	t.Log("file url:", url)
	// file url: https://img.os.fire.com/f628773f-0a12-449b-9a12-d6bd0b4680e2.go
}
