package qiniu

import (
	"context"
	"path/filepath"

	"github.com/google/uuid"
	"github.com/qiniu/go-sdk/v7/auth"
	"github.com/qiniu/go-sdk/v7/storage"
)

type PutConfig struct {
	Credential
	Bucket
}

type Credential struct {
	AccessKey string
	SecretKey string
}

type Bucket struct {
	Name   string
	Domain string
}

func PutFileResume(
	ctx context.Context,
	conf PutConfig,
	key string,
	file string,
) (url string, err error) {
	var (
		cfg = storage.Config{
			UseHTTPS: true,
		}
		policy = storage.PutPolicy{
			Scope: conf.Bucket.Name,
		}
		credentials = auth.New(conf.AccessKey, conf.SecretKey)

		ret      = storage.PutRet{}
		token    = policy.UploadToken(credentials)
		uploader = storage.NewResumeUploaderV2(&cfg)
	)

	err = uploader.PutFile(ctx, &ret, token, key, file, nil)
	if err != nil {
		return
	}

	url = conf.Domain + "/" + ret.Key

	return
}

func PutFileResumeWithoutKey(
	ctx context.Context,
	conf PutConfig,
	file string,
) (url string, err error) {
	key := uuid.New().String() + filepath.Ext(file)
	return PutFileResume(ctx, conf, key, file)
}
