# Goa demo

REF: [Getting Started](https://goa.design/learn/getting-started/)

## Commands

```sh
# Init go project
mkdir -p calc/design
cd calc
go mod init calc


# Design
vim calc/design/design.go


# Code generation
goa gen calc/design
goa example calc/design


# Implementation
vim calc.go


# Building and running the service
go build ./cmd/calc && go build ./cmd/calc-cli

## Run the server
./calc

## Run the client
./calc-cli --url="http://localhost:8000" calc add --a 1 --b 2
./calc-cli --url="grpc://localhost:8080" calc add --message '{"a": 1, "b": 2}'
```
