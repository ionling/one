package dbase

import (
	"log/slog"

	"imptest/dbase/sqlite"
)

func NewPostgres() {
	slog.Info("new postgres")
}

func NewSQLite() {
	slog.Info("new sqlite")
	sqlite.New()
}
