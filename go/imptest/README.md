# imptest

imptest is an abbreviation of "import test".

## Result

Got output:

```txt
2024/02/27 11:28:58 INFO init in sqlite
2024/02/27 11:28:58 INFO new postgres
2024/02/27 11:28:58 INFO hello
```

This means the unused `NewSQLite()` function is included in the final compiled binary.
