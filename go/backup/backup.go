package backup

import (
	"compress/flate"
	"context"
	"crypto/aes"
	"crypto/cipher"
	"fmt"
	"io"
	"log/slog"

	"go.uber.org/multierr"
)

type Backup struct {
	conf *BackupConf
	l    *slog.Logger
}

func New(c *BackupConf, l *slog.Logger) *Backup {
	return &Backup{
		conf: c,
		l:    l,
	}
}

func (b *Backup) Do(ctx context.Context, r io.Reader, w io.Writer) (err error) {
	xor, err := b.cipher()
	if err != nil {
		return fmt.Errorf("xor: %w", err)
	}

	sw := cipher.StreamWriter{
		S: xor,
		W: w,
	}
	defer multierr.AppendFunc(&err, sw.Close)
	fw, err := flate.NewWriter(sw, flate.BestCompression)
	if err != nil {
		return fmt.Errorf("new writer: %w", err)
	}
	defer multierr.AppendFunc(&err, fw.Close)

	_, err = io.Copy(fw, r)
	return err
}

func (b *Backup) Undo(ctx context.Context, r io.Reader, w io.Writer) error {
	xor, err := b.cipher()
	if err != nil {
		return fmt.Errorf("xor: %w", err)
	}

	sr := cipher.StreamReader{
		S: xor,
		R: r,
	}
	fr := flate.NewReader(sr)
	defer multierr.AppendFunc(&err, fr.Close)

	_, err = io.Copy(w, fr)
	return err
}

func (b *Backup) cipher() (res cipher.Stream, err error) {
	block, err := aes.NewCipher([]byte(b.conf.Key))
	if err != nil {
		return nil, fmt.Errorf("new cipher: %w", err)
	}

	res = cipher.NewOFB(block, []byte(b.conf.IV))
	return
}
