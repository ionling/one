package backup

import (
	"encoding/json"
	"fmt"
	"os"

	"github.com/aws/aws-sdk-go-v2/credentials"
	"github.com/aws/aws-sdk-go-v2/service/s3"
)

type Conf struct {
	Backup BackupConf
	S3     []*S3Conf
}

type BackupConf struct {
	Key string
	IV  string
}

type S3Conf struct {
	Name,
	Endpoint, Region,
	Key, Secret,
	Bucket string
}

func (c *Conf) Validate() error {
	if l := len(c.Backup.Key); l != 32 {
		return fmt.Errorf("len(Key) should be 32, got %d", l)
	}
	if l := len(c.Backup.IV); l != 16 {
		return fmt.Errorf("len(IV) should be 16, got %d", l)
	}
	return nil
}

func LoadConfig() (*Conf, error) {
	f, err := os.ReadFile("config.json")
	if err != nil {
		return nil, fmt.Errorf("read file: %w", err)
	}
	c := Conf{}
	if err := json.Unmarshal(f, &c); err != nil {
		return nil, fmt.Errorf("unmarshal: %w", err)
	}
	return &c, nil
}

func (c *S3Conf) NewClient() *s3.Client {
	cred := credentials.NewStaticCredentialsProvider(c.Key, c.Secret, "")
	return s3.New(s3.Options{
		BaseEndpoint: &c.Endpoint,
		Credentials:  cred,
		Region:       c.Region,
	})
}
