package main

import (
	"context"
	"flag"
	"fmt"
	"os"

	"log/slog"

	"go.uber.org/multierr"

	"backup"
)

var (
	do    = flag.String("do", "encrypt", "encrypt/decrypt")
	input = flag.String("input", "", `Input location:
    - hello.enc               local file
    - s3://R2/hello.enc       R2 S3 storage
    - s3://all/hello.enc      all S3 storage`)
	output = flag.String("output", "", `Output location: same as input`)
)

type Do string

const (
	DoEncrypt Do = "encrypt"
	DoDecrypt Do = "decrypt"
)

func main() {
	flag.Parse()
	l := slog.New(slog.NewTextHandler(os.Stdout, &slog.HandlerOptions{
		Level: slog.LevelDebug,
	}))
	if err := doit(l); err != nil {
		l.Error("do", "err", err)
	}
}

func doit(l *slog.Logger) error {
	if *input == "" {
		return fmt.Errorf("no input")
	}
	if *output == "" {
		return fmt.Errorf("no output")
	}
	in := backup.Input(*input)
	out := backup.Output(*output)

	cf, err := backup.LoadConfig()
	if err != nil {
		return fmt.Errorf("load config: %w", err)
	}
	if err := cf.Validate(); err != nil {
		return fmt.Errorf("validate: %w", err)
	}

	b := backup.New(&cf.Backup, l)
	ctx := context.Background()
	switch Do(*do) {
	default:
		return fmt.Errorf("invalid do: %s", *do)
	case DoEncrypt:
		l.DebugContext(ctx, "encrypting")
		r, err := in.Reader(ctx, cf.S3)
		if err != nil {
			return fmt.Errorf("new reader: %w", err)
		}
		defer multierr.AppendFunc(&err, r.Close)
		w, err := out.Writer(ctx, l, cf.S3)
		if err != nil {
			return fmt.Errorf("new writer: %w", err)
		}
		// Do will close the writer, so we skip closing here.
		return b.Do(ctx, r, w)
	case DoDecrypt:
		l.DebugContext(ctx, "decrypting")
		r, err := in.Reader(ctx, cf.S3)
		if err != nil {
			return fmt.Errorf("new reader: %w", err)
		}
		defer multierr.AppendFunc(&err, r.Close)
		w, err := out.Writer(ctx, l, cf.S3)
		if err != nil {
			return fmt.Errorf("new writer: %w", err)
		}
		defer multierr.AppendFunc(&err, w.Close)
		return b.Undo(ctx, r, w)
	}
}
