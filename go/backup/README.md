# Backup

Encrypt and compress files and back them up to S3 storage.

## Usage

```sh
# If you need proxy
# export https_proxy=socks5://127.0.0.1:7890
go run ./cmd -input ~/20241227141825.yaml -output s3://r2/20241227141825
```
