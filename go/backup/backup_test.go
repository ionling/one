package backup

import (
	"errors"
	"fmt"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	"go.uber.org/multierr"
)

func TestMultiErr(t *testing.T) {
	test := func(f any) {
		var errTime time.Time
		getErr := func() error {
			t.Log("called getErr()")
			errTime = time.Now()
			return fmt.Errorf("err at %v", errTime)
		}

		dur := time.Second
		err := func() (err error) {
			switch v := f.(type) {
			default:
				t.Fatalf("invalid f: %T", v)
			case func(into *error, err error) bool:
				defer v(&err, getErr())
			case func(into *error, fn func() error):
				defer v(&err, getErr)
			}
			t.Log("after defer")
			time.Sleep(dur - 200*time.Millisecond)
			return nil
		}()
		now := time.Now()
		assert.WithinDuration(t, errTime, now, dur)
		t.Log(err)
		t.Log(now)
	}

	t.Run("AppendInto", func(t *testing.T) {
		test(multierr.AppendInto)
	})

	t.Run("AppendFunc", func(t *testing.T) {
		test(multierr.AppendFunc)
	})

	t.Run("Named", func(t *testing.T) {
		err := func() (err error) {
			err = errors.New("hello")
			defer multierr.AppendFunc(&err, func() error {
				return errors.New("world")
			})
			return err
		}()
		t.Log(err)
	})

	t.Run("NotNamed", func(t *testing.T) {
		err := func() error {
			err := errors.New("hello")
			defer multierr.AppendFunc(&err, func() error {
				return errors.New("world")
			})
			return err
		}()
		t.Log(err)
	})
}
