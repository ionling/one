package backup

import (
	"bytes"
	"compress/flate"
	"context"
	"io"
	"log/slog"
	"os"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

func Test_s3Writer(t *testing.T) {
	var (
		ctx = context.Background()
		l   = slog.New(slog.NewTextHandler(os.Stdout, &slog.HandlerOptions{
			Level: slog.LevelDebug,
		}))
	)
	wc, err := newS3Writer(ctx, l, &S3Conf{Name: "test"}, "test")
	require.NoError(t, err)
	_, err = wc.Write([]byte("hello"))
	require.NoError(t, err)
	assert.NoError(t, wc.Close())
}

func TestFlate(t *testing.T) {
	buf := bytes.Buffer{}
	fw := R(flate.NewWriter(&buf, flate.BestCompression)).NoError(t).V()
	bs := []byte("hello")
	R(fw.Write(bs)).NoError(t)
	require.NoError(t, fw.Close())

	fr := flate.NewReader(&buf)
	all := R(io.ReadAll(fr)).NoError(t)
	assert.Equal(t, bs, all)
}

type Result[T any] struct {
	t   *testing.T
	v   T
	err error
}

func R[T any](v T, err error) *Result[T] {
	return &Result[T]{
		v:   v,
		err: err,
	}
}

func (r *Result[T]) V() T {
	return r.v
}

func (r *Result[T]) NoError(t *testing.T) *Result[T] {
	r.t = t
	require.NoError(r.t, r.err)
	return r
}

func (r *Result[T]) Equal(v T) *Result[T] {
	require.Equal(r.t, v, r.v)
	return r
}
