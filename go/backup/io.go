package backup

import (
	"bytes"
	"context"
	"log/slog"

	"errors"
	"fmt"
	"io"
	"net/url"
	"os"
	"strings"

	"github.com/aws/aws-sdk-go-v2/service/s3"
	"github.com/aws/aws-sdk-go-v2/service/s3/types"
	"github.com/samber/lo"
)

const s3Protocol = "s3://"

func s3ConfLengthErr(n int) error {
	return fmt.Errorf("matched S3 configurations should be 1, got %d", n)
}

type Input string

func (i Input) Reader(ctx context.Context, s3 []*S3Conf) (io.ReadCloser, error) {
	select {
	case <-ctx.Done():
		return nil, ctx.Err()
	default:
		if strings.HasPrefix(string(i), s3Protocol) {
			u, err := url.Parse(string(i))
			if err != nil {
				return nil, fmt.Errorf("parse s3: %w", err)
			}
			matches := lo.Filter(s3, func(it *S3Conf, _ int) bool { return it.Name == u.Host })
			if len(matches) != 1 {
				return nil, s3ConfLengthErr(len(matches))
			}
			return newS3Reader(ctx, matches[0], strings.TrimPrefix(u.Path, "/"))
		}
		return os.Open(string(i))
	}
}

type Output string

func (o Output) Writer(ctx context.Context, l *slog.Logger, s3 []*S3Conf) (io.WriteCloser, error) {
	select {
	case <-ctx.Done():
		return nil, ctx.Err()
	default:
		if strings.HasPrefix(string(o), s3Protocol) {
			u, err := url.Parse(string(o))
			if err != nil {
				return nil, fmt.Errorf("parse s3: %w", err)
			}
			matches := lo.Filter(s3, func(it *S3Conf, _ int) bool { return it.Name == u.Host })
			if len(matches) != 1 {
				return nil, s3ConfLengthErr(len(matches))
			}
			return newS3Writer(ctx, l, matches[0], strings.TrimPrefix(u.Path, "/"))
		}
		_, err := os.Stat(string(o))
		if err == nil {
			return nil, errors.New("output exists")
		}
		if errors.Is(err, os.ErrNotExist) {
			return os.Create(string(o))
		} else {
			return nil, err
		}
	}
}

func newS3Reader(ctx context.Context, conf *S3Conf, key string) (io.ReadCloser, error) {
	client := conf.NewClient()
	res, err := client.GetObject(ctx, &s3.GetObjectInput{
		Bucket: &conf.Bucket,
		Key:    &key,
	})
	if err != nil {
		return nil, err
	}
	return res.Body, nil
}

type s3Writer struct {
	buf    bytes.Buffer
	closed chan struct{}

	l      *slog.Logger
	ctx    context.Context
	client *s3.Client
	conf   *S3Conf
	key    string
}

func newS3Writer(
	ctx context.Context,
	l *slog.Logger,
	conf *S3Conf,
	key string,
) (io.WriteCloser, error) {
	client := conf.NewClient()
	_, err := client.HeadObject(ctx, &s3.HeadObjectInput{
		Bucket: &conf.Bucket,
		Key:    &key,
	})
	if err != nil {
		var e *types.NotFound
		if !errors.As(err, &e) {
			return nil, fmt.Errorf("head object: %w", err)
		}
	} else {
		return nil, fmt.Errorf("object exists")
	}

	res := &s3Writer{
		closed: make(chan struct{}),
		l:      l.With("struct", "s3Writer"),
		ctx:    ctx,
		client: client,
		conf:   conf,
		key:    key,
	}
	return res, nil
}

func (w *s3Writer) Write(bs []byte) (n int, err error) {
	return w.buf.Write(bs)
}

func (w *s3Writer) Close() error {
	select {
	case <-w.closed:
		return fmt.Errorf("already closed ")
	default:
		close(w.closed)
	}

	w.l.DebugContext(w.ctx, "PutObject", "len", w.buf.Len())
	_, err := w.client.PutObject(w.ctx, &s3.PutObjectInput{
		Bucket:        &w.conf.Bucket,
		Key:           &w.key,
		Body:          &w.buf,
		ContentLength: int64(w.buf.Len()),
	})
	return err
}
