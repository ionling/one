module aproxy

go 1.22

require (
	github.com/sirupsen/logrus v1.9.3
	golang.org/x/net v0.30.0
)

require golang.org/x/sys v0.26.0 // indirect
