package example

import "github.com/spf13/cobra"

type Command struct {
	Name string
	Subs []*Command
	Run  func()
}

func (c *Command) Transform() *cobra.Command {
	res := &cobra.Command{
		Use: c.Name,
	}
	if c.Run != nil {
		res.Run = func(cmd *cobra.Command, args []string) {
			c.Run()
		}
	}

	for _, sub := range c.Subs {
		res.AddCommand(sub.Transform())
	}

	return res
}

func (c *Command) Exec() error {
	return c.Transform().Execute()
}
