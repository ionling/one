package impl

import (
	"sync"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestBuiltinMutex(t *testing.T) {
	var (
		s  []int
		n  = 10000
		wg sync.WaitGroup
		m  sync.Mutex
	)

	wg.Add(n)
	for i := 0; i < n; i++ {
		go func(x int) {
			defer wg.Done()

			m.Lock()
			defer m.Unlock()

			s = append(s, x)
		}(i)
	}
	wg.Wait()

	assert.Equal(t, n, len(s))
}

func TestMyMutex(t *testing.T) {
	var (
		s  []int
		n  = 10000
		wg sync.WaitGroup
		m  Mutex
	)

	wg.Add(n)
	for i := 0; i < n; i++ {
		go func(x int) {
			defer wg.Done()

			m.Lock()
			defer m.Unlock()

			s = append(s, x)
		}(i)
	}
	wg.Wait()

	assert.Equal(t, n, len(s))
}
