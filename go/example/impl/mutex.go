package impl

import (
	"fmt"
	"sync"
	"sync/atomic"
	"time"
)

type Mutex struct {
	s uint32
}

func (m *Mutex) Lock() {
	for {
		if atomic.CompareAndSwapUint32(&m.s, 0, 1) {
			break
		}
		time.Sleep(time.Nanosecond)
	}
}

func (m *Mutex) Unlock() {
	for {
		if atomic.CompareAndSwapUint32(&m.s, 1, 0) {
			break
		}
		time.Sleep(time.Nanosecond)
	}
}

func MyMutex() {
	var (
		s  []int
		n  = 100000
		wg sync.WaitGroup
		m  Mutex
	)

	fmt.Printf("Concurrently appending %d number...\n", n)
	wg.Add(n)
	for i := 0; i < n; i++ {
		go func(x int) {
			m.Lock()
			s = append(s, x)

			m.Unlock()
			wg.Done()
		}(i)
	}
	wg.Wait()
	fmt.Println("Done, slice length:", len(s))
}
