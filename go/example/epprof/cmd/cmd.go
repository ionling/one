package cmd

import (
	"github.com/spf13/cobra"

	"example/epprof"
)

var Cmd = &cobra.Command{
	Use: "pprof",
}

func init() {
	Cmd.AddCommand(&cobra.Command{
		Use: "http-simple",
		Run: func(cmd *cobra.Command, args []string) {
			epprof.HTTPSimple()
		},
	})
}
