package epprof

import (
	"fmt"
	"net/http"
	_ "net/http/pprof"
)

func HTTPSimple() {
	addr := "localhost:8400"
	fmt.Println("listening on", addr)
	fmt.Printf("open http://%s/debug/pprof to view available profiles\n", addr)
	_ = http.ListenAndServe(addr, nil)
}
