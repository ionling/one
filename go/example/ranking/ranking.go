package ranking

import (
	"context"
	"fmt"

	"github.com/brianvoe/gofakeit/v6"
	"github.com/go-redis/redis/v8"
	"github.com/pkg/errors"

	"example"
)

var Examples = []example.Example{
	{
		Func: Redis,
	},
}

func Redis() {
	do := func() error {
		rkey := "ranking"
		rdb := redis.NewClient(&redis.Options{
			Addr: "localhost:6379",
		})

		ctx := context.Background()
		for i, ic := 0, gofakeit.Number(10, 66); i < ic; i++ {
			name := gofakeit.Name()
			for j, jc := 0, gofakeit.Number(100, 799); j < jc; j++ {
				if err := rdb.ZAddArgsIncr(ctx, rkey, redis.ZAddArgs{
					Members: []redis.Z{{
						Score:  1,
						Member: name,
					}},
				}).Err(); err != nil {
					return fmt.Errorf("ZADD %d, %d: %v", i, j, err)
				}
			}
		}

		defer func() {
			if err := rdb.Del(ctx, rkey).Err(); err != nil {
				fmt.Println("delete key:", err)
			}
		}()

		val, err := rdb.ZRevRangeWithScores(ctx, rkey, 0, 9).Result()
		if err != nil {
			return errors.Wrap(err, "ZREVRANGE")
		}

		fmt.Println("Top 10:")
		for _, v := range val {
			fmt.Printf("followers: %d, name: %s\n", int(v.Score), v.Member)
		}

		return nil
	}

	if err := do(); err != nil {
		fmt.Println("err:", err)
	}
}
