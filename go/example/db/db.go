package db

import (
	"context"
	"database/sql/driver"
	"encoding/json"
	"math/rand"
	"strconv"
	"strings"
	"time"

	"github.com/brianvoe/gofakeit/v6"
	"github.com/go-redis/redis/v8"
	"github.com/pkg/errors"
	"github.com/rs/zerolog/log"
	"gorm.io/driver/mysql"
	"gorm.io/gorm"
)

const dsn = "root:9999@tcp(127.0.0.1:3306)/example?charset=utf8mb4&parseTime=True&loc=Local"

// Not fouond error.
// 这里有几种方式避免直接在业务包引用 gorm.ErrRecordNotFound, 最简单的是方案一,
// 方案二主要是会修改第三方包, 感觉不太好:
// 1. 新建一个 ErrNotFound, 将 gorm.ErrRecordNotFound 赋值给他
// 2. 将 ErrNotFound 赋值给 gorm.ErrRecordNotFound
// 3. 新建一个 IsErrNotFound() 函数
// 4. 封装一个 wrapErr 函数, 将 gorm.ErrRecordNotFound 替换为 ErrNotFound
var ErrNotFound = gorm.ErrRecordNotFound

func MustOpen() *gorm.DB {
	db, err := gorm.Open(mysql.Open(dsn))
	if err != nil {
		panic(errors.Wrap(err, "open mysql"))
	}

	return db
}

type Pet struct {
	ID      int
	Name    string
	OwnerID int
	Species string
	Sex     int
	Birth   time.Time
	Death   time.Time
}

func RandomPet() *Pet {
	return &Pet{
		Name:    gofakeit.Name(),
		OwnerID: gofakeit.Number(1, 100000),
		Species: gofakeit.Sentence(3),
		Sex:     gofakeit.RandomInt([]int{0, 1}),
		Birth:   gofakeit.Date(),
		Death:   gofakeit.Date(),
	}
}

type PetDao interface {
	Create(p *Pet) error
	Get(id int) (*Pet, error)
	RandomGet() (*Pet, error)
	Update(p *Pet) error
	UpdateAndGet(p *Pet) (*Pet, error)
	Delete(ids ...int) error
	Page(PetPageParams) (items []*Pet, count int, err error)
}

var _ PetDao = &PetDBDao{}

type PetDBDao struct {
	db *gorm.DB
}

func NewPetDBDao(db *gorm.DB) *PetDBDao {
	return &PetDBDao{
		db: db,
	}
}

func (d *PetDBDao) Create(p *Pet) error {
	return d.db.Create(p).Error
}

func (d *PetDBDao) Get(id int) (res *Pet, err error) {
	res = new(Pet)
	if err := d.db.First(res, id).Error; err != nil {
		return nil, err
	}

	return
}

func (d *PetDBDao) RandomGet() (*Pet, error) {
	res := new(Pet)
	tx := d.db.Order("RAND()").First(res)
	if err := tx.Error; err != nil {
		return nil, err
	}
	return res, nil
}

func (d *PetDBDao) Update(p *Pet) error {
	return d.db.Where(&Pet{ID: p.ID}).Updates(p).Error
}

func (d *PetDBDao) UpdateAndGet(p *Pet) (res *Pet, err error) {
	if err = d.Update(p); err != nil {
		err = errors.Wrap(err, "update")
		return
	}

	if res, err = d.Get(p.ID); err != nil {
		err = errors.Wrap(err, "get")
		return
	}

	return
}

func (d *PetDBDao) Delete(ids ...int) error {
	return d.db.Delete(Pet{}, ids).Error
}

type PetPageParams struct {
	Page     int
	PageSize int
}

func (d *PetDBDao) Page(p PetPageParams) (items []*Pet, count int, err error) {
	limit, offset := p.PageSize, (p.Page-1)*p.PageSize
	tx := d.db.Limit(limit).Offset(offset).Find(&items)
	if err = tx.Error; err != nil {
		err = errors.Wrap(err, "find")
		return
	}

	var c int64
	tx = d.db.Model(&Pet{}).Count(&c)
	if err = tx.Error; err != nil {
		err = errors.Wrap(err, "count")
		return
	}

	count = int(c)
	return
}

func (d *PetDBDao) Tx(f func(tx *PetDBDao) error) error {
	return d.db.Transaction(func(tx *gorm.DB) error {
		return f(NewPetDBDao(tx))
	})
}

func (d *PetDBDao) RandomAction() error {
	n := rand.Intn(100)
	switch {
	case n < 25: // Create
		return errors.Wrap(d.Create(RandomPet()), "create")
	case n < 50: // Read
		if _, err := d.RandomGet(); err != nil {
			if !errors.Is(err, gorm.ErrRecordNotFound) {
				return errors.Wrap(err, "random")
			}
		}
	case n < 75: // Update
		err := d.Tx(func(tx *PetDBDao) error {
			pet, err := tx.RandomGet()
			if err != nil && !errors.Is(err, gorm.ErrRecordNotFound) {
				return errors.Wrap(err, "random")
			}
			newPet := RandomPet()
			newPet.ID = pet.ID
			if err := tx.Update(newPet); err != nil {
				return errors.Wrap(err, "do")
			}
			return nil
		})
		return errors.Wrap(err, "update")
	default: // Delete
		err := d.Tx(func(tx *PetDBDao) error {
			pet, err := tx.RandomGet()
			if err != nil && !errors.Is(err, gorm.ErrRecordNotFound) {
				return errors.Wrap(err, "random")
			}
			if err := tx.Delete(pet.ID); err != nil {
				return errors.Wrap(err, "do")
			}
			return nil
		})
		return errors.Wrap(err, "delete")
	}

	return nil
}

type PetCacheDao struct {
	PetDBDao

	rdb *redis.Client
}

func NewPetCacheDao(db *gorm.DB, rdb *redis.Client) *PetCacheDao {
	return &PetCacheDao{
		PetDBDao: *NewPetDBDao(db),
		rdb:      rdb,
	}
}

func (d *PetCacheDao) Get(id int) (res *Pet, err error) {
	res = new(Pet)
	ctx := context.Background()
	key := d.cacheKey(id)
	if val, err := d.rdb.Get(ctx, key).Result(); err == nil {
		if val == "n" {
			return nil, gorm.ErrRecordNotFound
		}
		if err := json.Unmarshal([]byte(val), res); err != nil {
			return nil, errors.Wrap(err, "unmarshal")
		}
	}

	if res, err = d.PetDBDao.Get(id); err != nil {
		if errors.Is(err, gorm.ErrRecordNotFound) {
			if err := d.rdb.Set(ctx, key, "n", time.Minute).Err(); err != nil {
				return nil, errors.Wrap(err, "set nil")
			}
		}
		return nil, errors.Wrap(err, "db")
	}

	j, err := json.Marshal(res)
	if err != nil {
		return nil, errors.Wrap(err, "marshal")
	}

	if err = d.rdb.Set(ctx, key, j, time.Minute).Err(); err != nil {
		return nil, errors.Wrap(err, "set")
	}

	return
}

func (d *PetCacheDao) Update(p *Pet) error {
	return d.withDoubleDel(func() error {
		return d.PetDBDao.Update(p)
	}, "PetCacheDao.Update", p.ID)
}

func (d *PetCacheDao) UpdateAndGet(p *Pet) (res *Pet, err error) {
	if err = d.Update(p); err != nil {
		err = errors.Wrap(err, "update")
		return
	}

	if res, err = d.Get(p.ID); err != nil {
		err = errors.Wrap(err, "get")
		return
	}

	return
}

func (d *PetCacheDao) Delete(ids ...int) error {
	return d.withDoubleDel(func() error {
		return d.PetDBDao.Delete(ids...)
	}, "PetCacheDao.Delete", ids...)
}

func (*PetCacheDao) cacheKey(id int) string {
	return "pet:" + strconv.Itoa(id)
}

func (d *PetCacheDao) delCache(ids ...int) error {
	var keys []string
	for _, id := range ids {
		keys = append(keys, d.cacheKey(id))
	}

	return d.rdb.Del(context.Background(), keys...).Err()
}

func (d *PetCacheDao) withDoubleDel(action func() error, actionName string, ids ...int) error {
	if err := d.delCache(ids...); err != nil {
		return errors.Wrap(err, "first del")
	}

	if err := action(); err != nil {
		return errors.Wrap(err, "action")
	}

	go func() {
		time.Sleep(400 * time.Millisecond)
		if err := d.delCache(ids...); err != nil {
			log.Err(err).Str("action", actionName).Msg("second del")
		}
	}()

	return nil
}

// Out of memory.
// Just open many connections.
// `docker update --memory=500M --memory-swap=500M mysql`
func OOM() {
	do := func() error {
		db, err := gorm.Open(mysql.Open(dsn))
		if err != nil {
			return errors.Wrap(err, "open mysql")
		}

		if err := db.AutoMigrate(&Pet{}); err != nil {
			return errors.Wrap(err, "migrate")
		}

		if err := db.Exec("set global max_connections=900").Error; err != nil {
			return errors.Wrap(err, "set max_connections")
		}

		dao := NewPetDBDao(db)
		goCh := make(chan struct{}, 100)
		go func() {
			for {
				goCh <- struct{}{}
				time.Sleep(100 * time.Microsecond)
			}
		}()

		dbDown := make(chan struct{})
		go func() {
			for range goCh {
				go func() {
					if err := dao.RandomAction(); err != nil {
						if errors.Is(err, driver.ErrBadConn) {
							dbDown <- struct{}{}
						}
						if strings.Contains(err.Error(), "connection refused") {
							dbDown <- struct{}{}
						}
						log.Err(err).Send()
					}
				}()
			}
		}()

		select {
		case <-dbDown:
			log.Info().Msg("database is down")
		case <-time.After(9 * time.Second):
			log.Info().Msg("done")
		}

		return nil
	}

	if err := do(); err != nil {
		log.Err(err).Send()
	}
}
