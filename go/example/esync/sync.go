package esync

import "sync"

func UnlockNotLocked() {
	var m sync.Mutex
	m.Unlock()
}
