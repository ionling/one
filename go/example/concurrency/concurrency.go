package concurrency

import (
	"context"
	"fmt"
	"math/rand"
	"sync"
	"time"

	"golang.org/x/sync/errgroup"
)

func PlusWithoutLock() {
	var (
		count       int
		concurrency = 9999
		wg          sync.WaitGroup
	)

	for i := 0; i < concurrency; i++ {
		wg.Add(1)
		go func() {
			defer wg.Done()
			count++
		}()
	}

	wg.Wait()
	fmt.Printf("count: %d, expected: %d\n", count, concurrency)
}

func PlusWithLock() {
	var (
		count       int
		concurrency = 9999
		mu          sync.Mutex
		wg          sync.WaitGroup
	)

	for i := 0; i < concurrency; i++ {
		wg.Add(1)
		go func() {
			defer wg.Done()
			mu.Lock()
			defer mu.Unlock()

			count++
		}()
	}

	wg.Wait()
	fmt.Printf("count: %d, expected: %d\n", count, concurrency)
}

func CancelOtherGoroutines() {
	var wg sync.WaitGroup
	resCh, errCh := make(chan int), make(chan error)
	ctx, cancel := context.WithCancel(context.Background())
	sumRange := func(begin, end int) {
		defer wg.Done()

		var res int
		for i := begin; i <= end; i++ {
			select {
			case <-ctx.Done():
				errCh <- ctx.Err()
				return
			default:
				res += i
			}
		}
		resCh <- res
	}

	errFunc := func() {
		cancel()
	}

	wg.Add(4)
	go sumRange(1, 100)
	go sumRange(101, 200)
	go sumRange(201, 300)
	go sumRange(301, 400)
	go errFunc()
	go func() {
		wg.Wait()
		close(resCh)
		close(errCh)
	}()

	var sum int
L:
	for {
		select {
		case res, ok := <-resCh:
			if !ok {
				break L
			}
			sum += res
		case err, ok := <-errCh:
			if !ok {
				break L
			}
			fmt.Println("Got err:", err)
			return
		}
	}

	fmt.Println("Result:", sum)
}

func work(i int) (int, error) {
	if n := rand.Intn(100); n < 10 { // 10% of failure
		return 0, fmt.Errorf("random error %d", n)
	}
	time.Sleep(time.Second)
	return 100 + i, nil
}

func CancelOtherGoroutines2() {
	var (
		wg          sync.WaitGroup
		concurrency = 3
		errs        = make(chan error, concurrency)
		ctx, cancel = context.WithCancel(context.Background())
	)

	defer cancel() // Make sure it's called to release resources even if no errors
	for i := 0; i < concurrency; i++ {
		wg.Add(1)
		go func(i int) {
			defer wg.Done()

			for j := 0; j < 10; j++ {
				// Check if any error occurred in any other gorouties:
				select {
				case <-ctx.Done():
					return
				default:
				}
				indicator := fmt.Sprintf("worker #%d during %d", i, j)
				result, err := work(j)
				if err != nil {
					errs <- fmt.Errorf("%s, error: %v\n", indicator, err)
					cancel()
					return
				}
				fmt.Printf("%s, result: %d.\n", indicator, result)
			}
		}(i)
	}
	wg.Wait()

	if ctx.Err() != nil {
		fmt.Println("err:", <-errs)
	}
}

func CancelOtherGoroutinesWithErrGroup() {
	concurrency := 3
	g, ctx := errgroup.WithContext(context.Background())
	for i := 0; i < concurrency; i++ {
		i := i
		g.Go(func() error {
			for j := 0; j < 10; j++ {
				select {
				case <-ctx.Done():
					return nil
				default:
				}
				indicator := fmt.Sprintf("worker #%d during %d", i, j)
				result, err := work(j)
				if err != nil {
					return fmt.Errorf("%s, error: %v\n", indicator, err)
				}
				fmt.Printf("%s, result: %d.\n", indicator, result)
			}
			return nil
		})
	}

	if err := g.Wait(); err != nil {
		fmt.Println("err:", err)
	}
}

func WriteMap() {
	var (
		m  = make(map[int]int)
		wg sync.WaitGroup
	)

	for i := 0; i < 100; i++ {
		wg.Add(1)
		ii := i
		go func() {
			m[ii%10]++
			wg.Done()
		}()
	}

	wg.Wait()
}
