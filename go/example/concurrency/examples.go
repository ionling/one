package concurrency

import (
	"example"
)

var Examples = []example.Example{
	{
		Func: PlusWithoutLock,
	},
	{
		Func: PlusWithLock,
	},
	{
		Func: CancelOtherGoroutines,
		Desc: `
多个协程有一个出错后, 所有协程全部退出.

感觉可读性比较差啊, context, wait group, channel, select 全用上了.
不过也没啥毛病, context 用于取消 goroutine, wait group, channel, select
用于保证所有线程执行完了, 并获取到最后结果.
`,
	}, {
		Func: CancelOtherGoroutines2,
		Desc: `
REF: https://stackoverflow.com/a/45502591/7134763
`,
	}, {
		Func: CancelOtherGoroutinesWithErrGroup,
		Desc: `
使用 errgroup.
`,
	},
	{
		Func: WriteMap,
	},
}
