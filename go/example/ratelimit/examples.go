package ratelimit

import (
	"context"
	"fmt"
	"runtime"
	"sync"
	"time"

	"github.com/rs/zerolog/log"
	"golang.org/x/time/rate"

	"example"
)

var Examples = []example.Example{
	{
		Func: TimeRate,
	},
}

func TimeRate() {
	var (
		mutex   sync.Mutex
		count   int
		limit   rate.Limit = 100
		burst              = 2
		seconds            = 2
	)

	lim := rate.NewLimiter(limit, burst)
	go func() {
		for i := 0; i < int(limit)*10; i++ {
			go func() {
				if err := lim.Wait(context.Background()); err != nil {
					log.Err(err).Msg("limitor wait")
					return
				}
				mutex.Lock()
				defer mutex.Unlock()
				count++
			}()
		}
	}()

	time.Sleep(time.Duration(seconds) * time.Second)
	fmt.Printf("goroutine num %d, count: %d, expected: %d\n",
		runtime.NumGoroutine(), count, int(limit)*seconds+burst)
}
