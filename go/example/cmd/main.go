package main

import (
	"errors"
	"fmt"
	"os"
	"regexp"
	"strconv"

	"github.com/spf13/cobra"

	"example"
	"example/basic"
	"example/basic/eslice"
	"example/channel"
	"example/concurrency"
	"example/db"
	"example/di"
	"example/dislock"
	"example/elog"
	epprof "example/epprof/cmd"
	"example/eredis"
	"example/esync"
	"example/etime"
	"example/impl"
	"example/intl"
	"example/ranking"
	"example/ratelimit"
	"example/retry"
	"example/server"
)

var examples = example.MergeExamples(
	eslice.Examples,
	esync.Examples,
	basic.Examples,
	channel.Examples,
	concurrency.Examples,
	db.Examples,
	di.Examples,
	dislock.Examples,
	elog.Examples,
	eredis.Examples,
	etime.Examples,
	impl.Examples,
	intl.Examples,
	ranking.Examples,
	ratelimit.Examples,
	retry.Examples,
	server.Examples,
)

var rootCmd = &cobra.Command{
	Use:   "goexample",
	Short: "Golang code examples",
}

var runCmd = &cobra.Command{
	Use:   "run",
	Short: "Name base execution",
}

var runExecRegex *bool
var runExecCmd = &cobra.Command{
	Use:   "exec [example-path]",
	Short: "Execute a example",
	Args:  cobra.ExactArgs(1),
	RunE: func(cmd *cobra.Command, args []string) (err error) {
		exsMap := make(map[string]example.Example)
		for _, ex := range examples {
			exsMap[ex.FullName()] = ex
		}

		notFound := errors.New("example not found")
		if !*runExecRegex {
			if ex, ok := exsMap[args[0]]; ok {
				ex.Run()
				return
			}

			return notFound
		}

		matches, err := match(examples, args[0])
		if err != nil {
			return err
		}

		if len(matches) == 0 {
			return notFound
		}

		run := func(i int) {
			match := matches[i]
			fmt.Println("run", match.FullName())
			match.Run()
		}

		if len(matches) == 1 {
			run(0)
			return nil
		}

		for i, v := range matches {
			fmt.Println(i, v.FullName())
		}
		i := inputNum("select one: ", 0, len(matches))
		run(i)
		return nil
	},
}

var runListVerbose *bool
var runListCmd = &cobra.Command{
	Use:   "list [query]",
	Short: "List examples",
	Long:  "List examples match the query, if no query, list all.",
	Args:  cobra.MaximumNArgs(1),
	Run: func(cmd *cobra.Command, args []string) {
		exs := examples
		if len(args) > 0 {
			exs = example.SearchExamples(exs, args[0])
		}
		for _, ex := range exs {
			if *runListVerbose {
				fmt.Println(ex)
			} else {
				fmt.Println(ex.FullName())
			}
		}
	},
}

func init() {
	runExecRegex = runExecCmd.Flags().BoolP("regex", "r", false, "regex matching")
	runListVerbose = runListCmd.Flags().BoolP("verbose", "v", false, "show detail")

	runCmd.AddCommand(
		runExecCmd,
		runListCmd,
	)

	rootCmd.AddCommand(
		elog.Cmd.Transform(),
		epprof.Cmd,
		runCmd,
	)
}

func main() {
	if err := rootCmd.Execute(); err != nil {
		fmt.Fprintln(os.Stderr, err)
		os.Exit(1)
	}
}

func match(
	exs []example.Example,
	expr string,
) (matches []example.Example, err error) {
	re, err := regexp.Compile(expr)
	if err != nil {
		return nil, err
	}

	for _, ex := range exs {
		if re.MatchString(ex.FullName()) {
			matches = append(matches, ex)
		}
	}

	return
}

// [min, max)
func inputNum(prompt string, min, max int) int {
	var input string
	fmt.Print(prompt)
	for {
		fmt.Scanln(&input)
		i, err := strconv.Atoi(input)
		if err == nil && i >= min && i < max {
			return i
		}

		fmt.Println("wrong input, try again: ")
	}
}
