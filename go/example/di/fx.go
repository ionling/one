package di

import "go.uber.org/fx"

type Cache struct{}

func NewCache() *Cache {
	return &Cache{}
}

func JustRun() {
	app := fx.New(fx.Provide(NewCache))
	app.Run()
}
