// intl: internal
package intl

import (
	"errors"
	"fmt"
	"io"
	"strings"
	"unsafe"

	"example"
)

var Examples = []example.Example{
	{Func: TypeSize},
}

type CustomErr struct {
	Name  string
	Cause error
}

func (ce CustomErr) Error() string {
	return fmt.Sprintf("%s: %v", ce.Name, ce.Cause)
}

func TypeSize() {
	var (
		b  = true
		r  rune
		c  complex128 = 111.111
		si [10]int16
		ss []string

		m    = make(map[int]string)
		mn   map[string]chan int
		ii   interface{} = 789
		is   interface{} = "789"
		ir   io.Reader   = strings.NewReader("yet")
		sr               = strings.NewReader("yet2")
		ci               = make(chan int)
		cf               = make(chan interface{})
		err              = errors.New("ion")
		errc error       = CustomErr{}
		f    func()
		fe   func() error
	)

	fmt.Printf("%T, %d\n", b, unsafe.Sizeof(b))
	fmt.Printf("%T, %d, %s\n", r, unsafe.Sizeof(r), "rune")
	fmt.Printf("%T, %d\n", c, unsafe.Sizeof(c))
	fmt.Printf("%T, %d\n", si, unsafe.Sizeof(si))
	fmt.Printf("%T, %d\n", ss, unsafe.Sizeof(ss))
	fmt.Printf("%T, %d\n", m, unsafe.Sizeof(m))
	fmt.Printf("%T, %d\n", mn, unsafe.Sizeof(mn))
	fmt.Printf("%T, %d, %s\n", ii, unsafe.Sizeof(ii), "interface{}")
	fmt.Printf("%T, %d, %s\n", is, unsafe.Sizeof(is), "interface{}")
	fmt.Printf("%T, %d, %s\n", ir, unsafe.Sizeof(ir), "io.Reader")
	fmt.Printf("%T, %d\n", sr, unsafe.Sizeof(sr))
	fmt.Printf("%T, %d\n", ci, unsafe.Sizeof(ci))
	fmt.Printf("%T, %d\n", cf, unsafe.Sizeof(cf))
	fmt.Printf("%T, %d\n", err, unsafe.Sizeof(err))
	fmt.Printf("%T, %d\n", errc, unsafe.Sizeof(errc))
	fmt.Printf("%T, %d\n", f, unsafe.Sizeof(f))
	fmt.Printf("%T, %d\n", fe, unsafe.Sizeof(fe))
}
