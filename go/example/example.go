package example

import (
	"fmt"
	"reflect"
	"runtime"
	"strings"
)

type Example struct {
	Name string
	Path string
	Desc string
	Func func()

	FuncErr func() error
}

func (e Example) FullName() string {
	if e.Name == "" && e.Path == "" {
		var funcName string
		if e.Func != nil {
			funcName = FuncName(e.Func)
		} else {
			funcName = FuncName(e.FuncErr)
		}
		return funcName[7:] // Remove `example` prefix
	}
	return e.Path + "/" + e.Name
}

func (e Example) String() (res string) {
	res = "# " + e.FullName() + "\n"
	if desc := strings.TrimSpace(e.Desc); desc != "" {
		res += "\n" + desc + "\n"
	}
	return
}

func (e Example) Run() {
	if f := e.Func; f != nil {
		f()
	} else if f := e.FuncErr; f != nil {
		if err := f(); err != nil {
			fmt.Println("ERR:", err)
		}
	} else {
		fmt.Println("Nothing to run")
	}
}

func MergeExamples(exss ...[]Example) (res []Example) {
	for _, s := range exss {
		res = append(res, s...)
	}
	return
}

func SearchExamples(exs []Example, q string) (res []Example) {
	var names []string
	for _, v := range exs {
		names = append(names, strings.ToLower(v.FullName()))
	}

	qlower := strings.ToLower(q)
	for i, n := range names {
		if strings.Contains(n, qlower) {
			res = append(res, exs[i])
		}
	}

	return
}

// e.g., `example/concurrency.CancelOtherGoroutines`.
// See more in example_test.go.
func FuncName(i interface{}) string {
	return runtime.FuncForPC(reflect.ValueOf(i).Pointer()).Name()
}
