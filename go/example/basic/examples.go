package basic

import (
	"fmt"
	"math/rand"
	"time"

	"example"
)

var Examples = []example.Example{
	{
		Func: EmptyStruct,
		Desc: `Show empty struct{} memory usage.`,
	}, {
		Func: DefaultRand,
		Desc: `
Every time this function is executed, the output is different from the last,
which is not expected, so guess why?

Because 'github.com/sean-/seed'	is used by 'github.com/spf13/cobra'
(by checking go.sum), and 'seed.MustInit()' may be called somewhere.
`,
	}, {
		Func: RandWithFixedSeed,
	}, {
		Func: RandWithTimeNow,
	},
}

func DefaultRand() {
	n := 1000
	fmt.Println(rand.Intn(n))
	fmt.Println(rand.Intn(n))
	fmt.Println(rand.Intn(n))
}

func RandWithFixedSeed() {
	r := rand.New(rand.NewSource(9))
	n := 1000
	fmt.Println(r.Intn(n))
	fmt.Println(r.Intn(n))
	fmt.Println(r.Intn(n))
}

func RandWithTimeNow() {
	seed := time.Now().UnixNano()
	r := rand.New(rand.NewSource(seed))
	n := 1000
	fmt.Println(r.Intn(n))
	fmt.Println(r.Intn(n))
	fmt.Println(r.Intn(n))
}
