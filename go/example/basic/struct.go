package basic

import (
	"fmt"
	"reflect"
)

func EmptyStruct() {
	a1 := [2]any{}
	a2 := [2]struct{}{}
	a3 := [2]struct{ any }{}
	a4 := [2]struct{ int8 }{}
	for _, a := range []any{a1, a2, a3, a4} {
		t := reflect.TypeOf(a)
		fmt.Println("type:", t, "size:", t.Size())
	}
}
