package eslice

import (
	"fmt"
	"sync"

	"example"
)

var Examples = []example.Example{
	{
		Name: "concurrently-write",
		Path: "/basic/slice",
		Func: ConcurrentlyWrite,
	},
}

// REF: https://juejin.cn/post/6844904134592692231
func ConcurrentlyWrite() {
	var (
		s  []int
		n  = 10000
		wg sync.WaitGroup
	)

	wg.Add(n)
	for i := 0; i < n; i++ {
		go func(x int) {
			s = append(s, x)
			wg.Done()
		}(i)
	}
	wg.Wait()

	fmt.Printf("Concurrently appending %d number...\n", n)
	fmt.Println("Done, slice length:", len(s))
	fmt.Println("First 100 numbers:", s[:100])
}
