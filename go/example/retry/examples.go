package retry

import (
	"example"
	"fmt"
	"time"

	"github.com/avast/retry-go/v3"
)

var Examples = []example.Example{
	{Func: AvastRetry},
}

func AvastRetry() {
	n := 0
	lastTime := time.Now()
	if err := retry.Do(
		func() error {
			n++
			ms := time.Since(lastTime).Milliseconds()
			fmt.Printf("req %d, since last: %d ms\n", n, ms)
			lastTime = time.Now()

			if n < 6 {
				return fmt.Errorf("error %d", n)
			}
			return nil
		},
		// 结合打印结果以及源码分析, OnRetry 是第一次执行失败后就会执行,
		// 然后等待一定时间, 开始下一次执行.
		// 而不是我以为的, 第一次执行失败后不执行, 下一次执行之前才执行.
		// 不过他这个逻辑也没有问题, 如果把等待时间算到重试里面的话.
		retry.OnRetry(func(n uint, err error) {
			fmt.Printf("retry %d, err: %v\n", n+1, err)
		}),
	); err != nil {
		fmt.Println("err:", err)
	}
}
