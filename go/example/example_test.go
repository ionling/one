package example_test

import (
	"testing"

	"example"
	"example/channel"
	"example/esync"
)

func TestFuncName(t *testing.T) {
	hello := func() string {
		return "world"
	}

	world := "world"
	hello2 := func() string {
		return world
	}

	type Args struct {
		i interface{}
	}

	var ch channel.Channel
	tests := []struct {
		name string
		args Args
		want string
	}{
		{
			"self",
			Args{example.FuncName},
			"example.FuncName",
		}, {
			"func",
			Args{esync.UnlockNotLocked},
			"example/esync.UnlockNotLocked",
		}, {
			"method",
			Args{ch.Read},
			"example/channel.(*Channel).Read-fm",
		}, {
			"value",
			Args{hello},
			"example_test.TestFuncName.func1",
		}, {
			"closure",
			Args{hello2},
			"example_test.TestFuncName.func2",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := example.FuncName(tt.args.i); got != tt.want {
				t.Errorf("FuncName() = %v, want %v", got, tt.want)
			}
		})
	}
}
