package elog

import (
	"example"
)

var Cmd = example.Command{
	Name: "log",
	Subs: []*example.Command{{
		Name: "std",
		Subs: []*example.Command{{
			Name: "fatal",
			Run:  StdFatal,
		}, {
			Name: "panic",
			Run:  StdPanic,
		}, {
			Name: "directly-panic",
			Run:  Panic,
		}},
	}, {
		Name: "zero",
		Subs: []*example.Command{{
			Name: "simple",
			Run:  ZeroSimple,
		}},
	}},
}
