package elog

import (
	"errors"
	"log"
)

var bootRedisError = errors.New("network unreachable")

func StdPrint() {
	log.Print("Print(): ", 1, 2, 3, 4)
	log.Println("Println():", 1, 2, 3, 4)
	log.Printf("Printf(): %d %d %d %d\n", 1, 2, 3, 4)
	log.Print("Print(): ", "one", "two", "three", "four")
	log.Println("Println(): ", "one", "two", "three", "four")
	log.Print("Print(): ", "one", 2, "three", "four", true)
	log.Print("Print(): ", 1, 2, "three", 4, 5)
}

func StdFatal() {
	log.Fatal("boot redis", bootRedisError)
}

func StdPanic() {
	log.Panic("boot redis", bootRedisError)
}

func Panic() {
	panic(bootRedisError)
}
