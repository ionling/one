package tests

import (
	"crypto/aes"
	"crypto/cipher"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

func TestAES(t *testing.T) {
	t.Run("XOR", func(t *testing.T) {
		iv := "abyRItXcyRCzYlVp"
		block, err := aes.NewCipher([]byte("lTQvKSytRlnnPppqatgBwFNIWvKYzcTG"))
		require.NoError(t, err)

		input := "hello"
		enc := make([]byte, len(input))
		s := cipher.NewOFB(block, []byte(iv))
		s.XORKeyStream(enc, []byte(input))

		dec := make([]byte, len(input))
		s = cipher.NewOFB(block, []byte(iv))
		s.XORKeyStream(dec, enc)
		assert.Equal(t, input, string(dec))
	})
}
