package etime

import (
	"fmt"
	"time"

	"example"
)

var Examples = []example.Example{
	{Func: Duration},
	{Func: ParseDuration},
}

func Duration() {
	fmt.Println("20s", 20*time.Second)
	fmt.Println("10m", 10*time.Minute)
	fmt.Println("30h", 30*time.Hour)
	fmt.Println("40d", 40*24*time.Hour)
}

func ParseDuration() {
	p := func(s string) {
		d, err := time.ParseDuration(s)
		fmt.Println(s, d, err)
	}

	p("20s")
	p("30m")
	p("40h")
	p("50d")
}
