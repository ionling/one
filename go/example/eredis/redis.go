package eredis

import (
	"context"
	"fmt"
	"time"

	"github.com/brianvoe/gofakeit/v6"
	"github.com/go-redis/redis/v8"
	"github.com/pkg/errors"

	"example"
)

const redisAddr = "localhost:6379"

var Examples = []example.Example{
	{
		Func: Demo,
	},
}

func Demo() {
	ctx := context.Background()
	rdb := MustOpen()

	type Case struct {
		Title string
		Run   func()
	}

	cases := []Case{
		{
			Title: "Get not existed key",
			Run: func() {
				val, err := rdb.Get(ctx, gofakeit.AppName()).Result()
				fmt.Printf("val: %v, err: %v\n", val, err)
			},
		},
		{
			Title: "Set bytes",
			Run: func() {
				key, setVal := gofakeit.Animal(), []byte("hello")
				val, err := rdb.Set(ctx, key, setVal, time.Minute).Result()
				fmt.Printf("set val: %v, \n", setVal)
				fmt.Printf("set res: val: %v, err: %v\n", val, err)
				val, err = rdb.Get(ctx, key).Result()
				fmt.Printf("get res: val: %v, err: %v\n", val, err)
			},
		},
	}

	for _, c := range cases {
		fmt.Println("#", c.Title)
		c.Run()
		fmt.Println()
	}
}

func MustOpen() *redis.Client {
	rdb := redis.NewClient(&redis.Options{
		Addr: redisAddr,
	})

	if err := rdb.Ping(context.Background()).Err(); err != nil {
		panic(errors.Wrap(err, "open redis"))
	}

	return rdb
}
