package eredis

import (
	"context"
	"strconv"
	"testing"
	"time"

	"github.com/go-redis/redis/v8"
	"github.com/pkg/errors"
)

func BenchmarkNormalGet(b *testing.B) {
	rdb := MustOpen()
	ctx := context.Background()
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		key := genKey("bench/get/normal", i)
		if err := rdb.Get(ctx, key).Err(); err != nil {
			if !errors.Is(err, redis.Nil) {
				b.Fatal("get:", err)
			}
		}
	}
}

func BenchmarkNormalSet(b *testing.B) {
	rdb := MustOpen()
	ctx := context.Background()
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		key := genKey("bench/set/normal", i)
		if err := rdb.Set(ctx, key, i, time.Minute).Err(); err != nil {
			b.Fatal("set:", err)
		}
	}
}

func BenchmarkPipelineSet(b *testing.B) {
	pipe := MustOpen().Pipeline()
	ctx := context.Background()
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		key := genKey("bench/set/pipeline", i)
		if err := pipe.Set(ctx, key, i, time.Minute).Err(); err != nil {
			b.Fatal("set:", err)
		}
	}

	if _, err := pipe.Exec(ctx); err != nil {
		b.Fatal("exec:", err)
	}
}

func BenchmarkTxPipelineSet(b *testing.B) {
	pipe := MustOpen().TxPipeline()
	ctx := context.Background()
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		key := genKey("bench/set/tx", i)
		if err := pipe.Set(ctx, key, i, time.Minute).Err(); err != nil {
			b.Fatal("set:", err)
		}
	}

	if _, err := pipe.Exec(ctx); err != nil {
		b.Fatal("exec:", err)
	}
}

func genKey(prefix string, n int) string {
	return prefix + ":" + strconv.Itoa(n)
}
