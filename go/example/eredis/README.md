
## Benchmarks

```
docker update --memory=800M --memory-swap=800M redis
flushall
```

### go-redis

- 本地 redis, 单进程客户端能跑到约 90 µs/op
- 使用 pipeline, 则能跑到约 4 µs/op
- 结论: 使用 pipeline 能提升 20 多倍性能

```
goos: linux
goarch: amd64
pkg: example/eredis
cpu: Intel(R) Core(TM) i5-4210M CPU @ 2.60GHz
BenchmarkNormalGet-4               14217             86527 ns/op
BenchmarkNormalSet-4               12976             88302 ns/op
BenchmarkPipelineSet-4            339499              3703 ns/op
BenchmarkTxPipelineSet-4          348721              3989 ns/op
```

### redis-benchmark

结论:

- pipeline 能有效提高 rps, 最大可提升至 80 多倍(1 vs 1000)
- 目前系统只能跑到 100M rps 左右, 如果增加客户端, 只会使得响应时间增加, rps 不会改变


| pipeline | op  | rps  | rps ratio |
|----------|-----|------|-----------|
| 1        | 63  | 12K  | 1         |
| 10       | 71  | 110K | 9         |
| 100      | 159 | 600K | 5         |
| 1000     | 420 | 1M   | 1.7       |
| 10000    | 500 | 1.1M | 1.1       |

```
❯ redis-benchmark -c 1 -q -t set,get,incr
SET: 12487.51 requests per second, p50=0.063 msec
GET: 13157.89 requests per second, p50=0.063 msec
INCR: 12888.26 requests per second, p50=0.063 msec

❯ redis-benchmark -c 1 -q -t set,get,incr -n 1000000 -P 10
SET: 116550.12 requests per second, p50=0.071 msec
GET: 112905.04 requests per second, p50=0.071 msec
INCR: 114246.54 requests per second, p50=0.079 msec

❯ redis-benchmark -c 1 -q -t set,get,incr -n 5000000 -P 100
SET: 528876.62 requests per second, p50=0.167 msec
GET: 618276.25 requests per second, p50=0.143 msec
INCR: 595805.50 requests per second, p50=0.151 msec

❯ redis-benchmark -c 1 -q -t set,get,incr -n 8000000 -P 1000
SET: 877770.44 requests per second, p50=0.431 msec
GET: 1078021.88 requests per second, p50=0.423 msec
INCR: 968640.31 requests per second, p50=0.415 msec

❯ redis-benchmark -c 1 -q -t set,get,incr -n 8000000 -P 10000
SET: 931966.50 requests per second, p50=0.527 msec
GET: 1157407.38 requests per second, p50=0.511 msec
INCR: 1090066.75 requests per second, p50=0.495 msec

❯ redis-benchmark -c 1 -q -t set,get,incr -n 8000000 -P 10000 -r 10000
SET: 823045.25 requests per second, p50=0.615 msec
GET: 1016518.44 requests per second, p50=0.599 msec
INCR: 872124.69 requests per second, p50=0.615 msec

❯ redis-benchmark -c 10 -q -t set,get,incr -n 8000000 -P 10000 -r 10000
SET: 918590.00 requests per second, p50=5.287 msec
GET: 1130262.75 requests per second, p50=6.223 msec
INCR: 979072.38 requests per second, p50=5.183 msec

❯ redis-benchmark -c 10 -q -t set,get,incr -n 8000000 -P 100 -r 10000
SET: 776548.25 requests per second, p50=1.159 msec
GET: 937207.12 requests per second, p50=0.959 msec
INCR: 816409.88 requests per second, p50=1.095 msec

❯ redis-benchmark -c 10 -q -t set,get,incr -n 5000000 -P 10 -r 10000
SET: 341343.53 requests per second, p50=0.247 msec
GET: 394944.72 requests per second, p50=0.215 msec
INCR: 363901.03 requests per second, p50=0.231 msec
```
