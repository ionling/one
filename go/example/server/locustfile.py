import random

from faker import Faker
from locust import FastHttpUser, HttpUser, task

count = 2
min_id = 1
max_id = 3


def random_id() -> int:
    return random.randint(min_id, max_id)


class ReadNotExistPetUser(FastHttpUser):
    host = "http://localhost:1323/api/pets"

    @task
    def get(self):
        self.client.get("/999999")


class PetsUser(HttpUser):
    weight = 0
    host = "http://localhost:1323/api/pets"
    faker = Faker()
    resource_name = "/api/pets"
    detail_name = "/api/pets/:id"

    def random_pet(self) -> dict:
        f = self.faker
        return {
            "name": f.name(),
            "ownerID": f.random_int(),
            "species": f.job(),
            "sex": f.random_element([0, 1]),
            "birth": f.date_time().isoformat() + "Z",
            "death": f.date_time().isoformat() + "Z",
        }

    @task
    def create(self):
        self.client.post("", json=self.random_pet())

    @task
    def get(self):
        self.client.get("/random")
        self.client.get(f"/{random_id()}", name=self.detail_name)

    @task
    def update(self):
        self.client.put(
            f"/{random_id()}", json=self.random_pet(), name=self.detail_name
        )

    @task
    def delete(self):
        self.client.delete(f"/{random_id()}", name=self.detail_name)

    @task
    def page(self):
        page_size = random.randint(1, int(count / 2))
        page = random.randint(0, int(count / page_size))
        self.client.get(f"?page={page}&page_size={page_size}", name=self.resource_name)

    @task
    def watch_max_id(self):
        resp = self.client.get("/?page=1&page_size=1", name=self.resource_name)
        data = resp.json()
        global count, min_id, max_id
        count = data["count"]
        min_id = data["items"][0]["id"]
        # Make sure max_id always greater than min_id
        max_id = min_id + 1000

        resp2 = self.client.get(f"/?page={count}&page_size=1", name=self.resource_name)
        data = resp2.json()
        # Pets maybe deleted in other tasks
        if data["items"] is None:
            return

        max_id = data["items"][0]["id"]
