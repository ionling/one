package server

import (
	"net/http"
	"strconv"
	"time"

	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
	"github.com/pkg/errors"

	"example/db"
	"example/eredis"
)

type Pet struct {
	ID      int       `json:"id"`
	Name    string    `json:"name"`
	OwnerID int       `json:"ownerID"`
	Species string    `json:"species"`
	Sex     int       `json:"sex"`
	Birth   time.Time `json:"birth"`
	Death   time.Time `json:"death"`
}

func ToDBPet(p *Pet) *db.Pet {
	if p == nil {
		return nil
	}

	return &db.Pet{
		ID:      p.ID,
		Name:    p.Name,
		OwnerID: p.OwnerID,
		Species: p.Species,
		Sex:     p.Sex,
		Birth:   p.Birth,
		Death:   p.Death,
	}
}

func FromDBPet(p *db.Pet) *Pet {
	if p == nil {
		return nil
	}

	return &Pet{
		ID:      p.ID,
		Name:    p.Name,
		OwnerID: p.OwnerID,
		Species: p.Species,
		Sex:     p.Sex,
		Birth:   p.Birth,
		Death:   p.Death,
	}
}

type PetAPI struct {
	dao db.PetDao
}

func NewPetAPI(dao db.PetDao) PetAPI {
	return PetAPI{
		dao: dao,
	}
}

func getIDParam(c echo.Context) (id int) {
	id, _ = strconv.Atoi(c.Param("id"))
	return
}

func (a PetAPI) Create(c echo.Context) error {
	p := new(Pet)
	if err := c.Bind(p); err != nil {
		return errors.Wrap(err, "bind")
	}

	dbp := ToDBPet(p)
	if err := a.dao.Create(dbp); err != nil {
		return errors.Wrap(err, "create")
	}

	return c.JSON(http.StatusCreated, FromDBPet(dbp))
}

func (a PetAPI) Get(c echo.Context) error {
	id := getIDParam(c)
	dbp, err := a.dao.Get(id)
	if err != nil {
		if errors.Is(err, db.ErrNotFound) {
			return c.NoContent(http.StatusNotFound)
		}
		return errors.Wrap(err, "get")
	}

	return c.JSON(http.StatusOK, FromDBPet(dbp))
}

func (a PetAPI) RandomGet(c echo.Context) error {
	dbp, err := a.dao.RandomGet()
	if err != nil {
		return errors.Wrap(err, "random")
	}

	return c.JSON(http.StatusOK, FromDBPet(dbp))
}

func (a PetAPI) Update(c echo.Context) error {
	p := new(Pet)
	if err := c.Bind(p); err != nil {
		return errors.Wrap(err, "bind")
	}

	p.ID = getIDParam(c)
	dbp := ToDBPet(p)
	dbp2, err := a.dao.UpdateAndGet(dbp)
	if err != nil {
		return errors.Wrap(err, "update")
	}

	return c.JSON(http.StatusOK, FromDBPet(dbp2))
}

func (a PetAPI) Delete(c echo.Context) error {
	id := getIDParam(c)
	if err := a.dao.Delete(id); err != nil {
		return errors.Wrap(err, "delete")
	}

	return c.NoContent(http.StatusNoContent)
}

type PetAPIPageReq struct {
	Page     int `query:"page"`
	PageSize int `query:"page_size"`
}

type PetAPIPageRes struct {
	Count int    `json:"count"`
	Items []*Pet `json:"items"`
}

func (a PetAPI) Page(c echo.Context) error {
	var req PetAPIPageReq
	if err := (&echo.DefaultBinder{}).BindQueryParams(c, &req); err != nil {
		return errors.Wrap(err, "bind")
	}

	items, count, err := a.dao.Page(db.PetPageParams{
		Page:     req.Page,
		PageSize: req.PageSize,
	})
	if err != nil {
		return errors.Wrap(err, "page")
	}

	res := PetAPIPageRes{
		Count: count,
	}

	for _, i := range items {
		res.Items = append(res.Items, FromDBPet(i))
	}

	return c.JSON(http.StatusOK, res)
}

func Run() {
	useCache := true
	var dao db.PetDao
	d := db.MustOpen()
	if useCache {
		rdb := eredis.MustOpen()
		dao = db.NewPetCacheDao(d, rdb)
	} else {
		dao = db.NewPetDBDao(d)
	}

	api := NewPetAPI(dao)

	e := echo.New()
	e.Use(middleware.Logger())
	e.Pre(middleware.RemoveTrailingSlash())
	{
		g := e.Group("api")
		g.GET("/pets", api.Page)
		g.POST("/pets", api.Create)
		g.GET("/pets/:id", api.Get)
		g.GET("/pets/random", api.RandomGet)
		g.PUT("/pets/:id", api.Update)
		g.DELETE("/pets/:id", api.Delete)
	}

	e.Logger.Fatal(e.Start(":1323"))
}
