
## Performance testing

`docker update --memory=800M --memory-swap=800M mysql`

### Broken pipe

压测中经常会出现 broken pipe 错误, 根据下面的日志, 对应请求的 page_size 都快 3000 了, 估
计和响应体过大有关.

```json
{
  "time": "2021-10-04T15:57:43.192673857+08:00",
  "id": "",
  "remote_ip": "::1",
  "host": "localhost:1323",
  "method": "GET",
  "uri": "/api/pets?page=0&page_size=2795",
  "user_agent": "python-requests/2.26.0",
  "status": 200,
  "error": "write tcp [::1]:1323-\u003e[::1]:33514: write: broken pipe",
  "latency": 2546444246,
  "latency_human": "2.546444246s",
  "bytes_in": 0,
  "bytes_out": 3958
}
```

### 200-users-without-cache

![200-users-without-cache](https://gitlab.com/ionling/one/uploads/ba1fc87bc6f99279511f9d566fb431a5/200-users-without-cache.png)

### qps-missing-key

请求 `/api/pets/999999`, 在 100, 200, 300 用户数量下的情况:

![qps-missing-key](https://gitlab.com/ionling/one/uploads/5db35657dfee3d4d5ac43ab5c25d94bc/qps-missing-key.png)

### qps-missing-key-with-fasthttp

将 HttpUser, 更换成 FastHttpUser 之后的情况, 可以看出 QPS 直接翻倍了, 但是随着请求数量的
上升, QPS 反而开始下降了, 系统的响应时间也增加了.

![qps-missing-key-with-fasthttp](https://gitlab.com/ionling/one/uploads/fafa8fc66a157814114340655eefe25a/qps-missing-key-with-fasthttp.png)

## qps-missing-key-with-redis

使用 redis, 处理了缓存穿透, QPS 最高能跑到 2500 左右, 且响应时间很稳定, 都在 30ms 左右.
不过随着用户数量的增加, QPS 未有明显提升, 估计是由于机器性能限制造成的.

![qps-missing-key-with-redis](https://gitlab.com/ionling/one/uploads/77236414758fc3eaf5b0924ecd9df282/qps-missing-key-with-redis.png)
