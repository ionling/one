package dislock

import (
	"fmt"
	"sync"

	"github.com/pkg/errors"
	clientv3 "go.etcd.io/etcd/client/v3"
	"go.etcd.io/etcd/client/v3/concurrency"

	"example"
)

var Examples = []example.Example{
	{
		FuncErr: PlusWithEtcd,
		Desc: `
编写的时候错误的使用了一个全局 session (不是在每个 goroutine 里面初始化), 导致 count
比预期少, 原因有待研究.
`,
	},
}

func PlusWithEtcd() (err error) {
	count := 0
	do := func() error {
		clt, err := clientv3.New(clientv3.Config{
			Endpoints: []string{"localhost:2379"},
		})
		if err != nil {
			return errors.Wrap(err, "new client")
		}
		defer clt.Close()

		s, err := concurrency.NewSession(clt)
		if err != nil {
			return errors.Wrap(err, "new session")
		}
		defer s.Close()

		l := concurrency.NewLocker(s, "example")
		l.Lock()
		defer l.Unlock()

		count++
		return nil
	}

	var (
		wg          sync.WaitGroup
		concurrency = 300
	)

	for i := 0; i < concurrency; i++ {
		if err != nil {
			break
		}

		wg.Add(1)
		go func() {
			defer wg.Done()
			if err2 := do(); err2 != nil {
				err = err2
			}
		}()
	}

	wg.Wait()
	fmt.Printf("count++ in %d goroutines\n", concurrency)
	fmt.Printf("equal: %v, count: %d, expected: %d\n",
		count == concurrency, count, concurrency)
	return err
}
