module example

go 1.19

require (
	github.com/avast/retry-go/v3 v3.1.1
	github.com/brianvoe/gofakeit/v6 v6.8.0
	github.com/coreos/etcd v3.3.27+incompatible
	github.com/dustin/go-humanize v1.0.0 // indirect
	github.com/go-redis/redis/v8 v8.11.4
	github.com/google/uuid v1.3.0 // indirect
	github.com/json-iterator/go v1.1.12 // indirect
	github.com/labstack/echo/v4 v4.6.1
	github.com/pkg/errors v0.9.1
	github.com/rs/zerolog v1.23.0
	github.com/spf13/cobra v1.0.0
	github.com/stretchr/testify v1.7.0
	golang.org/x/sync v0.0.0-20210220032951-036812b2e83c
	golang.org/x/time v0.0.0-20201208040808-7e3f01d25324
	gorm.io/driver/mysql v1.1.2
	gorm.io/gorm v1.21.15
	sigs.k8s.io/yaml v1.3.0 // indirect
)
