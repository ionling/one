package channel

import (
	"fmt"
	"sync"
	"time"

	"example"
)

type Channel struct {
	lock  sync.Mutex
	value interface{}
}

func (c *Channel) Read() (res interface{}) {
	for {
		c.lock.Lock()
		if c.value != nil {
			res = c.value
			c.value = nil
			c.lock.Unlock()
			return
		}

		c.lock.Unlock()
		time.Sleep(time.Nanosecond)
	}
}

func (c *Channel) Write(x interface{}) {
	for {
		c.lock.Lock()
		if c.value == nil {
			c.value = x
			c.lock.Unlock()
			return
		}

		c.lock.Unlock()
		time.Sleep(time.Nanosecond)
	}
}

var Examples = []example.Example{
	{
		Func: ReadFromClosed,
		Desc: `从已关闭 channel 中读取数据, channel 中还有一个元素.`,
	},
}

func ReadFromClosed() {
	ch := make(chan int, 1)
	ch <- 1
	close(ch)

	n, ok := <-ch
	fmt.Println(n, ok)
}
