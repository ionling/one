
## Benchmarks

```
go version go1.16.6 darwin/amd64
goos: darwin
goarch: amd64
pkg: example/channel
cpu: Intel(R) Core(TM) i7-1068NG7 CPU @ 2.30GHz
BenchmarkBuiltinChannel-8        7256190               163.8 ns/op
# time.Sleep(time.Nanosecond)
BenchmarkMyChannel-8             1276522               937.7 ns/op
# time.Sleep(time.Microsecond)
BenchmarkMyChannel-8              161800              6209 ns/op
# time.Sleep(time.Millisecond)
BenchmarkMyChannel-8                 657           1670619 ns/op

branch chan-cas (with CAS):
BenchmarkMyChannel-8             1656423               924.8 ns/op
```
