package channel

import (
	"sync"
	"testing"
	"time"
)

func BenchmarkBuiltinChannel(b *testing.B) {
	ch := make(chan struct{})
	go func() {
		for {
			<-ch
		}
	}()
	for i := 0; i < b.N; i++ {
		ch <- struct{}{}
	}
}

func BenchmarkMyChannel(b *testing.B) {
	ch := Channel{}
	go func() {
		for {
			ch.Read()
		}
	}()
	for i := 0; i < b.N; i++ {
		ch.Write(struct{}{})
	}
}

func TestThreadSafety(t *testing.T) {
	var (
		n     = 10000
		goNum = 10
		wg    sync.WaitGroup
	)

	run := func(
		name string,
		read func() interface{},
		write func(x interface{}),
	) {
		t.Run(name, func(t *testing.T) {
			var count int

			go func() {
				for {
					read()
					count++
				}
			}()

			wg.Add(goNum)
			for i := 0; i < goNum; i++ {
				go func() {
					for i := 0; i < n/goNum; i++ {
						write(i)
					}
					wg.Done()
				}()
			}

			wg.Wait()
			time.Sleep(time.Nanosecond) // Wait channel to be read
			if count != n {
				t.Errorf("count: %d, n: %d", count, n)
			}
		})
	}

	var (
		ch   = make(chan interface{})
		myCh = Channel{}
	)

	run(
		"builtin",
		func() interface{} { return <-ch },
		func(x interface{}) { ch <- x },
	)
	run(
		"my",
		func() interface{} { return myCh.Read() },
		func(x interface{}) { myCh.Write(x) },
	)
}
