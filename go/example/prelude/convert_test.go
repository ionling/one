package prelude

type Withdrawal struct{}

type Balance struct{}

type BalanceLog struct{}

func (wd *Withdrawal) Into() *BalanceLog {
	return nil
}

// method Withdrawal.Into already declared
// func (wd *Withdrawal) Into() *Balance {
// 	return nil
// }
