package prelude

type From[T any] interface {
	From(T)
}

type Into[T any] interface {
	Into() T
}
