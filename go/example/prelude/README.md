# prelude

## Convert

We are unable to implement the Rust `From` and `Into` traits,
due to the lack of function overloading[^fl].

[^fl]: https://stackoverflow.com/a/6987002
