package app

import (
	"context"
	"log"
	"net"

	_ "github.com/mattn/go-sqlite3"
	"go.uber.org/fx"
	"go.uber.org/zap"
	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"

	v1 "micro/api/v1"
	"micro/config"
	"micro/ent"
	"micro/service"
)

func New() *fx.App {
	return fx.New(
		fx.Provide(
			openDB,
			newLogger,
			newGRPCServer,
			config.ReadConfig,
			service.NewChinookSrv,
		),
		fx.Invoke(
			registerGRPC,
		),
	)
}

func openDB(lc fx.Lifecycle, conf *config.DB) (client *ent.Client, err error) {
	client, err = ent.Open(conf.Driver, conf.DSN)
	lc.Append(fx.Hook{
		OnStop: func(c context.Context) error {
			return client.Close()
		},
	})
	return
}

func newLogger(lc fx.Lifecycle) (logger *zap.SugaredLogger, err error) {
	p, err := zap.NewProduction()
	logger = p.Sugar()
	return
}

func newGRPCServer(
	lc fx.Lifecycle,
	conf *config.Server,
	logger *zap.SugaredLogger,
) (server *grpc.Server) {
	var opts []grpc.ServerOption
	server = grpc.NewServer(opts...)

	lc.Append(fx.Hook{
		OnStart: func(ctx context.Context) error {
			lis, err := net.Listen("tcp", conf.ListenAddr)
			if err != nil {
				return err
			}

			go func() {
				logger.Infof("Start serving at %s", lis.Addr())
				if err := server.Serve(lis); err != nil {
					log.Fatalln("server serve:", err)
				}
			}()

			return nil
		},
		OnStop: func(c context.Context) error {
			server.GracefulStop()
			return nil
		},
	})

	return
}

func registerGRPC(server *grpc.Server, chinookSrv v1.ChinookSvcServer) {
	v1.RegisterChinookSvcServer(server, chinookSrv)
	reflection.Register(server)
}
