package main

import "micro/app"

func main() {
	app.New().Run()
}
