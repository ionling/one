package service

import (
	"context"

	v1 "micro/api/v1"
	"micro/ent"
)

type chinookSvc struct {
	v1.UnimplementedChinookSvcServer
	db *ent.Client
}

func NewChinookSrv(db *ent.Client) v1.ChinookSvcServer {
	return chinookSvc{
		db: db,
	}
}

func (s chinookSvc) GetEmployee(
	ctx context.Context,
	req *v1.GetEmployeeReq,
) (*v1.Employee, error) {
	row, err := s.db.Employee.Get(ctx, req.GetId())
	if err != nil {
		return nil, err
	}

	return &v1.Employee{
		EmployeeId: row.ID,
		LastName:   row.LastName,
		FirstName:  row.FirstName,
		Title:      row.Title,
		ReportsTo:  row.ReportsTo,
		BirthDate:  row.BirthDate.Unix(),
		HireDate:   row.HireDate.Unix(),
		Address:    row.Address,
		City:       row.City,
		State:      row.State,
		Country:    row.Country,
		PostalCode: row.PostalCode,
		Phone:      row.Phone,
		Fax:        row.Fax,
		Email:      row.Email,
	}, nil
}
