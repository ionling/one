package schema

import (
	"entgo.io/ent"
	"entgo.io/ent/dialect/entsql"
	"entgo.io/ent/schema"
	"entgo.io/ent/schema/field"
)

// Employee holds the schema definition for the Employee entity.
type Employee struct {
	ent.Schema
}

func (Employee) Annotations() []schema.Annotation {
	return []schema.Annotation{
		entsql.Annotation{Table: "Employee"},
	}
}

// Fields of the Employee.
func (Employee) Fields() []ent.Field {
	return []ent.Field{
		field.Uint32("id").StorageKey("EmployeeId"),
		field.String("LastName").StorageKey("LastName"),
		field.String("FirstName").StorageKey("FirstName"),
		field.String("Title").StorageKey("Title"),
		field.Uint32("ReportsTo").StorageKey("ReportsTo"),
		field.Time("BirthDate").StorageKey("BirthDate"),
		field.Time("HireDate").StorageKey("HireDate"),
		field.String("Address").StorageKey("Address"),
		field.String("City").StorageKey("City"),
		field.String("State").StorageKey("State"),
		field.String("Country").StorageKey("Country"),
		field.String("PostalCode").StorageKey("PostalCode"),
		field.String("Phone").StorageKey("Phone"),
		field.String("Fax").StorageKey("Fax"),
		field.String("Email").StorageKey("Email"),
	}
}

// Edges of the Employee.
func (Employee) Edges() []ent.Edge {
	return nil
}
