module micro

go 1.17

require (
	entgo.io/ent v0.9.1
	go.uber.org/zap v1.16.0
	google.golang.org/grpc v1.33.2
	google.golang.org/protobuf v1.25.0
)

require (
	go.uber.org/atomic v1.6.0 // indirect
	go.uber.org/dig v1.12.0 // indirect
	go.uber.org/multierr v1.5.0 // indirect
)

require (
	github.com/BurntSushi/toml v0.4.1
	github.com/golang/protobuf v1.4.3 // indirect
	github.com/google/uuid v1.3.0 // indirect
	github.com/mattn/go-sqlite3 v1.14.9
	github.com/pkg/errors v0.9.1
	go.uber.org/fx v1.15.0
	golang.org/x/net v0.0.0-20210405180319-a5a99cb37ef4 // indirect
	golang.org/x/sys v0.0.0-20211007075335-d3039528d8ac // indirect
	golang.org/x/text v0.3.3 // indirect
	google.golang.org/genproto v0.0.0-20200526211855-cb27e3aa2013 // indirect
)
