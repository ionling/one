package config

import (
	"os"

	"github.com/BurntSushi/toml"
	"github.com/pkg/errors"
	"go.uber.org/fx"
)

const configPath = "config/config.toml"

type Config struct {
	fx.Out

	DB     *DB
	Server *Server
}

type DB struct {
	Driver string
	DSN    string
}

type Server struct {
	ListenAddr string
}

func ReadConfig() (conf Config, err error) {
	data, err := os.ReadFile(configPath)
	if err != nil {
		err = errors.Wrap(err, "read")
		return
	}

	err = toml.Unmarshal(data, &conf)
	if err != nil {
		err = errors.Wrap(err, "unmarshal")
		return
	}

	return
}
