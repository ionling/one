package lru

import (
	"container/list"
)

type entry struct {
	key   string
	value Value
}

// Value use Len to count how many bytes it takes.
type Value interface {
	Len() int
}

// onEvictedFunc is a callback when an entry is purged.
type onEvictedFunc func(key string, val Value)

type Cache struct {
	maxBytes  int64 // Max bytes of this cache
	nbytes    int64 // Total used bytes of this cache
	l         *list.List
	cache     map[string]*list.Element
	onEvicted onEvictedFunc
}

// New get a new LRUCache.
// If `maxBytes` set to 0, there is no memory limit.
func New(maxBytes int64, onEvicted onEvictedFunc) *Cache {
	return &Cache{
		maxBytes:  maxBytes,
		l:         list.New(),
		cache:     map[string]*list.Element{},
		onEvicted: onEvicted,
	}
}

// Get looks up a key's value.
func (c *Cache) Get(key string) (val Value, ok bool) {
	if v, ok := c.cache[key]; ok {
		c.l.MoveToFront(v)
		kv := v.Value.(*entry)
		return kv.value, true
	}
	return
}

// Set set key's value.
func (c *Cache) Set(key string, val Value) {
	if v, ok := c.cache[key]; ok {
		c.l.MoveToFront(v)
		en := v.Value.(*entry)
		c.nbytes += int64(val.Len() - en.value.Len())
		en.value = val
	} else {
		el := c.l.PushFront(&entry{key: key, value: val})
		c.cache[key] = el
		c.nbytes += int64(len(key) + val.Len())
	}
	for c.maxBytes != 0 && c.maxBytes < c.nbytes {
		c.RemoveOldest()
	}
}

// Len get the number of keys.
func (c *Cache) Len() int {
	return c.l.Len()
}

// RemoveOldest removes the oldest item.
func (c *Cache) RemoveOldest() {
	el := c.l.Back()
	if el == nil {
		return
	}

	c.l.Remove(el)
	en := el.Value.(*entry)
	delete(c.cache, en.key)
	c.nbytes -= int64(len(en.key) + en.value.Len())
	if c.onEvicted != nil {
		c.onEvicted(en.key, en.value)
	}
}
