package lru

import (
	"reflect"
	"testing"
)

type String string

func (s String) Len() int {
	return len(s)
}

func TestGetSet(t *testing.T) {
	lru := New(0, nil)
	k1, v1 := "key1", String("value1")
	k2 := "key2"

	lru.Set(k1, v1)
	v, ok := lru.Get(k1)
	if !ok {
		t.Fatalf("get %s failed", k1)
	}
	if v.(String) != v1 {
		t.Fatalf("get %s value %s, expect %s", k1, v, v1)
	}

	if _, ok := lru.Get(k2); ok {
		t.Fatalf("get unset %s", k2)
	}
}

func TestLen(t *testing.T) {
	lru := New(0, nil)
	lru.Set("key1", String("value1"))
	expect := 1
	if lru.Len() != expect {
		t.Fatalf("get length %d, expect %d", lru.Len(), expect)
	}

	lru.Set("key2", String("value2"))
	expect = 2
	if lru.Len() != expect {
		t.Fatalf("get length %d, expect %d", lru.Len(), expect)
	}
	lru.Set("key2", String("value2"))
	if lru.Len() != expect {
		t.Fatalf("reset key2, get length %d, expect %d", lru.Len(), expect)
	}
}

func TestRemoveOldest(t *testing.T) {
	lru := New(10, nil)
	k1, v1 := "key1", String("value1") // 10 bytes
	k2, v2 := "k2", String("value2")   // 8 bytes
	k3, v3 := "k3", String("v3")       // 4 bytes

	// Add 10 bytes
	lru.Set(k1, v1)
	lru.Set(k2, String(v2))
	if _, ok := lru.Get(k1); ok {
		t.Fatalf("key %s should be removed", k1)
	}
	v2g, ok := lru.Get(k2)
	if !ok {
		t.Fatalf("key %s should exist", k2)
	}
	if v2g != v2 {
		t.Fatalf("key %s value should be '%s', get '%s'", k2, v2, v2g)
	}

	// Add 4 bytes
	lru.Set(k3, v3)
	if _, ok := lru.Get(k2); ok {
		t.Fatalf("key %s should be removed", k2)
	}
	v3g, ok := lru.Get(k3)
	if !ok {
		t.Fatalf("key %s should exist", k3)
	}
	if v2g != v2 {
		t.Fatalf("key %s value should be '%s', get '%s'", k3, v3, v3g)
	}
}

func TestOnEvicted(t *testing.T) {
	keys := make([]string, 0)
	onEvicted := func(key string, value Value) {
		keys = append(keys, key)
	}
	lru := New(int64(10), onEvicted)
	lru.Set("key1", String("123456"))
	lru.Set("k2", String("k2"))
	lru.Set("k3", String("k3"))
	lru.Set("k4", String("k4"))

	expect := []string{"key1", "k2"}
	if !reflect.DeepEqual(expect, keys) {
		t.Fatalf("Call OnEvicted failed, expect keys equals to %s, got %s", expect, keys)
	}
}
