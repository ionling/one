package geecache

import (
	"fmt"
	"log"
	"testing"

	"github.com/stretchr/testify/assert"
)

var db = map[string]string{
	"Tom":  "630",
	"Jack": "589",
	"Sam":  "567",
}

func TestGet(t *testing.T) {
	loadCounts := make(map[string]int, len(db))
	gee := NewGroup("scores", 2<<10, GetterFunc(
		func(key string) ([]byte, error) {
			log.Println("[SlowDB] search key", key)
			if v, ok := db[key]; ok {
				if _, ok := loadCounts[key]; !ok {
					loadCounts[key] = 0
				}
				loadCounts[key] += 1
				return []byte(v), nil
			}
			return nil, fmt.Errorf("%s not exist", key)
		}))

	for k, v := range db {
		// Load from callback function
		view, err := gee.Get(k)
		assert.NoError(t, err)
		assert.EqualValues(t, v, view.String())

		// Cache hit
		_, err = gee.Get(k)
		assert.NoError(t, err)
		assert.EqualValues(t, 1, loadCounts[k])
	}

	_, err := gee.Get("unknown")
	assert.ErrorContains(t, err, "not exist")
}
