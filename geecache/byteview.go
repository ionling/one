package geecache

// ByteView holds an immutable view of bytes.
type ByteView struct {
	b []byte
}

func (bv ByteView) Len() int {
	return len(bv.b)
}

// ByteSlice returns a copy of data as a byte slice.
func (bv ByteView) ByteSlice() []byte {
	res := make([]byte, len(bv.b))
	copy(res, bv.b)
	return res
}

// String returns the data as a string.
func (bv ByteView) String() string {
	return string(bv.b)
}
