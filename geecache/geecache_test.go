package geecache

import (
	"slices"
	"testing"
)

func TestGetter(t *testing.T) {
	var f Getter = GetterFunc(func(key string) ([]byte, error) {
		return []byte(key), nil
	})

	expect := []byte("key")
	if v, _ := f.Get("key"); !slices.Equal(v, expect) {
		t.Errorf("callback failed")
	}
}
