package geecache

import (
	"fmt"
	"log"
	"slices"
	"sync"
)

// Getter loads data for a key.
type Getter interface {
	Get(key string) ([]byte, error)
}

// GetterFunc implements the [Getter] interface for a function.
type GetterFunc func(key string) ([]byte, error)

// Get implements [Getter] interface.
func (f GetterFunc) Get(key string) ([]byte, error) {
	return f(key)
}

// Group represents a cache namespace, distributing associated data across multiple sources.
type Group struct {
	name   string
	getter Getter
	cache  cache
}

var (
	mu     sync.RWMutex
	groups = make(map[string]*Group)
)

// NewGroup creates a new instance of Group.
func NewGroup(name string, maxBytes int64, getter Getter) *Group {
	if getter == nil {
		panic("nil getter")
	}

	g := &Group{
		name:   name,
		getter: getter,
		cache:  cache{maxBytes: maxBytes},
	}
	mu.Lock()
	groups[name] = g
	mu.Unlock()
	return g
}

// GetGroup returns the named group previously created with NewGroup,
// or nil if there's no such group.
func GetGroup(name string) *Group {
	mu.RLock()
	g := groups[name]
	mu.RUnlock()
	return g
}

// Get returns value for a key from cache.
func (g *Group) Get(key string) (ByteView, error) {
	if key == "" {
		return ByteView{}, fmt.Errorf("key is required")
	}

	if v, ok := g.cache.get(key); ok {
		log.Println("[GeeCache] hit")
		return v, nil
	}

	return g.load(key)
}

func (g *Group) load(key string) (value ByteView, err error) {
	return g.getLocally(key)
}

func (g *Group) getLocally(key string) (ByteView, error) {
	bytes, err := g.getter.Get(key)
	if err != nil {
		return ByteView{}, err
	}
	value := ByteView{b: slices.Clone(bytes)}
	g.populateCache(key, value)
	return value, nil
}

func (g *Group) populateCache(key string, value ByteView) {
	g.cache.set(key, value)
}
