
1. `init` function in one file will only execute one time.
2. Each file in a package can have their own `init` function.
