package main

import (
	"fmt"
	"go-init/person"
	"go-init/school"
)

func main() {
	var p = person.Person{Name: "ling"}
	var s = school.School{Principal: p}

	fmt.Println(s, p)
}
