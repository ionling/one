module gonat

go 1.23.0

require (
	go.uber.org/multierr v1.11.0
	golang.org/x/net v0.29.0
)

require (
	golang.org/x/crypto v0.27.0 // indirect
	golang.org/x/sys v0.25.0 // indirect
)
