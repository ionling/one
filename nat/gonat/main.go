package main

import (
	"bytes"
	"context"
	"fmt"
	"log"
	"net"
	"os"
	"strconv"
	"sync"
	"time"

	"go.uber.org/multierr"
	"golang.org/x/net/quic"
)

var (
	addrRespPrefix = []byte("addr|")
)

func main() {
	if len(os.Args) < 3 {
		log.Fatal("No action")
	}

	var err error
	addr := os.Args[2]
	if os.Args[1] == "client" {
		err = Client(addr)
	} else {
		err = Server(addr)
	}
	if err != nil {
		log.Fatal(err)
	}
}

func Client(server string) error {
	saddr, err := net.ResolveUDPAddr("udp4", server)
	if err != nil {
		return wrap(err, "resolve server addr")
	}

	laddr, err := net.ResolveUDPAddr("udp4", "0.0.0.0:8900")
	if err != nil {
		return wrap(err, "resolve local addr")
	}

	conn, err := net.ListenUDP("udp", laddr)
	if err != nil {
		return wrap(err, "listen")
	}
	defer conn.Close()

	log.Println("Local address", conn.LocalAddr())

	_, err = conn.WriteToUDP([]byte("0"), saddr)
	if err != nil {
		return wrap(err, "ping server")
	}

	for {
		buffer := make([]byte, 1024)
		n, remoteAddr, err := conn.ReadFromUDP(buffer)
		if err != nil {
			log.Println("Read udp", err)
			continue
		}

		msg := buffer[:n]
		fmt.Printf("Client received: %v %s\n", remoteAddr, msg)
		if bytes.HasPrefix(msg, addrRespPrefix) {
			go func() {
				addr, err := parseAddr(bytes.TrimLeft(msg, string(addrRespPrefix)))
				if err != nil {
					log.Println("Parse addr", err)
					return
				}
				ctx := context.Background()
				if err := handshakeQUIC(ctx, laddr.String(), addr.String()); err != nil {
					log.Println("Handshake", err)
				}
			}()
		} else {
			if _, err := conn.WriteToUDP([]byte("Cool!"), remoteAddr); err != nil {
				log.Println("Pong", err)
			}
		}

	}
}

var addresses []*net.UDPAddr

func Server(addr string) error {
	udpAddr, err := net.ResolveUDPAddr("udp", addr)
	if err != nil {
		return wrap(err, "resolve addr")
	}

	conn, err := net.ListenUDP("udp", udpAddr)
	if err != nil {
		return wrap(err, "listen")
	}
	defer conn.Close()

	log.Printf("Server listening on %s", addr)

	var mu sync.Mutex
	for {
		buffer := make([]byte, 1024)
		_, remoteAddr, err := conn.ReadFromUDP(buffer)
		if err != nil {
			log.Println("Error reading from UDP:", err)
			continue
		}
		mu.Lock()

		log.Printf("Connection from: %v", remoteAddr)
		addresses = append(addresses, remoteAddr)

		if len(addresses) == 2 {
			log.Printf("Server - send client info to: %v", addresses[0])
			_, err = conn.WriteToUDP(addrResp(addresses[1]), addresses[0])
			if err != nil {
				log.Println("Error writing to UDP:", err)
			}

			log.Printf("Server - send client info to: %v", addresses[1])
			_, err = conn.WriteToUDP(addrResp(addresses[0]), addresses[1])
			if err != nil {
				log.Println("Error writing to UDP:", err)
			}

			// Remove the first two addresses
			addresses = addresses[2:]
		}

		mu.Unlock()
	}
}

func addrResp(addr *net.UDPAddr) []byte {
	return append(addrRespPrefix, []byte(addr.String())...)
}

func parseAddr(data []byte) (*net.UDPAddr, error) {
	return net.ResolveUDPAddr("udp", string(data))
}

func wrap(err error, msg string) error {
	if err == nil {
		return nil
	}
	return fmt.Errorf("%s: %w", msg, err)
}

func handshakeUDP(conn *net.UDPConn, dst *net.UDPAddr) (err error) {
	_, err = conn.WriteToUDP([]byte("1"), dst)
	if err != nil {
		return wrap(err, "first")
	}

	time.Sleep(1000 * time.Millisecond)
	_, err = conn.WriteToUDP([]byte("2"), dst)
	if err != nil {
		return wrap(err, "second")
	}
	return nil
}

func handshakeQUIC(ctx context.Context, local, remote string) error {
	ep, err := quic.Listen("udp", local, nil)
	if err != nil {
		return wrap(err, "listen")
	}
	defer multierr.AppendInto(&err, ep.Close(ctx))

	conn, err := ep.Dial(ctx, "udp", remote, nil)
	if err != nil {
		return wrap(err, "dial")
	}

	s, err := conn.NewStream(ctx)
	if err != nil {
		return wrap(err, "new stream")
	}

	if err = s.WriteByte(98); err != nil {
		return wrap(err, "handshake")
	}

	go func() {
		for i := 0; i < 9; i++ {
			log.Printf("send %dth message", i)
			if _, err := s.Write([]byte("hello" + strconv.Itoa(i))); err != nil {
				log.Printf("got err: %v", err)
			}
			time.Sleep(time.Second)
		}
		if err := s.Close(); err != nil {
			log.Printf("close handshake stream: %v", err)
		}
	}()

	go func() {
		s, err := conn.AcceptStream(ctx)
		if err != nil {
			log.Printf("accept stream: %v", err)
			return
		}

		buf := make([]byte, 1024)
		for {
			if n, err := s.Read(buf); err != nil {
				log.Printf("read stream: %s %v", buf[:n], err)
			}
		}
	}()
	return nil
}
