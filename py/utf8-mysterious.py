words = "一些文字"
print(f"original text: {words}")

utf8_decoded = words.encode("gbk").decode(errors="replace")
print(f"decode gbk encoded bytes with utf8: {utf8_decoded}")

gbk_decoded = utf8_decoded.encode().decode("gbk")
print(f"reencode with utf8 and decode with gbk: {gbk_decoded}")
