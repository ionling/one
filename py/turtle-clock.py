# For Arch:
#   sudo pacman -S tk
#
# https://sm.ms/image/RprSkFjnyqIXP2Y

import turtle


def q1():
    for _ in range(3):
        turtle.circle(-100)
        turtle.left(30)


def q2():
    p1 = turtle.Pen()
    p2 = turtle.Pen()
    p1.right(90)
    p2.left(160)
    p1.left(70)
    # p2.right(70)
    p1.forward(100)
    p2.forward(100)


def q3():
    def draw_scale():
        scale_radius = 100
        scale_lenght = 10
        scale = turtle.Pen()
        scale.left(90)
        scale.penup()
        for i in range(60):
            if i % 5 == 0:
                scale.color("blue")
                scale.pensize(5)
            else:
                scale.color("black")
                scale.pensize(1)

            scale.forward(scale_radius)
            scale.pendown()
            scale.forward(scale_lenght)
            scale.penup()
            scale.bk(scale_radius + scale_lenght)
            scale.right(360 // 60)

    def draw_hands():
        hour_hand_length = 30
        minute_hand_length = 60
        second_hand_length = 80

        hour_hand = turtle.Pen()
        hour_hand.left(90)
        hour_hand.pensize(6)

        minute_hand = turtle.Pen()
        minute_hand.left(90)
        minute_hand.pensize(3)

        second_hand = turtle.Pen()
        second_hand.left(90)
        second_hand.pensize(1)
        second_hand.color("darkred")

        second_angle = 360 // 60
        minute_angle = second_angle / 60
        hour_angle = minute_angle / 60
        for _ in range(10000):
            hour_hand.fd(hour_hand_length)
            hour_hand.bk(hour_hand_length)
            minute_hand.fd(minute_hand_length)
            minute_hand.bk(minute_hand_length)
            second_hand.fd(second_hand_length)
            second_hand.bk(second_hand_length)

            hour_hand.clear()
            minute_hand.clear()
            second_hand.clear()

            hour_hand.right(hour_angle)
            minute_hand.right(minute_angle)
            second_hand.right(second_angle)

    draw_scale()
    draw_hands()


if __name__ == "__main__":
    q3()
