import unittest

import parserc


suite "parser":
  test "charp":
    let p = charp 'm'
    check p.parse("") == newErrParseResult EmptyStringErr
    check p.parse("abc") == newErrParseResult newExpectErr("m", "a")
    check p.parse("mbc") == newValParseResult (@["m"], "bc")

  test "andThen":
    let
      p1 = charp 'k'
      p2 = charp 'j'
      p = p1 >> p2

    check p.parse("") == newErrParseResult EmptyStringErr
    check p.parse("abc") == newErrParseResult newExpectErr("k", "a")
    check p.parse("kbc") == newErrParseResult newExpectErr("j", "b")
    check p.parse("kjc") == newValParseResult (@["k", "j"], "c")

  test "orElse":
    let
      p1 = charp 'r'
      p2 = charp 'k'
      p = p1 | p2

    check p.parse("") == newErrParseResult FailedBothErr
    check p.parse("abc") == newErrParseResult FailedBothErr
    check p.parse("mbc") == newErrParseResult FailedBothErr
    check p.parse("rjc") == newValParseResult (@["r"], "jc")
    check p.parse("kjc") == newValParseResult (@["k"], "jc")

  test "n":
    let p = 'r'.charp * 3

    check p.parse("") == newErrParseResult EmptyStringErr
    check p.parse("abc") == newErrParseResult newExpectErr("r", "a")
    check p.parse("rjc") == newErrParseResult newExpectErr("r", "j")
    check p.parse("rrc") == newErrParseResult newExpectErr("r", "c")
    check p.parse("rrrc") == newValParseResult (@["r", "r", "r"], "c")

  test "chioce":
    let p = choice @['1'.charp, '2'.charp, '3'.charp]

    check p.parse("") == newErrParseResult FailedBothErr
    check p.parse("abc") == newErrParseResult FailedBothErr
    check p.parse("1jc") == newValParseResult (@["1"], "jc")
    check p.parse("12c") == newValParseResult (@["1"], "2c")

  test "optional":
    let p = optional charp 'r'

    check p.parse("") == newValParseResult (@[], "")
    check p.parse("abc") == newValParseResult (@[], "abc")
    check p.parse("rjc") == newValParseResult (@["r"], "jc")

  test "many":
    let p = many charp 'r'

    check p.parse("") == newValParseResult (@[], "")
    check p.parse("abc") == newValParseResult (@[], "abc")
    check p.parse("rbc") == newValParseResult (@["r"], "bc")
    check p.parse("rrc") == newValParseResult (@["r", "r"], "c")

  test "many1":
    let p = many1 charp 'r'

    check p.parse("") == newErrParseResult EmptyStringErr
    check p.parse("abc") == newErrParseResult newExpectErr("r", "a")
    check p.parse("rbc") == newValParseResult (@["r"], "bc")
    check p.parse("rrc") == newValParseResult (@["r", "r"], "c")

  test "surroundedBy":
    let p = 'r'.charp.surroundedBy '\''.charp

    check p.parse("") == newErrParseResult EmptyStringErr
    check p.parse("abc") == newErrParseResult newExpectErr("'", "a")
    check p.parse("'bc") == newErrParseResult newExpectErr("r", "b")
    check p.parse("'rc") == newErrParseResult newExpectErr("'", "c")
    check p.parse("'r'") == newValParseResult (@["'", "r", "'"], "")

  test "between":
    let p = 'r'.charp.between('('.charp, ')'.charp)

    check p.parse("") == newErrParseResult EmptyStringErr
    check p.parse("abc") == newErrParseResult newExpectErr("(", "a")
    check p.parse("(bc") == newErrParseResult newExpectErr("r", "b")
    check p.parse("(rc") == newErrParseResult newExpectErr(")", "c")
    check p.parse("(r)") == newValParseResult (@["(", "r", ")"], "")
