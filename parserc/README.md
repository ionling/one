# parserc

A parser combinator package in Nim.  
Refer [Day 21: Parser combinators - Nim Days](https://xmonader.github.io/nimdays/day21_parsec.html).

Others:

1. [parser combinator library in Nim](https://gist.github.com/kmizu/2b10c2bf0ab3eafecc1a825b892482f3)
2. [PMunch/combparser: A parser combinator library...](https://github.com/PMunch/combparser)
3. [Building a useful set of parser combinators...](https://fsharpforfunandprofit.com/posts/understanding-parser-combinators-2/)
