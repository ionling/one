import sequtils
import strformat
import sugar


const
  FailedBothErr* = "Failed both"
  EmptyStringErr* = "Empty string"


func newExpectErr*(expect: string, actual: string): string =
  &"Expecting '{expect}', found '{actual}'"


type
  ParseResultKind* = enum
    prkErr, prkVal

  ParseValue* = tuple[parsed: seq[string], remaining: string]

  ParseResult* = ref object
    case kind*: ParseResultKind
    of prkErr: err*: string
    of prkVal: val*: ParseValue


func newErrParseResult*(err: string): ParseResult =
  ParseResult(kind: prkErr, err: err)


func newValParseResult*(val: ParseValue): ParseResult =
  ParseResult(kind: prkVal, val: val)


func `$`*(this: ParseResult): string =
  case this.kind
  of prkErr: &"<Error {this.err}>"
  of prkVal: &"<Value parsed: {this.val.parsed}, remaining: {this.val.remaining}>"


func `==`*(this: ParseResult, other: ParseResult): bool =
  if this.kind != other.kind:
    return false
  elif this.kind == prkErr:
    return this.err == other.err
  elif this.kind == prkVal:
    return this.val == other.val


type Parser = ref object
  f: string -> ParseResult
  suppressed: bool


func newParser(f: string -> ParseResult, suppressed = false): Parser =
  Parser(f: f, suppressed: suppressed)


proc parse*(this: Parser, s: string): ParseResult =
  return this.f s


proc suppress(this: Parser) =
  this.suppressed = true


proc charp*(c: char): Parser =
  proc f(s: string): ParseResult =
    if s == "":
      ParseResult(kind: prkErr, err: EmptyStringErr)
    elif s[0] == c:
      ParseResult(kind: prkVal, val: (@[$c], s[1..^1]) )
    else:
      ParseResult(kind: prkErr, err: &"Expecting '{c}', found '{s[0]}'")

  newParser f


proc andThen*(p1, p2: Parser): Parser =
  proc f(s: string): ParseResult =
    let res1 = p1.parse s
    case res1.kind
    of prkErr: res1
    of prkVal:
      let res2 = p2.parse res1.val.remaining
      case res2.kind:
        of prkErr: res2
        of prkVal:
          var parsed: seq[string]
          if not p1.suppressed:
            parsed.add(res1.val.parsed)
          if not p2.suppressed:
            parsed.add(res2.val.parsed)

          ParseResult(kind: prkVal, val: (parsed, res2.val.remaining))

  newParser f


proc `>>`*(p1: Parser, p2: Parser): Parser =
  andThen(p1, p2)


proc orElse*(p1, p2: Parser): Parser =
  proc f(s: string): ParseResult =
    let res1 = p1.parse s
    case res1.kind
    of prkVal:
      res1
    of prkErr:
      let res2 = p2.parse s
      case res2.kind
      of prkVal:
        res2
      of prkErr:
        newErrParseResult FailedBothErr

  newParser f


proc `|`*(p1, p2: Parser): Parser =
  orElse(p1, p2)


proc n*(p: Parser, count: int): Parser =
  proc f(s: string): ParseResult =
    var
      rem = s
      parsed: seq[string]

    for i in 1..count:
      let res = p.parse rem
      case res.kind
      of prkErr:
        return res
      of prkVal:
        parsed.add res.val.parsed
        rem = res.val.remaining

    newValParseResult (parsed, rem)

  newParser f


proc `*`*(this: Parser, count: int): Parser =
  n this, count


func choice*(ps: seq[Parser]): Parser =
  foldl ps, a | b


func optional*(p: Parser): Parser =
  p | newParser s => newValParseResult (@[], s)


func many*(p: Parser): Parser =
  proc f(s: string): ParseResult =
    var
      parsed: seq[string] = @[]
      rem = s

    while true:
        let res = p.parse rem
        case res.kind
        of prkErr:
          break
        of prkVal:
          rem = res.val.remaining
          parsed.add res.val.parsed

    newValParseResult (parsed, rem)

  newParser f


func many1*(p: Parser): Parser =
  proc f(s: string): ParseResult =
    let res = p.parse s
    case res.kind
    of prkErr:
      res
    of prkVal:
      many(p).parse s

  newParser f


func surroundedBy*(p, surround: Parser): Parser =
  return surround >> p >> surround


func between*(p, left, right: Parser): Parser =
  return left >> p >> right
