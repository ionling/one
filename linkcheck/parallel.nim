import httpclient
import threadpool

import linkcheck


proc checkLinkParallel(link: string): LinkCheckRes {.thread.} =
  result = LinkCheckRes(link: link)
  try:
    result.ok = newHttpClient().get(link).code == Http200
  except:
    result.ok = false


proc doCheckParallel(links: seq[string]) =
  var results = newSeq[FlowVar[LinkCheckRes]]()
  for l in links:
    results.add(spawn checkLinkParallel l)

  for r in results:
    print ^r


check doCheckParallel
