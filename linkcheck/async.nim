import asyncdispatch
import httpclient
import sugar

import linkcheck


proc checkLinkAsync(link: string): Future[LinkCheckRes] {.async.} =
  var ok: bool
  let future = newAsyncHttpClient().get(link)

  yield future
  if future.failed:
    ok = false
  else:
    let resp = future.read
    ok = resp.code == Http200

  return LinkCheckRes(link: link, ok: ok)


proc doCheckAsync(links: seq[string]) {.async.} =
  var futures = newSeq[Future[LinkCheckRes]]()
  for l in links:
    futures.add(checkLinkAsync(l))

  let done = await all(futures)
  for x in done:
    print x


check (links: seq[string]) => waitFor doCheckAsync links
