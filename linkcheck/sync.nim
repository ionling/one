import httpclient

import linkcheck


proc checkLink(link: string): LinkCheckRes =
  var ok: bool
  try:
    ok = newHttpClient().get(link).code == Http200
  except:
    ok = false

  return LinkCheckRes(link: link, ok: ok)


proc doCheck(links: seq[string]) =
  for l in links:
     print checkLink l


check doCheck
