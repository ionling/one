# linkcheck

A simple link check tool implemented with Nim.

REF [Day 4: LinksChecker](https://xmonader.github.io/nimdays/day04_asynclinkschecker.html).

Run:

```sh
nim c -r -d:ssl sync.nim
nim c -r -d:ssl async.nim
nim c -r -d:ssl --threads:on parallel.nim
```
