import strutils
import sugar
import times


type
  LinkCheckRes* = ref object
    link*: string
    ok*: bool


proc print*(res: LinkCheckRes) =
  echo res.link, " is ", res.ok


proc readLinks(): seq[string] =
  open("links.txt").readAll().strip.splitLines


proc check*(p: seq[string] -> void) =
  let start = epochTime()
  p readLinks()
  echo epochTime() - start
