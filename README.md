# one

[![pre-commit](https://img.shields.io/badge/pre--commit-enabled-brightgreen?logo=pre-commit&logoColor=white)](https://github.com/pre-commit/pre-commit)

## Index

- assistant
  - todo
- container
  - dstop
- coroutine
- data
  - banks
- demos
  - dapr
  - db-migration
    - goose
  - kratos2
  - micro - An example of microservice using ent, fx, gRPC
  - validator - github.com/go-playground/validator
- go
  - bench
    - DNS - The benchmark of popular DNS servers
  - calc - Goa demo
  - example
    - basic
      - DefaultRand - Default rand without seeding
      - RandWithFixedSeed
      - RandWithTimeNow
    - channel - Implementing the channel
      - ReadFromClosed
    - concurrency
      - CancelOtherGoroutines
      - CancelOtherGoroutines2
      - CancelOtherGoroutinesWithErrGroup
      - PlusWithoutLock
      - PlusWithLock
    - db
      - oom - Out of memory
    - di - Dependency injection
      - fx
    - eredis
      - bench
    - impl
      - Mutex - Implementing the mutex
    - ranking
      - Redis
    - ratelimit
      - x/time/rate
    - retry
      - avast/retry-go
    - server
  - imptest [20240227] Import test
  - pkg
    - ptime
- rust
  - filez - A file archiver
- shareit
- tools
  - upcheck - [2023-11-15 Wed] An update check tool

## Changelog

1. [2023-03-03 Fri] Add filez, a file archiver, as a Rust learning project
1. [2023-02-01 Wed] Add DNS benchmark, to select the fastest DNS server
   - Supports traditional 53 port protocol only, no DoH or DOT
