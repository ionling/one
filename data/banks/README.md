## Notes

### Chinese characters in tabulate

Use `wcwidth` package to display Chinese characters properly in `tabulate`

See https://stackoverflow.com/a/66648740/7134763
