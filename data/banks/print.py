import json

from colorama import Back
from tabulate import tabulate


def format(x: str, color: str) -> str:
    if color == "red":
        c = Back.RED
    elif color == "green":
        c = Back.GREEN
    elif color == "blue":
        c = Back.BLUE
    elif color == "orange":
        c = Back.CYAN
    else:
        c = None

    return x if c is None else f"{c}{x}{Back.RESET}"


if __name__ == '__main__':
    with open("banks.json") as f:
        data = f.read()
        banks: list[dict] = json.loads(data)
        headers = ("Abbr", "Chinese", "English")
        table = [
            (
                format(b.get("shortName", ""), b.get("color", "")),
                b.get("bankName", ""),
                b.get("englishFullName", ""),
            )
            for b in banks
        ]
        print(tabulate(table, headers))
