import httpclient
import json
import nre
import os
import parseopt
import strformat

import jester

const
  ConfigHome = getEnv("XDG_CONFIG_HOME", "~/.config")
  ConfigName = "config.json"


type
  Config = tuple
    markupLink: MarkupLinkConf

  MarkupLinkConf = tuple
    timeout: int
    proxy: string


var conf: Config


template jsonResp(code, message: untyped): untyped =
  resp code, @{"Content-Type": "application/json"}, message


template errorResp(code, message: untyped): untyped =
  jsonResp code, $ %*{
    "message": message
  }


proc loadConfig =
  for path in [ConfigName, ConfigHome / "nimapi" / ConfigName]:
    try:
      conf = readFile(path).parseJson.to Config
      break
    except IOError:
      discard

  if conf.markupLink.timeout == 0:
    conf.markupLink.timeout = 4000


proc getUrlTitle(link: string): Future[string] {.async.} =
  proc getContent(c: AsyncHttpClient): Future[Option[string]] {.async.} =
    try:
      let fut = c.getContent(link)
      if await fut.withTimeout conf.markupLink.timeout:
        return some fut.read
    except:
      return some getCurrentExceptionMsg()


  let proxyUrl = conf.markupLink.proxy
  let pclient = case proxyUrl
    of "":
      newAsyncHttpClient()
    else:
      newAsyncHttpClient(proxy = newProxy(proxyUrl))

  let pcontent = await pclient.getContent
  if pcontent.isNone:
    return "timeout"

  let matchRes = pcontent.get.find re"<title>\s*(.+)\s*<\/title>"
  if matchRes.isSome:
    return matchRes.get.captures[0]
  else:
    return "nomatch"


loadConfig()

var
  p = initOptParser()
  format = "md"
  url = ""

while true:
  p.next()
  case p.kind
  of cmdEnd:
    break
  of cmdShortOption, cmdLongOption:
    case p.key
    of "format":
      format = p.val
  of cmdArgument:
    url = p.key

if url != "":
  try:
    let title = waitFor url.getUrlTitle
    case format
    of "org":
      echo &"[[{url}][{title}]]"
    of "md":
      echo &"[{title}]({url})"

    quit 0
  except Exception as e:
    echo e.msg
    quit(1)


routes:
  post "/api/markup-link":
    let
      data = parseJson request.body
      link = data.getOrDefault("link").getStr
      format = data.getOrDefault("format").getStr

    if link == "":
      errorResp Http422, "Blank link params"

    if format == "":
      errorResp Http422, "Blank format params"

    if format notin ["org", "md"]:
      errorResp Http422, "Unsupported format"

    let title = await getUrlTitle link
    var markup: string

    case format
    of "org":
      markup = &"[[{link}][{title}]]"
    of "md":
      markup = &"[{title}]({link})"

    jsonResp Http200, $ %*{
      "markup": markup
    }
