# Nim API

## Docker

```sh
nimble run

docker build -t api-nim .
docker run -p 8000:5000 -d api-nim
```

## Todos

- [ ] JSON Validator
- [ ] Hot reload
- [ ] Params validation
- [ ] Redirect
