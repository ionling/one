# Package

version       = "0.0.1"
author        = "ionling"
description   = "API in Nim"
license       = "MIT"
srcDir        = "src"
bin           = @["api"]


# Dependencies

requires "nim >= 1.4.2"
requires "jester >= 0.5.0"
