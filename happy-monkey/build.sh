if [ ! $1 ]; then
    echo "No monkey specified"
    exit 1
fi

docker run --rm -v $PWD:/script -u `id -u $USER` rakudo-star perl6 /script/happy-monkey.raku $1

if command -v prettier >/dev/null; then
    prettier --write born/$1.user.js
else
    echo '`prettier` not found, try to install it to get beautiful JavaScript.'
fi

# TODO getopt
