# TODO <{code}> is so slow for `style` tag

sub get-pattern ($tag) {
    '"<%s>".+"</%s>"'.sprintf($tag, $tag)
}


sub convert ($tag, $html) {
    # Sub inside regex
    # https://stackoverflow.com/a/47260479/7134763
    my $pattern  = get-pattern $tag;
    $html ~~ / <$pattern> /;
    # $html ~~ / <{ get-pattern $tag }> /;

    # Regex match
    # https://docs.perl6.org/language/variables#The_$/_variable
    return '' if !$/ ;
    my $len = $tag.codes + 2;
    my $inner = $/.substr($len, *-($len + 1));
    given $tag {
        when "html" {
            # Heredocs: :to
            # https://docs.perl6.org/language/quoting#Heredocs:_:to
            qq:to/END/;
            /* HTML */
            const div = document.createElement('div')
            div.id = 'happy-monkey'
            div.innerHTML = `$inner`
            document.body.appendChild(div)
            END
        }
        when "style" {
            qq:to/END/;
            /* CSS */
            const style = document.createElement('style')
            style.type = 'text/css'
            style.innerHTML = `$inner`
            document.head.appendChild(style)
            END
        }
        when "script" {
            qq:to/END/;
            /* JavaScript */
            $inner
            END
        }
    }
}


sub MAIN ($monkey) {
    my $dirname = IO::Path.new($?FILE).dirname;
    my $html = slurp "$dirname/monkeys/$monkey.html";
    my @tags = 'html', 'style', 'script';
    my $out = @tags.map(-> $x {"\n" ~ convert $x, $html});
    $html ~~ / '<script type="pre">'.+?'</script>' /;
    my $born = mkdir("$dirname/born");
    spurt $born.path ~ "/$monkey.user.js", $/.substr(19, *-9) ~ $out;
    say 'Monkey born!';
}
