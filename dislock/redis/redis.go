package redis

import (
	"context"
	"errors"
	"time"

	goredis "github.com/redis/go-redis/v9"

	"dislock"
)

type locker struct {
	rdb  goredis.UniversalClient
	key  string
	exp  time.Duration
	wait time.Duration
}

type NewParams struct {
	Client     goredis.UniversalClient
	Key        string
	Expiration time.Duration
	Wait       time.Duration
}

const (
	keyPrefix = "dislock:"
)

var (
	Timeout   = errors.New("timeout")
	NotLocked = errors.New("not locked")
)

func New(p NewParams) dislock.Locker {
	l := locker{
		rdb:  p.Client,
		key:  keyPrefix + p.Key,
		exp:  p.Expiration,
		wait: p.Wait,
	}
	if l.exp == 0 {
		l.exp = 3 * time.Second
	}
	if l.wait == 0 {
		l.wait = 500 * time.Millisecond
	}

	return &l
}

func (l *locker) Lock(ctx context.Context) (err error) {
	ok := false
	for !ok {
		ok, err = l.rdb.SetNX(ctx, l.key, "", l.exp).Result()
		if err != nil {
			return err
		}
		if ok {
			return nil
		}
		time.Sleep(l.wait)
	}
	return nil
}

func (l *locker) Unlock(ctx context.Context) error {
	n, err := l.rdb.Del(ctx, l.key).Result()
	if err != nil {
		return err
	}
	if n == 0 {
		return NotLocked
	}
	return nil
}
