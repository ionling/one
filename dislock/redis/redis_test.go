package redis

import (
	"context"
	"sync"
	"testing"
	"time"

	"github.com/redis/go-redis/v9"
	"github.com/rs/zerolog/log"
	"github.com/stretchr/testify/assert"
)

func TestRedis(t *testing.T) {
	var (
		rdb = redis.NewClient(&redis.Options{
			Addr: "127.0.0.1:6379",
		})
		ctx    = context.Background()
		prefix = "dislock:test:"
	)

	key := prefix + "setnx"
	exp := 2 * time.Second
	t.Run("setnx", func(t *testing.T) {
		ok, err := rdb.SetNX(ctx, key, "", exp).Result()
		assert.NoError(t, err)
		assert.True(t, ok)
	})

	t.Run("setnx-again", func(t *testing.T) {
		ok, err := rdb.SetNX(ctx, key, "", exp).Result()
		assert.NoError(t, err)
		assert.False(t, ok)
	})

	time.Sleep(exp)
	t.Run("check-nx", func(t *testing.T) {
		err := rdb.Get(ctx, key).Err()
		assert.ErrorIs(t, err, redis.Nil)
	})

	t.Run("del", func(t *testing.T) {
		n, err := rdb.Del(ctx, prefix+"not-exist").Result()
		assert.NoError(t, err)
		assert.Zero(t, n)
	})
}

func TestLocker(t *testing.T) {
	var (
		rdb = redis.NewClient(&redis.Options{
			Addr: "127.0.0.1:6379",
		})
		l = New(NewParams{
			Client:     rdb,
			Key:        "test",
			Expiration: 2 * time.Second,
		})
		wg sync.WaitGroup
	)

	do := func(name string) {
		defer wg.Done()

		ctx := context.Background()
		logger := log.With().Str("goroutine", name).Logger()

		if err := l.Lock(ctx); err != nil {
			logger.Err(err).Msg("lock")
			return
		}

		logger.Info().Msg("locked")
		time.Sleep(time.Second)
		if err := l.Unlock(ctx); err != nil {
			logger.Err(err).Msg("unlock")
			return
		}
		logger.Info().Msg("unlocked")
	}

	go do("apple")
	go do("orange")
	wg.Add(2)
	wg.Wait()
}
