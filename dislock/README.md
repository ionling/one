# Dislock

Various distributed lock implementations.

- [ ] Redis
- [ ] MySQL
- [ ] etcd
- [ ] MongoDB
- [ ] Memcached
- [ ] ZooKeeper
