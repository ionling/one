package dislock

import "context"

type Locker interface {
	Lock(context.Context) error
	Unlock(context.Context) error
}
