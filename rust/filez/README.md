# FileZ

## Design

### Which parsing lib

- https://rust-cli.github.io/book/tutorial/cli-args.html
- https://github.com/rosetta-rs/argparse-rosetta-rs
- https://rust-cli-recommendations.sunshowers.io/cli-parser.html
- clap
  - usages
    - https://github.com/BurntSushi/ripgrep/blob/44fb9fce2c1ee1a86c450702ea3ca2952bf6c5a7/Cargo.toml#L54
    - https://github.com/sharkdp/bat/blob/5e77ca37e89c873e4490b42ff556370dc5c6ba4f/Cargo.toml#L79
    - https://github.com/starship/starship/blob/55fa90c004afcdba33788d4d2ab94fa48f8bb35c/Cargo.toml#L46
