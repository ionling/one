use std::{
    env,
    fs::{self, File},
    io::Write,
    path::{Path, PathBuf},
    sync::mpsc,
};

use anyhow::{bail, Context, Result};
use clap::Parser;
use log::info;
use zip::{write::FileOptions, ZipWriter};

// TODO clippy
// TODO multiple inpuit files
// TODO stream compression
/// Using [`anyhow::Context`] can make the code verbose
/// and may introduce a slight performance overhead.
fn main() -> Result<()> {
    env_logger::try_init().context("init log")?;
    let cwd = env::current_dir().context("get cwd")?;
    let exec_path = env::args().nth(0).expect("no exec path");
    log::debug!("CWD: {:?}", cwd);
    log::debug!("Exec path: {}", exec_path);
    let args = Args::parse();
    log::debug!("Args: {:?}", args);
    let p = PathBuf::from(&args.in_path);
    let mut files = Vec::new();
    let (dir_tx, dir_rx) = mpsc::channel();

    // Step: Collect files
    if p.is_file() {
        files.push(p)
    } else if p.is_dir() {
        if !args.dir {
            bail!("{:?} is a directory", p)
        }
    }

    for dir in dir_rx {
        let entries = fs::read_dir(dir)?;
        for entry in entries {
            if let Err(err) = entry {
                info!("Error when iterate directory: {err}");
            } else if let Ok(entry) = entry {
                let path = entry.path();
                if path.is_file() {
                    files.push(path);
                } else if path.is_dir() {
                    if args.recursive {
                        dir_tx.send(path)?;
                    } else {
                        info!("{path:?} is directory, skipping");
                    }
                } else {
                    info!("Found irregular path: {path:?}, skipping")
                }
            }
        }
    }

    // Step: Compress
    let out_file = File::create(&args.out_path).context("create file")?;
    let mut zip_writer = ZipWriter::new(out_file);
    // TODO Add more compression options
    let zip_options = FileOptions::default();
    for f in files {
        match f.to_str() {
            None => {
                // TODO warning before writing
                info!("Invalid path {f:?}, skipping")
            }
            Some(str) => {
                let content = fs::read(str).context("read file")?;
                zip_writer
                    .start_file(str, zip_options)
                    .context("start file")?;
                zip_writer.write_all(&content).context("write all")?;
            }
        }
    }
    zip_writer.finish()?.flush().context("flush")?;
    Ok(())
}

/// A file compressor
#[derive(Parser, Debug)]
#[command(author, version, about, long_about = None)]
struct Args {
    /// Path of the input file
    #[arg(value_parser = path_exists)]
    in_path: String,

    /// Path of the output file
    out_path: String,

    /// Indicate [Self::in_path] is a directory.
    ///
    /// Otherwise the program will exit if it encounters a directory.
    #[arg(long)]
    dir: bool,

    /// Recursively traverses directories.
    ///
    /// It is only useful when [Self::dir] is `true`.
    #[arg(long)]
    recursive: bool,
}

fn path_exists(s: &str) -> Result<String, String> {
    match Path::new(s).try_exists() {
        Ok(ok) if ok => Ok(s.to_owned()),
        Ok(_) => Err("not exists".to_owned()),
        Err(e) => Err(format!("ERR: {}", e)),
    }
}
