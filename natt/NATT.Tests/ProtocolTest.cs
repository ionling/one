using System.Net;

namespace NATT.Tests;

public class ProtocolTest
{
    [Fact]
    public void BlankMessage()
    {
        var msg = new Message(MessageType.Ok);
        var bs = new byte[] { 4, 0, 0 };
        Assert.True(msg.Encode().Span.SequenceEqual(bs));

        var de = Message.Decode(bs);
        Assert.Equal(MessageType.Ok, de.Type);
    }

    [Fact]
    public void ErrMessage()
    {
        var msg = new Message.Err(MessageType.Register, "Not found");
        var bs = msg.Encode();
        var got = Message.Decode(bs) as Message.Err;
        Assert.NotNull(got);
        Assert.Equal(msg.ErrTyp, got.ErrTyp);
        Assert.Equal(msg.Msg, got.Msg);
    }

    [Fact]
    public void BinaryIPEndPoint()
    {
        var ep = new IPEndPoint(1008611, 4000);
        using var ms = new MemoryStream();
        using var bw = new BinaryWriter(ms);
        bw.Write(ep);

        ms.Position = 0;
        using var br = new BinaryReader(ms);
        var got = br.ReadIPEndPoint();
        Assert.Equal(ep, got);
    }

    [Fact]
    public void TestMemoryStream()
    {
        using var ms = new MemoryStream();
        ms.WriteByte(1);
        ms.Position = 0;
        var got = ms.ReadByte();
        Assert.Equal(1, got);
    }
}
