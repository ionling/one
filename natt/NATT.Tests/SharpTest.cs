namespace NATT.Tests;

using System.Net;
using System.Net.Sockets;
using System.Runtime.InteropServices;
using System.Text;

public record struct Person(string Name, int Age);

public class SharpTest
{
    Person person = new("John Doe", 30);

    [Fact]
    public void BinaryWriter()
    {
        using var ms = new MemoryStream();
        using var writer = new BinaryWriter(ms);
        writer.Write(person.Name);
        writer.Write(person.Age);
        //                    length,                               , int
        var bs = new byte[] { 8, 74, 111, 104, 110, 32, 68, 111, 101, 30, 0, 0, 0 };
        Assert.Equal(bs, ms.ToArray());
        Assert.Equal(13, ms.Length);
    }

    [Fact(Skip = "A broken test")]
    public void MarshalIt()
    {
        var size = Marshal.SizeOf(person);
        Assert.Equal(16, size);
        var arr = new byte[size];
        var ptr = Marshal.AllocHGlobal(size);
        // NOTE: The below line will make the test not work
        Marshal.StructureToPtr(person, ptr, true);
        Marshal.Copy(ptr, arr, 0, size);
        Marshal.FreeHGlobal(ptr);
        var bs = new byte[] { 8, 74, 111, 104, 110, 32, 68, 111, 101, 30, 0, 0, 0 };
        Assert.Equal(bs, arr);
    }

    [Fact]
    public void UdpReceive()
    {
        var port = 33449;
        var server = new UdpClient(port);
        var client = new UdpClient();
        var msg = "Hello";
        client.Connect(IPAddress.Loopback, port);

        Task.Run(() =>
        {
            client.Send(Encoding.UTF8.GetBytes(msg));
        });

        var ep = new IPEndPoint(IPAddress.Any, 0);
        var packet = server.Receive(ref ep);
        var packetStr = Encoding.UTF8.GetString(packet);
        Assert.Equal(msg, packetStr);
        Assert.NotEqual(port, ep.Port); // Finally understood what "ref" means
    }

    [Fact(Skip = "Change IP first")]
    public async void UdpSend()
    {
        var msg = new Message.Err(MessageType.Register, "Hello");
        var bs = msg.Encode();
        var ip = "145.28.87.99";
        var se = SocketError.AlreadyInProgress;
        using var udpClient = new UdpClient(ip, 22345);
        try
        {
            for (int i = 0; i < 3; i++)
            {
                await udpClient.SendAsync(bs);
                await Task.Delay(1000);
            }
        }
        catch (SocketException e)
        {
            se = e.SocketErrorCode;
        }
        Assert.Equal(SocketError.ConnectionRefused, se);
    }

    [Fact]
    public void TestIPEndPoint()
    {
        var ep1 = new IPEndPoint(IPAddress.None, 996);
        var ep2 = new IPEndPoint(IPAddress.None, 996);
        Assert.Equal(ep1, ep2);

        var ep3 = new IPEndPoint(new IPAddress(10092), 996);
        var ep4 = new IPEndPoint(new IPAddress(10092), 996);
        Assert.Equal(ep3, ep4);
        Assert.False(ep3 == ep4);
    }
}