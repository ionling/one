# NATT

It's NAT Traversal

## Plan

1. [x] 20240805 Basic client/server
2. [x] 20240805 Try AOT deployment
3. [x] 20240809 Support register message
4. [x] 20240812 Add xunit
5. [x] 20240813 Add `MemoryPack`
6. [x] 20240815 Add UDP server
7. [x] 20240816 Add `System.CommandLine`
8. [x] 20240820 Fix stream handling
9. [x] 20240821 Found UDP ConnectionRefused
10. [x] 20240822 Test it
11. [x] 20240826 Migrate Server to UDP
12. [x] 20240827 Migrate Client to UDP
13. [x] 20240828 Migrate Protocol to UDP
14. [x] 20240829 Test it

## Rookie

1. Project init
2. Span vs Memory
3. Concurrent list
4. Out param

## C# UdpClient SocketException

You're correct that in C#, a `SocketException` with the message "Connection refused"
can occur when using a UDP client.
This might seem surprising given that UDP is a connectionless protocol.
However, here's what's happening:

When a UDP client sends a packet to a remote endpoint (IP address and port)
where no service is listening,
the operating system of the remote machine may send back an ICMP (Internet Control Message Protocol)
message indicating that the destination is unreachable, specifically a "Port Unreachable" message.

In C#, the `UdpClient` or `Socket` class might interpret this ICMP message
as a `SocketException` with the error code `SocketError.ConnectionRefused`.
This exception essentially indicates that the UDP packet could not be delivered
because the target port was not open or there was no application listening on that port.

So, while UDP itself does not establish connections
and does not have the concept of connection refusal in the way TCP does,
the underlying network stack can still signal to your application
that a packet couldn't be delivered,
which is surfaced as a `SocketException` in C#.

## References

- [MemoryT usage guidelines](https://gist.github.com/GrabYourPitchforks/4c3e1935fd4d9fa2831dbfcab35dffc6)
- https://blog.cloudflare.com/introducing-0-rtt
- https://learn.microsoft.com/en-us/dotnet/core/testing/unit-testing-with-dotnet-test
