namespace NATT;

using System.Collections.Concurrent;
using System.Net;
using System.Net.Sockets;

using Microsoft.Extensions.Logging;

public class Client(string name, IPEndPoint server, ILoggerFactory factory)
{

    readonly UdpClient _udp = new(23443);
    IPEndPoint? _public_endpoint;

    readonly ILogger<Client> _logger = factory.CreateLogger<Client>();

    readonly ConcurrentDictionary<string, IPEndPoint> _udpClients = [];
    readonly ConcurrentDictionary<string, DateTime> _handshakeTimes = [];
    readonly ConcurrentDictionary<string, CancellationTokenSource> _handshakeCancels = [];

    MessageType _state = MessageType.Ok;
    DateTime _state_updated_at = DateTime.Now;
    readonly TimeSpan _resetStatusInterval = TimeSpan.FromMinutes(4);

    MessageType State
    {
        get => _state;
        set
        {
            _state = value;
            _state_updated_at = DateTime.Now;
        }
    }

    public async Task StartAsync()
    {
        await new Message.Register(name).SendAsync(_udp, server);
        State = MessageType.Register;
        // Start listening for messages from the server
        _ = Task.Run(Listen);
        _ = Task.Run(CleanHandshakes);
        _ = Task.Run(ResetStateTimer);
    }

    async Task Listen()
    {
        while (true)
        {
            var packet = await _udp.ReceiveAsync();
            var msg = Message.Decode(packet);
            _ = Task.Run(() => Handle(msg));
        }
    }

    async Task Handle(Message msg)
    {
        switch (msg)
        {
            default:
                _logger.LogWarning("Unknown msg: {Type}", msg.Type);
                break;
            case Message.Broadcast b:
                _logger.LogInformation("Received: {Str}", b.Msg);
                break;
            case Message.ConnectClient cc:
                await HandleConnectClientAsync(cc);
                break;
            case Message.RegisterResp rr:
                if (State is not MessageType.Register)
                {
                    _logger.LogWarning("Unknown msg: {Type}", msg.Type);
                }
                _public_endpoint = rr.Endpoint;
                ResetState();
                break;
            case Message.ConnectResp cr:
                if (State is not MessageType.Connect)
                {
                    _logger.LogWarning("Bad state {State} for {Type}", State, cr.Type);
                    break;
                }
                await Handshake(cr.Dst);
                break;
            case Message.Handshake h:
                HandleHandshakeAsync(h);
                break;
            case Message.Err e:
                _logger.LogWarning("Err {Type} with {Msg}", e.ErrTyp, e.Msg);
                ResetState();
                break;
        }
    }

    public async Task BroadcastAsync(string s)
    {
        if (_public_endpoint is null)
        {
            _logger.LogWarning("Not register to server");
        }
        await new Message.Broadcast(s).SendAsync(_udp, server);
    }

    public async Task ConnectAsync(string name)
    {
        if (_public_endpoint is null)
        {
            _logger.LogInformation("Not connected to server");
        }
        await new Message.Connect(name).SendAsync(_udp, server);
        State = MessageType.Connect;
    }

    async Task HandleConnectClientAsync(Message.ConnectClient msg)
    {
        if (State is MessageType.ConnectClient)
        {
            var err = "Another P2P connection is in progress";
            await new Message.ConnectClientResp(err).SendAsync(_udp, msg.Endpoint);
            return;
        }

        State = MessageType.ConnectClient;
        await new Message.ConnectClientResp("").SendAsync(_udp, msg.Endpoint);
        _ = Task.Run(() => Handshake(msg.SrcEp));
    }

    async Task Handshake(IPEndPoint dst, int n = 6)
    {
        var addr = dst.ToString();
        var cts = new CancellationTokenSource();
        _handshakeCancels[addr] = cts;
        _logger.LogInformation("P2P handshake starts");
        for (int i = 0; i < n; i++)
        {
            try
            {
                _handshakeTimes[addr] = DateTime.Now;
                _logger.LogDebug("Send handshake from {Src} to {Dst}",
                    _udp.Client.LocalEndPoint, dst);
                await new Message.Handshake().SendAsync(_udp, dst, cts.Token);
            }
            catch (OperationCanceledException)
            {
                _logger.LogInformation("P2P handshake done");
            }
            catch (SocketException e)
            {
                // See README > C# UdpClient SocketException
                if (e.SocketErrorCode != SocketError.ConnectionRefused)
                {
                    throw;
                }
            }
            await Task.Delay(1000);
        }
        _logger.LogInformation("P2P handshake completed three times");
        ResetState();
    }

    bool HandleHandshakeAsync(Message.Handshake msg)
    {
        _logger.LogInformation("Got UDP packet from {Addr}", msg.Endpoint);
        var addr = msg.Endpoint.ToString();
        if (!_handshakeTimes.TryGetValue(addr, out DateTime exp) || exp < DateTime.Now)
        {
            return false;
        }
        else
        {
            _udpClients[addr] = msg.Endpoint;
            _handshakeTimes.Remove(addr, out _);
            if (_handshakeCancels.Remove(addr, out CancellationTokenSource? cts))
            {
                cts.Cancel();
            }
            _logger.LogInformation("UDP connection established: {Endpoint}", msg.Endpoint);
            return true;
        }
    }

    async Task CleanHandshakes()
    {
        while (true)
        {
            foreach (var shake in _handshakeTimes)
            {
                if (shake.Value < DateTime.Now)
                {
                    _handshakeTimes.Remove(shake.Key, out _);
                    _handshakeCancels.Remove(shake.Key, out _);
                }
            }
            await Task.Delay(TimeSpan.FromMinutes(1));
        }
    }

    async Task ResetStateTimer()
    {
        while (true)
        {
            if (_state_updated_at + _resetStatusInterval < DateTime.Now)
            {
                ResetState();
            }
            await Task.Delay(TimeSpan.FromMinutes(1));
        }
    }

    void ResetState() => State = MessageType.Ok;
}
