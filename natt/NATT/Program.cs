// See https://aka.ms/new-console-template for more information
namespace NATT;

using System.CommandLine;
using System.Net;
using Microsoft.Extensions.Logging;

class Program
{
    static async Task<int> Main(string[] args)
    {
        using var factory = LoggerFactory.Create(builder => builder.AddConsole());
        var logger = factory.CreateLogger<Program>();
        logger.LogInformation("Hello World! Logging is {Description}.", "fun");

        var portArg = new Argument<int>("port", () => 33444,
            "The port for the server to listen on");
        var serverCmd = new Command("server", "Run the server")
        {
            portArg,
        };
        serverCmd.SetHandler((port) =>
        {
            var server = new Server(port, factory.CreateLogger<Server>());
            _ = Task.Run(server.StartAsync);
            while (true)
            {
                Console.Write("> ");
                var line = Console.ReadLine()?.Split(" ", 2);
                switch (line?[0])
                {
                    case "l":
                        foreach (var c in server.Clients)
                        {
                            Console.WriteLine($"{c.Name} => {c.Endpoint}");
                        }
                        break;
                }
                if (line?[0] == "q")
                {
                    break;
                }
            }
        }, portArg);

        var nameArg = new Option<string>("--name", () => "random", "The client name");
        var serverAddrArg = new Argument<string>("server-addr", "The server address");
        var clientCmd = new Command("client", "Run the client")
        {
            nameArg,
            serverAddrArg,
        };
        clientCmd.SetHandler(async (addr, name) =>
        {
            if (!IPEndPoint.TryParse(addr, out IPEndPoint? ep))
            {
                logger.LogError("Invalid server addr: {Addr}", addr);
                return;
            };
            if (name == "random")
            {
                name = StringUtils.Random(6);
            }
            var client = new Client(name, ep, factory);
            _ = Task.Run(client.StartAsync);
            while (true)
            {
                Console.Write("> ");
                var line = Console.ReadLine()?.Split(" ", 2);
                switch (line?[0])
                {
                    case "b":
                        await client.BroadcastAsync(line[1]);
                        break;
                    case "c":
                        await client.ConnectAsync(line[1]);
                        break;
                    case "r":
                        await client.StartAsync();
                        break;
                }
                if (line?[0] == "q")
                {
                    break;
                }
            }
        }, serverAddrArg, nameArg);

        var root = new RootCommand("A NAT Traversal tool")
        {
            clientCmd,
            serverCmd,
        };

        // Parse the incoming arguments and invoke the appropriate handler
        return await root.InvokeAsync(args);
    }
}
