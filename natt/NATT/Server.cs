namespace NATT;

using System.Collections.Concurrent;
using System.Net;
using System.Net.Sockets;

using Microsoft.Extensions.Logging;

public class Server(int port, ILogger<Server> logger)
{
    readonly UdpClient _udp = new(port);
    // TODO Clean it up
    readonly ConcurrentBag<ClientInfo> _clients = [];
    Traversal? _traversal;

    internal ConcurrentBag<ClientInfo> Clients => _clients;

    public async Task StartAsync()
    {
        logger.LogInformation("Server listening on {Port}", port);
        while (true)
        {
            var packet = await _udp.ReceiveAsync();
            var msg = Message.Decode(packet);
            // Handle client in a separate task
            _ = Task.Run(() => HandleAsync(msg));
        }
    }

    private async Task HandleAsync(Message msg)
    {
        switch (msg)
        {
            case Message.Register reg:
                await HandleRegister(reg);
                break;
            case Message.Broadcast brd:
                await HandleBroadcast(brd);
                break;
            case Message.Connect conn:
                await HandleConnect(conn);
                break;
            case Message.ConnectClientResp ccr:
                if (_traversal is not null)
                {
                    logger.LogInformation("OK response from {Dst}", _traversal.Dst);
                    await new Message.ConnectResp(_traversal.Dst.Endpoint).
                        SendAsync(_udp, _traversal.Src.Endpoint);
                    _traversal = null;
                }
                break;
            case Message.Err e:
                if (_traversal is not null)
                {
                    logger.LogInformation("Err response from {Dst}", _traversal.Dst);
                    await new Message.Err(MessageType.ConnectResp, e.Msg).
                        SendAsync(_udp, _traversal.Src.Endpoint);
                }
                break;
            default:
                await new Message.Err(MessageType.Err, $"Invalid msg type {msg.Type}").
                    SendAsync(_udp, msg.Endpoint);
                break;
        }
    }

    private async Task HandleConnect(Message.Connect msg)
    {
        async Task SendErr(string s) =>
            await new Message.Err(MessageType.Connect, s).SendAsync(_udp, msg.Endpoint);

        if (_traversal is not null)
        {
            await SendErr("Another P2P connection is in progress");
            return;
        }

        var src = GetClient(msg.Endpoint);
        if (src is null)
        {
            await SendErr($"No src client {msg.Name}");
            return;
        }

        var dst = GetClient(msg.Name);
        if (dst is null)
        {
            await SendErr($"No dst client {msg.Name}");
            return;
        };

        logger.LogInformation("Starting connection to {Dst} from {Src}", dst.Name, src.Name);
        await new Message.ConnectClient(src.Name, src.Endpoint).
            SendAsync(_udp, dst.Endpoint);
        _traversal = new Traversal(src, dst);
    }

    private async Task HandleRegister(Message.Register msg)
    {
        Message res;
        var info = GetClient(msg.Name);
        if (info is not null)
        {
            var err = $"client {info.Name} existed";
            res = new Message.Err(MessageType.Register, err);
        }
        else
        {
            _clients.Add(new ClientInfo(msg.Name, msg.Endpoint));
            res = new Message.RegisterResp(msg.Endpoint);
        }
        await res.SendAsync(_udp, msg.Endpoint);
        logger.LogInformation("New client registered: {Name}", msg.Name);
    }

    private async Task HandleBroadcast(Message.Broadcast msg)
    {
        var bs = msg.Encode();
        foreach (var (_, endpoint) in _clients)
        {
            if (!endpoint.Equals(msg.Endpoint))
            {
                await _udp.SendAsync(bs, endpoint);
            }
        }
    }

    ClientInfo? GetClient(string name) => _clients.FirstOrDefault(it => it.Name == name);

    ClientInfo? GetClient(IPEndPoint endpoint) =>
        _clients.FirstOrDefault(it => it.Endpoint.Equals(endpoint));
}

record ClientInfo(string Name, IPEndPoint Endpoint);

record Traversal(ClientInfo Src, ClientInfo Dst);