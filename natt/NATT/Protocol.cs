namespace NATT;

using System.Buffers.Binary;
using System.Net;
using System.Net.Sockets;
using System.Text;

using MemoryPack;

public enum MessageType
{
    /// <summary>
    /// Used when a client connects to the server for the first time.
    /// </summary>
    Register,
    RegisterResp,
    // TODO Deregister,

    /// <summary>
    /// Used when one client wants to establish a P2P connection with another client.
    /// </summary>
    Connect,
    ConnectResp,
    ConnectClient,
    ConnectClientResp,
    Handshake,

    Ok,
    Err,

    /// <summary>
    /// Broadcast the message to all clients.
    /// </summary>
    Broadcast,
}

public record Message(MessageType Type)
{
    [MemoryPackIgnore]
    public IPEndPoint Endpoint = new(IPAddress.None, 0);

    public record Register(string Name) : Message(MessageType.Register);
    public record RegisterResp(IPEndPoint Public) : Message(MessageType.RegisterResp);
    public record Broadcast(string Msg) : Message(MessageType.Broadcast);
    public record Connect(string Name) : Message(MessageType.Connect);
    public record ConnectResp(IPEndPoint Dst) : Message(MessageType.ConnectResp);
    public record ConnectClient(string SrcName, IPEndPoint SrcEp) :
        Message(MessageType.ConnectClient)
    {
        public Memory<byte> EncodeIt()
        {
            using var ms = new MemoryStream();
            using var bw = new BinaryWriter(ms);
            bw.Write(SrcName);
            bw.Write(SrcEp);
            return ms.ToArray();
        }

        public static ConnectClient DecodeIt(byte[] bs)
        {
            using var ms = new MemoryStream(bs);
            using var br = new BinaryReader(ms);
            return new ConnectClient(
                br.ReadString(),
                br.ReadIPEndPoint()
            );
        }
    }

    public record ConnectClientResp(string Msg) : Message(MessageType.ConnectClientResp);
    public record Handshake() : Message(MessageType.Handshake);
    public record Err(MessageType ErrTyp, string Msg) : Message(MessageType.Err)
    {
        public Memory<byte> EncodeIt()
        {
            using var ms = new MemoryStream();
            using var bw = new BinaryWriter(ms);
            bw.Write((byte)ErrTyp);
            bw.Write(Msg);
            return ms.ToArray();
        }

        public static Err DecodeIt(Span<byte> bs)
        {
            using var ms = new MemoryStream(bs.ToArray());
            using var br = new BinaryReader(ms);
            return new Err(
                (MessageType)br.ReadByte(),
                br.ReadString()
            );
        }
    }

    public static Message Decode(Memory<byte> bs)
    {
        var mt = (MessageType)bs.Span[0];
        var data = bs[3..].Span;
        return mt switch
        {
            MessageType.Register => new Register(DecodeString(data)),
            MessageType.RegisterResp => new RegisterResp(IPEndPointExt.Decode2(data)),
            MessageType.Broadcast => new Broadcast(DecodeString(data)),
            MessageType.Connect => new Connect(DecodeString(data)),
            MessageType.ConnectResp => new ConnectResp(IPEndPointExt.Decode2(data)),
            MessageType.ConnectClient => ConnectClient.DecodeIt(data.ToArray()),
            MessageType.ConnectClientResp => new ConnectClientResp(DecodeString(data)),
            MessageType.Handshake => new Handshake(),
            MessageType.Ok => new Message(MessageType.Ok),
            MessageType.Err => Err.DecodeIt(data),
            _ => throw new NotImplementedException(),
        } ?? throw new InvalidOperationException("Deserialize failed");
    }

    public static Message Decode(UdpReceiveResult packet)
    {
        var msg = Decode(packet.Buffer);
        msg.Endpoint = packet.RemoteEndPoint;
        return msg;
    }

    public Memory<byte> Encode()
    {
        var data = this switch
        {
            Register r => EncodeString(r.Name),
            RegisterResp rr => rr.Public.Encode2(),
            Broadcast b => EncodeString(b.Msg),
            Connect c => EncodeString(c.Name),
            ConnectResp cr => cr.Dst.Encode2(),
            ConnectClient cc => cc.EncodeIt(),
            ConnectClientResp ccr => EncodeString(ccr.Msg),
            Handshake => null,
            Err err => err.EncodeIt(),
            _ => throw new NotImplementedException($"Should implement {Type}"),
        };
        if (data.Length > short.MaxValue)
        {
            throw new ArgumentOutOfRangeException("Too large data");
        }

        var len = sizeof(byte) + sizeof(short) + data.Length;
        var bs = new byte[len];
        bs[0] = (byte)Type;
        BinaryPrimitives.WriteInt16LittleEndian(bs.AsSpan(1), (short)data.Length);
        data.CopyTo(bs.AsMemory(3));
        return bs;
    }

    public async Task SendAsync(UdpClient client, IPEndPoint ep, CancellationToken ct = default)
    {
        await client.SendAsync(Encode(), ep, ct);
    }

    static Memory<byte> EncodeString(string s) => Encoding.UTF8.GetBytes(s);
    static string DecodeString(Span<byte> b) => Encoding.UTF8.GetString(b);
}

// IPEndPoint is not MemoryPackable, so a string type is used for the address instead.
[MemoryPackable]
public partial record struct NameAddress(string Name, string Addr, int Port);

public static class BinaryExt
{
    public static void Write(this BinaryWriter bw, IPEndPoint ep)
    {
        var bs = ep.Encode2();
        bw.Write((byte)bs.Length);
        bw.Write(bs);
    }

    public static IPEndPoint ReadIPEndPoint(this BinaryReader bw)
    {
        var len = bw.ReadByte();
        var bs = bw.ReadBytes(len);
        return IPEndPointExt.Decode2(bs);
    }
}

public static class IPEndPointExt
{
    // Encode2 uses less memory.
    [Obsolete("Use Encode2 instead.")]
    public static Memory<byte> Encode(this IPEndPoint ep)
    {
        return ep.Serialize().Buffer;
    }

    [Obsolete("Use Decode2 instead.")]
    public static IPEndPoint Decode(this IPEndPoint ep, Span<byte> data)
    {
        var family = data.Length == 6 ? AddressFamily.InterNetwork : AddressFamily.InterNetworkV6;
        var address = new SocketAddress(family, data.Length);

        for (int i = 0; i < data.Length; i++)
        {
            address[i] = data[i];
        }

        return (IPEndPoint)ep.Create(address);
    }

    public static byte[] Encode2(this IPEndPoint ep)
    {
        // IPv4 (4 bytes) or IPv6 (16 bytes) + Port (2 bytes)
        var len = (ep.AddressFamily == AddressFamily.InterNetwork ? 4 : 16) + 2;
        var bs = new byte[len];
        var port = BitConverter.GetBytes((ushort)ep.Port);
        ep.Address.TryWriteBytes(bs, out _);
        Array.Copy(port, 0, bs, len - 2, port.Length);
        return bs;
    }

    public static IPEndPoint Decode2(Span<byte> data)
    {
        // Determine if the address is IPv4 or IPv6
        int addrLen = (data.Length == 6) ? 4 : 16;
        var addr = new IPAddress(data[..addrLen]);
        ushort port = BitConverter.ToUInt16(data[addrLen..]);
        return new IPEndPoint(addr, port);
    }
}
