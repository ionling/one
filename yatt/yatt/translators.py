import hashlib
import time
import uuid
from dataclasses import dataclass
from enum import StrEnum

import requests
from openai import OpenAI
from rich.console import Console
from rich.table import Table

headers = {
    "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/111.0.0.0 Safari/537.36"
}


class AbbrSearchType(StrEnum):
    Exact = "e"
    Reverse = "r"


@dataclass
class AbbrTerm:
    id: str
    term: str
    definition: str
    score: str
    category: str
    category_name: str
    parent_category: str
    parent_category_name: str


class Abbr:
    _api = "https://www.abbreviations.com/services/v2/abbr.php"

    def __init__(self, uid: str, token: str) -> None:
        self._uid = uid
        self._token = token

    def request(self, q: str, search_type: AbbrSearchType) -> list[AbbrTerm]:
        resp = requests.get(
            self._api,
            headers=headers,
            params={
                "uid": self._uid,
                "tokenid": self._token,
                "term": q,
                "searchtype": search_type.value,
                "format": "json",
            },
        )
        return [
            AbbrTerm(
                t["id"],
                t["term"],
                t["definition"],
                t["score"],
                t["category"],
                t["categoryname"],
                t["parentcategory"] or "",
                t["parentcategoryname"] or "",
            )
            for t in resp.json()["result"]
        ]
        # Response example
        {
            "id": "2165341",
            "term": "TEC",
            "definition": "Technology",
            "category": "WEBSITES",
            "categoryname": "Websites",
            "parentcategory": "INTERNET",
            "parentcategoryname": "Internet",
            "score": "4.25",
        }

    @staticmethod
    def print(terms: list[AbbrTerm]):
        console = Console()
        table = Table(
            "ID",
            "Term",
            "Definition",
            "Score",
            "Category",
            "Category Name",
            "Parent Category",
            "Parent Category Name",
        )
        for t in terms:
            table.add_row(
                t.id,
                t.term,
                t.definition,
                t.score,
                t.category,
                t.category_name,
                t.parent_category,
                t.parent_category_name,
            )

        console.print(table)


class Youdao:
    _api = "https://openapi.youdao.com/api"

    @staticmethod
    def _encrypt(s: str):
        hash = hashlib.sha256()
        hash.update(s.encode("utf-8"))
        return hash.hexdigest()

    @staticmethod
    def _truncate(q):
        size = len(q)
        return q if size <= 20 else q[0:10] + str(size) + q[size - 10 : size]

    def __init__(self, key, secret):
        self._key = key
        self._secret = secret

    def translate(self, q: str, from_: str, to: str) -> list[str]:
        data = {
            "from": from_,
            "to": to,
            "signType": "v3",
            "appKey": self._key,
            "q": q,
        }

        curtime = str(int(time.time()))
        data["curtime"] = curtime
        salt = str(uuid.uuid1())
        sign_str = self._key + self._truncate(q) + salt + curtime + self._secret
        sign = self._encrypt(sign_str)
        data["salt"] = salt
        data["sign"] = sign

        resp = requests.post(self._api, data=data)
        j = resp.json()
        return j["translation"]


class DeepSeek:
    _api = "https://api.deepseek.com"

    def __init__(self, token: str) -> None:
        self._client = OpenAI(api_key=token, base_url=self._api)

    def lookup(self, q: str):
        msg = (
            "Check the spell and"
            + f" Explain '{q}' with the pronunciation, meaning and examples"
            + " with the Chinese translation"
        )
        response = self._client.chat.completions.create(
            model="deepseek-chat",
            messages=[
                {"role": "system", "content": "You are a language teacher"},
                {
                    "role": "user",
                    "content": msg,
                },
            ],
            stream=False,
        )

        return response.choices[0].message.content
