import typer
from rich import print
from rich.console import Console
from rich.markdown import Markdown

from yatt import config, translators

app = typer.Typer()
cf = config.load()


@app.command()
def abbr(
    q: str,
    type: translators.AbbrSearchType = translators.AbbrSearchType.Reverse,
):
    abbr_cf = cf.abbr
    if abbr_cf is None:
        print("[bold red]ERR[/bold red] No Abbr config")
        raise typer.Exit(1)

    t = translators.Abbr(abbr_cf.uid, abbr_cf.token)
    terms = t.request(q, search_type=type)
    t.print(terms)


@app.command()
def youdao(q: str):
    youdao_cf = cf.youdao
    if youdao_cf is None:
        print("[bold red]ERR[/bold red] No Youdao config")
        raise typer.Exit(1)

    t = translators.Youdao(youdao_cf.key, youdao_cf.secret)
    trans = t.translate(q, from_="auto", to="zh-CHS")
    for tr in trans:
        print(tr)


@app.command()
def deepseek(q: str):
    deepseek_cf = cf.deepseek
    if deepseek_cf is None:
        print("[bold red]ERR[/bold red] No DeepSeek config")
        raise typer.Exit(1)

    t = translators.DeepSeek(deepseek_cf.token)
    res = t.lookup(q)
    console = Console()
    md = Markdown(res or "")
    console.print(md)


if __name__ == "__main__":
    app()
