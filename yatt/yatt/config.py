import os
import tomllib
from dataclasses import dataclass
from pathlib import Path

xdg_config_dir = os.getenv("XDG_CONFIG_HOME", "~/.config")
config_dir = Path(xdg_config_dir).expanduser() / "yatt"


@dataclass
class Abbr:
    uid: str
    token: str


@dataclass
class Youdao:
    key: str
    secret: str


@dataclass
class DeepSeek:
    token: str


@dataclass
class Config:
    abbr: Abbr | None
    youdao: Youdao | None
    deepseek: DeepSeek | None


def load():
    with open(config_dir / "config.toml", "rb") as f:
        d = tomllib.load(f)

    load_section = lambda cls, name: cls(**d[name]) if name in d else None
    return Config(
        abbr=load_section(Abbr, "abbr"),
        youdao=load_section(Youdao, "youdao"),
        deepseek=load_section(DeepSeek, "deepseek"),
    )
