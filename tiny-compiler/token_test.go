package tinyc

import (
	"reflect"
	"testing"
)

func TestTokenize(t *testing.T) {
	type args struct {
		code string
	}
	tests := []struct {
		name       string
		args       args
		wantResult []Token
		wantErr    bool
	}{
		{
			"normal",
			args{"(add 1 2)"},
			[]Token{
				{ParenToken, "("},
				{IdentToken, "add"},
				{NumToken, "1"},
				{NumToken, "2"},
				{ParenToken, ")"},
			},
			false,
		},
		{
			"invalid-char",
			args{"(add% 1 2)"},
			nil,
			true,
		},
		{
			"nested",
			args{"(add 2 (subtract 4 2))"},
			[]Token{
				{ParenToken, "("},
				{IdentToken, "add"},
				{NumToken, "2"},
				{ParenToken, "("},
				{IdentToken, "subtract"},
				{NumToken, "4"},
				{NumToken, "2"},
				{ParenToken, ")"},
				{ParenToken, ")"},
			},
			false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			gotResult, err := Tokenize(tt.args.code)
			if (err != nil) != tt.wantErr {
				t.Errorf("Tokenize() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(gotResult, tt.wantResult) {
				t.Errorf("Tokenize() = %v, want %v", gotResult, tt.wantResult)
			}
		})
	}
}

func TestIsNumber(t *testing.T) {
	type args struct {
		char rune
	}
	tests := []struct {
		name string
		args args
		want bool
	}{
		{"0", args{'0'}, true},
		{"9", args{'9'}, true},
		{"x", args{'x'}, false},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := isNumber(tt.args.char); got != tt.want {
				t.Errorf("isNumber() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestIsLetter(t *testing.T) {
	type args struct {
		char rune
	}
	tests := []struct {
		name string
		args args
		want bool
	}{
		{"0", args{'0'}, false},
		{"a", args{'a'}, true},
		{"Z", args{'Z'}, false},
		{"%", args{'%'}, false},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := isLetter(tt.args.char); got != tt.want {
				t.Errorf("isLetter() = %v, want %v", got, tt.want)
			}
		})
	}
}
