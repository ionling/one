package tinyc

import (
	"reflect"
	"testing"
)

func TestParserParse(t *testing.T) {
	type fields struct {
		c      int
		tokens []Token
	}
	want1 := Node{
		Type: Program,
		Children: []Node{
			{
				CallExpr,
				"add",
				[]Node{
					{Type: NumLiteral, Value: "1"},
					{Type: NumLiteral, Value: "2"},
				},
			},
		},
	}

	sub21 := Node{
		CallExpr,
		"subtract",
		[]Node{
			{Type: NumLiteral, Value: "4"},
			{Type: NumLiteral, Value: "2"},
		},
	}

	want2 := Node{
		Type: Program,
		Children: []Node{
			{
				CallExpr,
				"add",
				[]Node{
					{Type: NumLiteral, Value: "2"},
					sub21,
				},
			},
		},
	}

	tests := []struct {
		name    string
		fields  fields
		wantN   Node
		wantErr bool
	}{
		{
			"normal",
			fields{
				0,
				[]Token{
					{ParenToken, "("},
					{IdentToken, "add"},
					{NumToken, "1"},
					{NumToken, "2"},
					{ParenToken, ")"},
				},
			},
			want1,
			false,
		},
		{
			"nested",
			fields{
				0,
				[]Token{
					{ParenToken, "("},
					{IdentToken, "add"},
					{NumToken, "2"},
					{ParenToken, "("},
					{IdentToken, "subtract"},
					{NumToken, "4"},
					{NumToken, "2"},
					{ParenToken, ")"},
					{ParenToken, ")"},
				},
			},
			want2,
			false,
		},
		{
			"error",
			fields{
				0,
				[]Token{
					{ParenToken, "("},
					{IdentToken, "add"},
					{NumToken, "1"},
					{NumToken, "2"},
				},
			},
			Node{},
			true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			p := &parser{
				c:      tt.fields.c,
				tokens: tt.fields.tokens,
			}
			gotN, err := p.Parse()
			if (err != nil) != tt.wantErr {
				t.Errorf("parser.Parse() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(gotN, tt.wantN) {
				t.Errorf("parser.Parse() = %v, want %v", gotN, tt.wantN)
			}
		})
	}
}
