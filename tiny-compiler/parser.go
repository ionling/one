package tinyc

import "fmt"

type parser struct {
	c      int
	tokens []Token
}

type NodeType uint8

const (
	NumLiteral NodeType = iota
	CallExpr
	Program
)

type Node struct {
	Type     NodeType
	Value    string
	Children []Node
}

func NewParser(ts []Token) *parser {
	return &parser{tokens: ts}
}

func (p *parser) Parse() (n Node, err error) {
	defer func() {
		if r := recover(); r != nil {
			err = r.(error)
		}
	}()

	bn := Node{Type: Program}

	for p.c < len(p.tokens) {
		bn.Children = append(bn.Children, p.walk())
	}

	n = bn
	return
}

func (p *parser) walk() (n Node) {
	t := p.getToken()
	switch {
	case t.Type == NumToken:
		n = Node{Type: NumLiteral, Value: t.Value}
	case t.Type == ParenToken && t.Value == "(":
		// Skip `(`
		p.step()
		t = p.getToken()

		n.Type = CallExpr
		n.Value = t.Value
		p.step()
		t = p.getToken()

		// TODO Handle index out of range
		for t.Type != ParenToken || t.Value != ")" {
			n.Children = append(n.Children, p.walk())
			t = p.getToken()
		}
	default:
		panic(fmt.Errorf("wrong token: %v", t.Type))
	}
	p.step()
	return
}

// getToken returns current token indexed by `p.c`.
func (p *parser) step() {
	p.c++
}

// getToken returns current token indexed by `p.c`.
func (p *parser) getToken() Token {
	return p.tokens[p.c]
}
