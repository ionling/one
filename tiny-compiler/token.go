package tinyc

import "fmt"

type TokenType uint8

const (
	ParenToken = iota
	NumToken
	IdentToken
)

type Token struct {
	Type  TokenType
	Value string
}

func Tokenize(code string) (result []Token, err error) {
	cur := 0
l:
	for cur < len([]rune(code)) {
		char := rune(code[cur])
		switch {
		case char == ' ':
			cur++
		case char == '(':
			result = append(result, Token{
				ParenToken,
				"(",
			})
			cur++
		case char == ')':
			result = append(result, Token{
				ParenToken,
				")",
			})
			cur++
		case isNumber(char):
			value := ""

			for isNumber(char) {
				value += string(char)
				cur++
				char = rune(code[cur])
			}

			result = append(result, Token{
				NumToken,
				value,
			})
		case isLetter(char):
			value := ""

			for isLetter(char) {
				value += string(char)
				cur++
				char = rune(code[cur])
			}

			result = append(result, Token{
				IdentToken,
				value,
			})
		default:
			result = nil
			err = fmt.Errorf("invalid character: %v", char)
			break l
		}
	}
	return
}

func isNumber(char rune) bool {
	return char >= '0' && char <= '9'
}

func isLetter(char rune) bool {
	return char >= 'a' && char <= 'z'
}
