# Package

version       = "0.1.0"
author        = "ionling"
description   = "A todo list app"
license       = "MIT"
srcDir        = "src"
bin           = @["todo"]


# Dependencies

requires "nim >= 2.0.0"
requires "norm >= 2.8.0"
