import options
import std/with
import times

import norm/[model, sqlite]

type
  Todo = ref object of Model
    title: string
    content: string
    done: bool
    done_at: Option[DateTime]
    created_at: DateTime
    updated_at: DateTime
    deleted_at: Option[DateTime]

proc newTodo(title = "", content = ""): Todo =
  Todo(title: title, content: content, created_at: now(), updated_at: now())


when isMainModule:
  let dbConn = open("todo.db", "", "", "")
  dbConn.createTables newTodo()

  var
    t1 = newTodo("1111")
    t2 = newTodo("2222")

  with dbConn:
    insert t1
    insert t2
