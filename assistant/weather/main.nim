import httpclient
import json
import logging
import os
import strformat
import times
import std/sugar


type
  ApiResponse = tuple
    apiStatus: string
    apiVersion: string
    lang: string
    location: Location
    result: Result

  Config = tuple
    colorfulcloudsToken: string
    pushbulletToken: string

  Location = tuple
    latitude: float
    longitude: float

  Result = tuple
    aqi: int
    temperature: float # 温度
    apparentTemperature: float # 体感温度


const
  HomeLocation = (104.050479, 30.616285)
  ColorfulcloudsApi = "https://api.caiyunapp.com/v2"
  PushbulletApi = "https://api.pushbullet.com/v2"
  PushbulletPushApi = &"{PushbulletApi}/pushes"
  ConfigHome = getEnv("XDG_CONFIG_HOME", "~/.config")

var config: Config

addHandler newConsoleLogger(fmtStr = "$datetime $levelname ")


proc loadConfig() =
  let path = ConfigHome / "weather" / "config.json"
  let j = path.expandTilde.readFile
  config = j.parseJson.to Config


proc pushMsg(title, contant: string) =
  let
    client = newHttpClient(
      headers = newHttpHeaders({
        "Content-Type": "application/json",
        "Access-Token": config.pushbulletToken
      })
    )
    body = %*{
      "type": "note",
      "title": title,
      "body": contant
    }
    resp = client.request(PushbulletPushApi, httpMethod = HttpPost, body = $body)

  echo resp.status


proc toApiResponse(jn: JsonNode): ApiResponse =
  let
    jl = jn["location"]
    jr = jn["result"]

  (
    apiStatus: jn["api_status"].getStr,
    apiVersion: jn["api_version"].getStr,
    lang: jn["lang"].getStr,
    location: (jl[0].getFloat, jl[1].getFloat),
    result: (
      aqi: jr["aqi"].getInt,
      temperature: jr["temperature"].getFloat,
      apparentTemperature: jr["apparent_temperature"].getFloat,
    )
  )


proc realtime(): Result =
  let
    client = newHttpClient()
    loc = &"{HomeLocation[0]},{HomeLocation[1]}"
    api = &"{ColorfulcloudsApi}/{config.colorfulcloudsToken}/{loc}/realtime.json"
    resp = client.request api

  resp.body.parseJson.toApiResponse.result


proc pushWeather() =
  info("pushing weather")
  let weather = realtime()
  pushMsg("天气提醒", &"""
空气指数: {weather.aqi}
温度: {weather.temperature}, 体感温度: {weather.apparentTemperature}
""")


proc everyDay(dt: DateTime) =
  info(&"Runs daily at {dt.hour:02}:{dt.minute:02}")

  let
    interval = initDuration(minutes = 1)

  while true:
    let now = getTime().local
    if now.hour == dt.hour and now.minute == dt.minute:
      pushWeather()

    sleep interval.inMilliseconds.int


when isMainModule:
  import parseopt

  var
    rightNow = false
    showHelp = false
    time = "08:00"
    p = initOptParser()
    help = """
Options:
  -n         run now
  -t=[time]  run in background, push weather at `time`,
             default 08:00.
"""

  setControlCHook () {.noconv.} => quit(0)

  try:
    loadConfig()
  except IOError:
    echo "No config file"
    quit(1)

  while true:
    p.next()
    case p.kind
    of cmdEnd: break
    of cmdShortOption, cmdLongOption:
      case p.key
      of "h":
        showHelp = true
      of "n":
        rightNow = true
      of "t":
        time = p.val
    of cmdArgument:
      discard

  if showHelp:
    echo help
  elif rightNow:
    pushWeather()
  else:
    try:
      let dt = parse(time, "HH:mm")
      everyDay(dt)
    except TimeParseError:
      echo "wrong time: ", time
